Readme --- DEATHSTARSHOCK ALPHA DEMO --

KEYS
Left, Right, Up, Down - movement
w   - wait (need to restore tiredness)
r   - reload weapon
h   - hard punch
f   - fire from current weapon
z   - swap current and additional weapons
a   - assult forward (move and fire)
i   - inventory
g   - get item from floor
TAB - start/stop run mode

KEYS IN INVENTORY
UP, DOWN  - move pointer in inventory list
ENTER, e  - equip weapon/armor
i, ESCAPE - close inventory
d         - drop item from inventory
u         - use medicament

KEYS IN GET ITEM MENU
UP, DOWN  - move pointer in get item list
ENTER, g  - get item from floor to inventory
ESCAPE    - close get item menu

GAMEPLAY
You can destroy boxes using hard punch
Try to find Ammo or Weapon Box

END OF GAME
Now, only if you die game will be ended. Good Luck, Marine!