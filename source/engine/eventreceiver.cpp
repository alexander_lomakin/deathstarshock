

#include "eventreceiver.h"


using namespace irr;


bool EventReceiver::_keysIsDown[KEY_KEY_CODES_COUNT];
boost::posix_time::ptime  EventReceiver::_keysPressTime[KEY_KEY_CODES_COUNT];
bool EventReceiver::_keysPressedInPressTime[KEY_KEY_CODES_COUNT];
boost::posix_time::ptime  EventReceiver::_keysRepeatTime[KEY_KEY_CODES_COUNT];

ITimer* EventReceiver::_timer = 0;

int  EventReceiver::_keyDelayTime  = 100;
int  EventReceiver::_keyRepeatTime = 40;


EventReceiver::EventReceiver()
{
    for (u32 i=0; i < KEY_KEY_CODES_COUNT; i++)
        _keysIsDown[i] = false;
}


bool EventReceiver::OnEvent(const SEvent& event)
{
    if (event.EventType == irr::EET_KEY_INPUT_EVENT)
    {
        if (!_keysIsDown[event.KeyInput.Key] && event.KeyInput.PressedDown)
        {
            _keysPressTime[event.KeyInput.Key]          = boost::posix_time::microsec_clock::local_time();
            _keysPressedInPressTime[event.KeyInput.Key] = false;
            _keysRepeatTime[event.KeyInput.Key]         = _keysPressTime[event.KeyInput.Key] + boost::posix_time::millisec(_keyDelayTime);
            _keysIsDown[event.KeyInput.Key]             = true;
        }
        else
            _keysIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
    }

    return false;
}


void EventReceiver::setTimer(ITimer* timer)
{
    _timer = timer;
}


bool EventReceiver::isKeyDown(EKEY_CODE keyCode)
{
    if (_keysIsDown[keyCode])
    {
        boost::posix_time::time_duration diff = boost::posix_time::microsec_clock::local_time() - _keysPressTime[keyCode];

        if (diff.total_milliseconds() <= _keyDelayTime)
        {
            if (!_keysPressedInPressTime[keyCode])
            {
                _keysPressedInPressTime[keyCode] = true;
                return true;
            }
            else
                return false;
        }
        else
        {
            boost::posix_time::time_duration diff2 = boost::posix_time::microsec_clock::local_time() - _keysRepeatTime[keyCode];

            if (diff2.total_milliseconds() >= _keyRepeatTime)
            {
                _keysRepeatTime[keyCode] = boost::posix_time::microsec_clock::local_time();
                return true;
            }else
                return false;
        }
    }
    else
        return false;
}
