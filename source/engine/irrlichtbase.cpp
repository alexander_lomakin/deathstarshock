

#include "irrlichtbase.h"


using namespace irr;
using namespace std;
using namespace audiere;


IrrlichtDevice* IrrlichtBase::_device      = 0;
video::IVideoDriver* IrrlichtBase::_driver = 0;
EventReceiver IrrlichtBase::_evReceiver;
AudioDevicePtr IrrlichtBase::_soundDevice;

int IrrlichtBase::_windowWidth     = 0;
int IrrlichtBase::_windowHeight    = 0;
int IrrlichtBase::_framesPerSecond = 1;


IrrlichtBase::IrrlichtBase(int width, int height, bool fullscreen) {
    if (fullscreen) {
        // create a NULL device to detect screen resolution
        IrrlichtDevice *nulldevice = createDevice(video::EDT_NULL);

        core::dimension2d<u32> deskres = nulldevice->getVideoModeList()->getDesktopResolution();

        nulldevice -> drop();

        _windowWidth  = deskres.Width;
        _windowHeight = deskres.Height;
    } else {
        _windowWidth  = width;
        _windowHeight = height;
    }

    _device = createDevice(video::EDT_OPENGL, core::dimension2d<u32>(_windowWidth, _windowHeight), 32
    , fullscreen, false, true, &_evReceiver);

    if (_device == 0)
    {
        std::cout << "Cannot create Irrlicht device" << std::endl;
        exit(EXIT_FAILURE);
    }

    _driver = _device->getVideoDriver();

    _soundDevice = OpenDevice();

    if (! _soundDevice) {
		exit(EXIT_FAILURE);
	}

    _evReceiver.setTimer(_device->getTimer());
}


IrrlichtDevice* IrrlichtBase::getDevice()
{
    return _device;
}


void IrrlichtBase::sound(string soundFilePath)
{
    OutputStreamPtr sound(OpenSound(_soundDevice, soundFilePath.c_str(), false));

    if (sound)
        sound->play();
}


video::IVideoDriver* IrrlichtBase::getDriver()
{
    return _driver;
}


AudioDevicePtr* IrrlichtBase::getAudioDevice()
{
    return &_soundDevice;
}


void IrrlichtBase::drop()
{
    _device->drop();
}


void IrrlichtBase::setFramesPerSecond(int nFps)
{
	_framesPerSecond = nFps;
}


void IrrlichtBase::setCaption(string caption)
{
    _device->setWindowCaption(L"demo");
}


void IrrlichtBase::startCalcFrames()
{
	_device->getTimer()->start();
}


void IrrlichtBase::checkFramesPerSecond()
{
	if(_device->getTimer()->getTime() < 1000 / _framesPerSecond)
		_device->sleep((1000 / _framesPerSecond) - _device->getTimer()->getTime());
}


video::ITexture* IrrlichtBase::loadTexture(string path)
{
    return _driver->getTexture(path.c_str());
}


int IrrlichtBase::getWindowWidth()
{
    return _windowWidth;
}


int IrrlichtBase::getWindowHeight()
{
    return _windowHeight;
}
