#ifndef SPRITESCALING_H
#define SPRITESCALING_H


#include "irrlichtbase.h"


using namespace irr;
using namespace video;


IImage* scaleImage(IImage* oldImage, int scaleSize);


#endif // SPRITESCALING_H
