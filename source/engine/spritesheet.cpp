

#include "spritesheet.h"


using namespace std;
using namespace irr;

SpriteSheet::SpriteSheet()
{
    _sheet = 0;
}


void SpriteSheet::loadAndInitSheet
        (
         std::string filePath,
         int sheetWidth,
         int sheetHeight,
         int tailWidth,
         int tailHeight,
         bool useBorder,
         int scaleSize
         )
{
    // SYS INIT
    _sheetWidth  = sheetWidth;
    _sheetHeight = sheetHeight;
    _tailWidth   = tailWidth;
    _tailHeight  = tailHeight;
    _useBorder   = useBorder;

    IVideoDriver* driver = IrrlichtBase::getDriver();


    // SCALE ORIGINAL SPRITE SHEET
    if (scaleSize == 1) {
        _sheet      = driver->getTexture(filePath.c_str());
        _borderSize = _useBorder;
    } else {
        video::IImage* image     = driver->createImageFromFile(filePath.c_str());
        video::IImage* tempImage = image;
        image             = scaleImage(image, scaleSize);
        _sheet            = driver->addTexture("ss", image);
        delete tempImage;

        _borderSize =  useBorder * scaleSize;
        _tailWidth  *= scaleSize;
        _tailHeight *= scaleSize;
    }

    // ACTIVATE ALPHA COLOR - CYAN
    driver->makeColorKeyTexture(_sheet, core::position2d <s32 >(0,0));
}


core::rect<s32> SpriteSheet::getSpriteById(int id)
{
    int width = id % _sheetWidth;
    int height = id / _sheetWidth;

    return core::rect<s32>
    (
        width* _borderSize + width*_tailWidth + _borderSize,
        height* _borderSize + height*_tailHeight + _borderSize,
        width* _borderSize + width*_tailWidth + _tailWidth + _borderSize,
        height* _borderSize + height*_tailHeight + _tailHeight + _borderSize
    );
}


video::ITexture* SpriteSheet::getSheetTexture()
{
    return _sheet;
}




