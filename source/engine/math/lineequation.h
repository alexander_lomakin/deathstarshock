#ifndef LINEEQUATION_H
#define LINEEQUATION_H


#include <cmath>
#include <iostream>
#include <cstdlib>

using namespace std;

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define HALF_M_PI (M_PI * 0.5)

class LineEquation {
public:
    LineEquation();

    LineEquation(float sourceX, float sourceY, float angleDegrees);
    LineEquation(float sourceX, float sourceY, float destX, float destY);

	void init(float sourceX, float sourceY, float angleDegrees);
	void init(float sourceX, float sourceY, float destX, float destY);

	void setCoordByStep(float& x, float& y, float step);
	void setCoordByStep(int& x, int& y, float step);

	void setAngleDeg(float degrees);
	void setAngleRad(float radians);
    float getAngleInDegrees();

    int getStepSum();
private:
	float _srcX; /* Source point of the line (X) */
	float _srcY; /* Source point of the line (Y) */
	float _destX;
	float _destY;
	float _angle; /* Angle in radians */

	// Some optimizations
	float _lastCos; /* Last calculated cos */
	float _lastSin; /* Last calculated sin */
	float _lastX; /* Last calculated position (X) */
	float _lastY;    /* Last calculated position (Y) */
	float _lastAngle; /* Last calculation angle */
	float _lastStep; /* Last calculation step */
};


#endif // LINEEQUATION_H
