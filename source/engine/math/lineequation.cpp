

#include "lineequation.h"


LineEquation::LineEquation() {
	init(0.0f, 0.0f, 0.0f);
}


LineEquation::LineEquation(float sourceX, float sourceY, float angleDegrees) {
	init(sourceX, sourceY, angleDegrees);
}


LineEquation::LineEquation(float sourceX, float sourceY, float destX, float destY) {
	init(sourceX, sourceY, destX, destY);
}


void LineEquation::init(float sourceX, float sourceY, float angleDegrees) {
    _srcX      = sourceX;
	_srcY      = sourceY;
    _destX     = 0.0f;
	_destY     = 0.0f;

	_lastCos   = 1.0f;
	_lastSin   = 0.0f;
	_lastX     = 0.0f;
	_lastY     = 0.0f;
	_lastAngle = 0.0f;
	_lastStep  = 0.0f;
	setAngleDeg(angleDegrees); /* Turn degrees into radians */
}


void LineEquation::init(float sourceX, float sourceY, float destX, float destY) {
    _srcX      = sourceX;
	_srcY      = sourceY;
    _destX     = destX;
	_destY     = destY;

	_lastCos   = 1.0f;
	_lastSin   = 0.0f;
	_lastX     = 0.0f;
	_lastY     = 0.0f;
	_lastAngle = 0.0f;
	_lastStep  = 0.0f;
	setAngleRad(atan2(destY - _srcY, destX - _srcX)  ); /* Turn degrees into radians */
}


void LineEquation::setCoordByStep(float& destX, float& destY, float step)
{
    if (step == 0.0) {
        destX = _srcX;
		destY = _srcY;
		return;
    }

	if(_lastStep == step && _lastAngle == _angle)
	{
		destX = _lastX;
		destY = _lastY;
		return;
	}
	if(_lastAngle != _angle)
	{
		_lastCos = cos(_angle);
		_lastSin = sin(_angle);
	}
	_lastX = _srcX + step * _lastCos;
	_lastY = _srcY + step * _lastSin;
	_lastStep  = step;
	_lastAngle = _angle;
	destX = _lastX;
	destY = _lastY;
}


void LineEquation::setCoordByStep(int& destX, int& destY, float step)
{
    if (step == 0.0) {
        destX = _srcX;
		destY = _srcY;
		return;
    }

	if(_lastStep == step && _lastAngle == _angle)
	{
		destX = (int)rint(_lastX);
		destY = (int)rint(_lastY);
		return;
	}
	if(_lastAngle != _angle)
	{
		_lastCos = cos(_angle);
		_lastSin = sin(_angle);
	}
	_lastX = _srcX + step * _lastCos;
	_lastY = _srcY + step * _lastSin;
	_lastStep  = step;
	_lastAngle = _angle;
	destX = (int)rint(_lastX);
	destY = (int)rint(_lastY);
}


void LineEquation::setAngleDeg(float degrees)
{
	_angle = degrees * (float)M_PI / 180.0f;
}


void LineEquation::setAngleRad(float radians)
{
	cout << "degress:  " <<  radians * 180 / M_PI << endl;
	_angle = radians;
}


float LineEquation::getAngleInDegrees() {
    return _angle * 180.0f / (float)M_PI;
}

int LineEquation::getStepSum() {
    float distX = abs(_srcX - _destX);
    float   distY = abs(_srcY - _destY);

    if (distX > distY)
        return (int)ceil(distX);
    else
        return (int)ceil(distY);
}

