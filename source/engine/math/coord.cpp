

#include "coord.h"



Direction getDirectionByGlobCoord(int globX, int globY)
{
	if (globX >= -AREA_WIDTH && globX < 0 && globY < 0 && globY >= -AREA_HEIGHT)
		return LEFT_TOP;

	if (globX >= 0 && globX < AREA_WIDTH && globY < 0)
		return TOP;

	if (globX >= AREA_WIDTH && globX < 2*AREA_WIDTH && globY < 0)
		return RIGHT_TOP;

	if (globX < 0 && globY >= 0 && globY < AREA_HEIGHT)
		return LEFT;

	if (globX >= 0 && globX < AREA_WIDTH && globY >= 0 && globY < AREA_HEIGHT)
		return CENTER;

	if (globX >= AREA_WIDTH && globX < 2*AREA_WIDTH && globY >= 0 &&  globY < AREA_HEIGHT)
		return RIGHT;

	if (globX < 0 && globY >= AREA_HEIGHT && globY < 2*AREA_HEIGHT)
		return LEFT_BOTTOM;

	if (globX >= 0 && globX < AREA_WIDTH && globY >= AREA_HEIGHT && globY < 2*AREA_HEIGHT)
		return BOTTOM;

	if (globX >= AREA_WIDTH && globX < 2*AREA_WIDTH && globY >= AREA_HEIGHT && globY < 2*AREA_HEIGHT)
		return RIGHT_BOTTOM;

	return CENTER;
}


void setAreaCoordByDirectionAndCentralCoord(int centralX, int centralY, Direction direction, int& areaX, int& areaY)
{
    switch (direction)
	{
		case LEFT:
			areaX = centralX - 1;
			areaY = centralY;
			break;
		case RIGHT:
			areaX = centralX + 1;
			areaY = centralY;
			break;
		case TOP:
			areaX = centralX;
			areaY = centralY - 1;
			break;
		case BOTTOM:
			areaX = centralX;
			areaY = centralY + 1;
			break;
		case LEFT_TOP:
			areaX = centralX - 1;
			areaY = centralY - 1;
			break;
		case RIGHT_TOP:
			areaX = centralX + 1;
			areaY = centralY - 1;
			break;
		case LEFT_BOTTOM:
			areaX = centralX - 1;
			areaY = centralY + 1;
			break;
		case RIGHT_BOTTOM:
			areaX = centralX + 1;
			areaY = centralY + 1;
			break;
		case CENTER:
			areaX = centralX;
			areaY = centralY;
			break;
	}
}


void setGlobalCoordByDir(int localX, int localY, Direction direction, int& globX, int& globY)
{
	switch (direction)
	{
		case LEFT:
			globX = localX - AREA_WIDTH;
			globY = localY;
			break;
		case RIGHT:
			globX = localX + AREA_WIDTH;
			globY = localY;
			break;
		case TOP:
			globX = localX;
			globY = localY - AREA_HEIGHT;
			break;
		case BOTTOM:
			globY = localY + AREA_HEIGHT;
			globX = localX;
			break;
		case LEFT_TOP:
			globX = localX - AREA_WIDTH;
			globY = localY - AREA_HEIGHT;
			break;
		case RIGHT_TOP:
			globX = localX + AREA_WIDTH;
			globY = localY - AREA_HEIGHT;
			break;
		case LEFT_BOTTOM:
			globX = localX - AREA_WIDTH;
			globY = localY + AREA_HEIGHT;
			break;
		case RIGHT_BOTTOM:
			globX = localX + AREA_WIDTH;
			globY = localY + AREA_HEIGHT;
			break;
		case CENTER:
			globX = localX;
			globY = localY;
			break;
	}
}


void setLocalCoordByDir(int globX, int globY, Direction direction, int& localX, int& localY)
{
    switch (direction)
	{
		case LEFT:
			localX = AREA_WIDTH + globX;
			localY = globY;
			break;
		case RIGHT:
			localX = globX - AREA_WIDTH;
			localY = globY;
			break;
		case TOP:
			localX = globX;
			localY = AREA_HEIGHT + globY;
			break;
		case BOTTOM:
		    localX = globX;
			localY = globY - AREA_HEIGHT;
			break;
		case LEFT_TOP:
			localX = AREA_WIDTH + globX;
			localY = AREA_HEIGHT + globY;
			break;
		case RIGHT_TOP:
			localX = globX - AREA_WIDTH;
			localY = AREA_HEIGHT + globY;
			break;
		case LEFT_BOTTOM:
			localX = AREA_WIDTH + globX;
			localY = globY - AREA_HEIGHT;
			break;
		case RIGHT_BOTTOM:
			localX = globX - AREA_WIDTH;
			localY = globY - AREA_HEIGHT;
			break;
		case CENTER:
			localX = globX;
			localY = globY;
			break;
	}
}


bool isLocalCoordCorrect(int localX, int localY)
{
    if (localX >= 0 && localX < AREA_WIDTH && localY >= 0 && localY < AREA_HEIGHT)
        return true;
    return false;
}


void updateCoordByDir(int& globX, int& globY, Direction direction) {
    switch (direction) {
		case LEFT: {
			globX--;
        } break;
		case RIGHT: {
			globX++;
        } break;
		case TOP: {
			globY--;
        } break;
		case BOTTOM: {
			globY++;
        } break;
        case LEFT_BOTTOM: {
            globX--;
            globY++;
		} break;
		case RIGHT_BOTTOM: {
            globY++;
            globX++;
		} break;
		case LEFT_TOP: {
            globY--;
            globX--;
		} break;
		case RIGHT_TOP: {
            globY--;
            globX++;
		} break;
	}
}


Direction getDirectionBetweenCoord(int globX1, int globY1, int globX2, int globY2) {
    if (globX2 < globX1 && globY2 < globY1) {
        return LEFT_TOP;
    }

    if (globX2 == globX1 && globY2 < globY1) {
        return TOP;
    }

    if (globX2 > globX1 && globY2 < globY1) {
        return RIGHT_TOP;
    }

    if (globX2 < globX1 && globY2 == globY1) {
        return LEFT;
    }

    if (globX2 == globX1 && globY2 == globY1) {
        return CENTER;
    }

    if (globX2 > globX1 && globY2 == globY1) {
        return RIGHT;
    }

    if (globX2 < globX1 && globY2 > globY1) {
        return LEFT_BOTTOM;
    }

    if (globX2 == globX1 && globY2 > globY1) {
        return BOTTOM;
    }

    if (globX2 > globX1 && globY2 > globY1) {
        return RIGHT_BOTTOM;
    }

    return CENTER;
}
