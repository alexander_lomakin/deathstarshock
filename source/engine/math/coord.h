#ifndef COORD_H_INCLUDED
#define COORD_H_INCLUDED


enum Direction { CENTER = 0, LEFT, RIGHT, TOP, BOTTOM, RIGHT_TOP, LEFT_TOP, LEFT_BOTTOM, RIGHT_BOTTOM };


#define AREA_HEIGHT 20
#define AREA_WIDTH 20


Direction getDirectionByGlobCoord(int globX, int globY);
void setAreaCoordByDirectionAndCentralCoord(int centralX, int centralY, Direction direction, int& areaX, int& areaY);
void setGlobalCoordByDir(int localX, int localY, Direction direction, int& globX, int& globY);
void setLocalCoordByDir(int globX, int globY, Direction direction, int& localX, int& localY);

bool isLocalCoordCorrect(int localX, int localY);

void updateCoordByDir(int& globX, int& globY, Direction direction);
Direction getDirectionBetweenCoord(int globX1, int globY1, int globX2, int globY2);


#endif // COORD_H_INCLUDED
