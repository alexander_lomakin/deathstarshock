#ifndef DICE_H
#define DICE_H


#include <cstdlib>
#include <ctime>


    class Dice
    {
    public:
        Dice();
        static int dmgDiceFormula(int diceSum, int diceSize, int additPoints);
        static int D100();
        static int getRandomValue(int mn, int mx);
        static int RandomRange(int length);
    };


#endif // DICE_H
