

#include "dice.h"


Dice::Dice() {
    srand(time(0));
}


int Dice::dmgDiceFormula(int diceSum, int diceSize, int additPoints)
{
    int val = 0;
    for (int i = 0; i < diceSum; i++)
        val += getRandomValue(1, diceSize);

    val += additPoints;

    return val;
}


int Dice::getRandomValue(int mn, int mx)
{
    return rand()%(mx - mn) + mn;
}


int Dice::D100()
{
    return getRandomValue(1, 100);
}


int Dice::RandomRange(int length) {
    length = abs(length);

    if (length == 0) {
        return 0;
    }

    return getRandomValue(-length, length);
}
