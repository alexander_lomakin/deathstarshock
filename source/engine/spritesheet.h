#ifndef SPRITESHEET_H_INCLUDED
#define SPRITESHEET_H_INCLUDED


#include <string>
#include "irrlichtbase.h"
#include "spritescaling.h"


using namespace std;
using namespace irr;


    class SpriteSheet {


    public:
        SpriteSheet();

        void loadAndInitSheet
        (
         std::string filePath,
         int sheetWidth,
         int sheetHeight,
         int tailWidth,
         int tailHeight,
         bool useBorder,
         int scaleSize
         );

        core::rect<s32> getSpriteById(int id);
        video::ITexture* getSheetTexture();
    private:
        video::ITexture* _sheet;

        int _sheetWidth;
        int _sheetHeight;
        int _tailWidth;
        int _tailHeight;
        int _borderSize;
        bool _useBorder;
    };


#endif // SPRITESHEET_H_INCLUDED
