

#include "semaphore.h"


Semaphore::Semaphore() {
    _users  = 0;
    _now    = 0;
}


void Semaphore::init(int usersNum) {
    _users = usersNum;
    _now   = _users;
}


void Semaphore::enter() {
    while (_now == 0) {
        irr::IrrlichtDevice* _device = IrrlichtBase::getDevice();
        //_device->sleep(1);
        continue;
    }

    _now--;
}


void Semaphore::leave() {
    _now++;
}
