

#include "colors.h"


using namespace irr;
using namespace video;
using namespace std;


COLOR STC(string color)
{
    if (color == "Black")
        return BLACK;
    if (color == "Red")
        return RED;
    if (color == "DarkRed")
        return DARK_RED;
    if (color == "Yellow")
        return YELLOW;
    if (color == "DarkYellow")
        return DARK_YELLOW;
    if (color == "White")
        return WHITE;
    if (color == "Gray")
        return GRAY;
    if (color == "DarkGray")
        return DARK_GRAY;
    if (color == "Green")
        return GREEN;
    if (color == "DarkGreen")
        return DARK_GREEN;
    if (color == "Cyan")
        return CYAN;
    if (color == "DarkCyan")
        return DARK_CYAN;
    if (color == "Pink")
        return PINK;
    if (color == "DarkPink")
        return DARK_PINK;
    if (color == "Blue")
        return BLUE;
    if (color == "DarkBlue")
        return DARK_BLUE;

    return BLACK;
}


SColor CTSC(COLOR color)
{
    if (color == BLACK)
        return SColor(255, 0, 0, 0);
    if (color == RED)
        return SColor(255, 239, 0, 0);
    if (color == DARK_RED)
        return SColor(255, 170, 0, 0);
    if (color == YELLOW)
        return SColor(255, 255, 255, 85);
    if (color == DARK_YELLOW)
        return SColor(255, 127, 106, 0);
    if (color == WHITE)
        return SColor(255, 255, 255, 255);
    if (color == GRAY)
        return SColor(255, 170, 170, 170);
    if (color == DARK_GRAY)
        return SColor(255, 85, 85, 85);
    if (color == GREEN)
        return SColor(255, 0, 170, 0);
    if (color == DARK_GREEN)
        return SColor(255, 0, 85, 0);
    if (color == CYAN)
        return SColor(255, 85, 255, 255);
    if (color == DARK_CYAN)
        return SColor(255, 0, 170, 170);
    if (color == PINK)
        return SColor(255, 255, 188, 225);
    if (color == DARK_PINK)
        return SColor(255, 145, 107, 128);
    if (color == BLUE)
        return SColor(255, 85, 85, 255);
    if (color == DARK_BLUE)
        return SColor(255, 0, 0, 156);

    return SColor(255, 0, 0, 0);
}


SColor CTSC(int alpha, COLOR color)
{
    if (color == BLACK)
        return SColor(alpha, 0, 0, 0);
    if (color == RED)
        return SColor(alpha, 239, 0, 0);
    if (color == DARK_RED)
        return SColor(alpha, 170, 0, 0);
    if (color == YELLOW)
        return SColor(alpha, 255, 255, 85);
    if (color == DARK_YELLOW)
        return SColor(alpha, 127, 106, 0);
    if (color == WHITE)
        return SColor(alpha, 255, 255, 255);
    if (color == GRAY)
        return SColor(alpha, 170, 170, 170);
    if (color == DARK_GRAY)
        return SColor(alpha, 85, 85, 85);
    if (color == GREEN)
        return SColor(alpha, 0, 170, 0);
    if (color == DARK_GREEN)
        return SColor(alpha, 0, 85, 0);
    if (color == CYAN)
        return SColor(alpha, 85, 255, 255);
    if (color == DARK_CYAN)
        return SColor(alpha, 0, 170, 170);
    if (color == PINK)
        return SColor(alpha, 255, 188, 225);
    if (color == DARK_PINK)
        return SColor(alpha, 145, 107, 128);
    if (color == BLUE)
        return SColor(alpha, 85, 85, 255);
    if (color == DARK_BLUE)
        return SColor(alpha, 0, 0, 170);

    return SColor(alpha, 0, 0, 0);
}
