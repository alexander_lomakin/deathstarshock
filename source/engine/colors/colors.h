#ifndef COLORS_H
#define COLORS_H


#include <irrlicht.h>
#include <string>


using namespace irr;
using namespace video;
using namespace std;


enum COLOR
{
        BLACK = 0,
        RED,
        DARK_RED,
        YELLOW,
        DARK_YELLOW,
        WHITE,
        GRAY,
        DARK_GRAY,
        GREEN,
        DARK_GREEN,
        CYAN,
        DARK_CYAN,
        PINK,
        DARK_PINK,
        BLUE,
        DARK_BLUE
};


COLOR STC(string color);
SColor CTSC(COLOR color);
SColor CTSC(int alpha, COLOR color);


#endif // COLORS_H
