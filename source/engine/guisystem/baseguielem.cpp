

#include "baseguielem.h"


using namespace irr;
using namespace video;
using namespace std;


BaseGuiElem::BaseGuiElem()
{
    _draw  = true;
    _leftX = 0;
    _leftY = 0;
}


void BaseGuiElem::drawElem(bool drawElem)
{
    _draw = drawElem;
}


void BaseGuiElem::setPosition(int nX, int nY)
{
    _leftX = nX;
    _leftY = nY;
}
