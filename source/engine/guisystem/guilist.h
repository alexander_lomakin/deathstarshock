#ifndef GUILIST_H
#define GUILIST_H


#include "baseguielem.h"
#include "guilistelem.h"


using namespace irr;
using namespace video;
using namespace std;

    class GuiList : public BaseGuiElem
    {
    public:
        GuiList();

        void draw(IVideoDriver* _driver);

        void setTitle(std::string nTitle, COLOR color);
        void setSize(int nWidth, int nHeight);

        void useDoublePointer(bool use);
        void alignCenter(bool align);

        void clearList();

        void addItem(std::string text, COLOR color);

        void setSelectedItem(int nSelected);
        int getSelectedItem();

    private:
        std::string _title;
        COLOR  _titleColor;

        vector<GuiListElem> _items;

        int _width;
        int _height;

        int _selectedItem;

        bool _doublePointer;
        bool _alignCenter;
    };



#endif // GUILIST_H
