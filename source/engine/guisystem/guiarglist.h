#ifndef GUIARGLIST_H
#define GUIARGLIST_H


#include "baseguielem.h"
#include "guiarglistelem.h"
#include "../its.h"


using namespace irr;
using namespace video;
using namespace std;

    class GuiArgList : public BaseGuiElem
    {
    public:
        GuiArgList();

        void draw(IVideoDriver* _driver);

        void setTitle(std::string nTitle, COLOR color);
        void setSize(int nWidth, int nHeight);

        void setValOffset(int nValOffset);

        void useDoublePointer(bool use);
        void alignCenter(bool align);

        void clearList();

        void addItem(std::string text, COLOR color, int value);

        void setSelectedItem(int nSelected);
        int getSelectedItem();

    private:
        std::string _title;
        COLOR  _titleColor;

        vector<GuiArgListElem> _items;

        int _width;
        int _valueOffset;
        int _height;

        int _selectedItem;

        bool _doublePointer;
        bool _alignCenter;
    };



#endif // GUIARGLIST_H
