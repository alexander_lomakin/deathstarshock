#ifndef GUIMANAGER_H
#define GUIMANAGER_H


#include "baseguielem.h"


using namespace irr;
using namespace video;
using namespace std;



    class GuiManager
    {
    public:
        ~GuiManager();

        void draw(IVideoDriver* _driver);

        void addElem(BaseGuiElem* elem);
    private:
        vector<BaseGuiElem*> _elements;
    };



#endif // GUIMANAGER_H
