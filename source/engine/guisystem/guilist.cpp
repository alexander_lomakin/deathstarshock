

#include "guilist.h"


using namespace irr;
using namespace video;
using namespace std;


GuiList::GuiList()
{
    _title      = "";
    _titleColor = WHITE;

    _leftX = 0;
    _leftY = 0;

    _width  = 0;
    _height = 0;

    _doublePointer = false;
    _alignCenter   = false;

    _selectedItem = -1;
}


void GuiList::draw(IVideoDriver* _driver)
{
    if (!_draw)
        return;

    _driver->draw2DRectangle(CTSC(100, DARK_BLUE), core::rect < s32 >(_leftX, _leftY, _leftX+_width, _leftY + _height));

    _driver->draw2DLine(core::position2d<s32>(_leftX-1, _leftY-1),core::position2d< s32 >(_leftX-1, _leftY+_height),CTSC(DARK_GRAY));
    _driver->draw2DLine(core::position2d<s32>(_leftX-1, _leftY-1),core::position2d< s32 >(_leftX+_width, _leftY-1),CTSC(DARK_GRAY));
    _driver->draw2DLine(core::position2d<s32>(_leftX+_width, _leftY-1),core::position2d< s32 >(_leftX+_width, _leftY+_height),CTSC(DARK_GRAY));
    _driver->draw2DLine(core::position2d<s32>(_leftX-1, _leftY+_height),core::position2d< s32 >(_leftX+_width, _leftY+_height),CTSC(DARK_GRAY));

    Font::drawBig(_leftX+10, _leftY+10, _title, CTSC(_titleColor));

    for (int i = 0; i < _items.size(); i++)
    {
        if (i == _selectedItem) {
            if (_doublePointer == true) {
                Font::drawBig(_leftX+5, _leftY+30+i*20, "# "+_items[i].text+" #", CTSC(WHITE));
            } else {
                Font::drawBig(_leftX+5, _leftY+30+i*20, "# "+_items[i].text, CTSC(WHITE));
            }
        } else {
            Font::drawBig(_leftX+10, _leftY+30+i*20, _items[i].text, CTSC(_items[i].textColor));
        }
    }
}


void GuiList::setTitle(string nTitle, COLOR color)
{
    _title      = nTitle;
    _titleColor = color;
}


void GuiList::setSize(int nWidth, int nHeight)
{
    _width  = nWidth;
    _height = nHeight;
}


void GuiList::useDoublePointer(bool use)
{
    _doublePointer = use;
}


void GuiList::alignCenter(bool align)
{
    _alignCenter = align;
}


void GuiList::clearList()
{
    _items.clear();
}


void GuiList::addItem(string text, COLOR color)
{
    GuiListElem elem;

    elem.text      = text;
    elem.textColor = color;

    _items.push_back(elem);
}


void GuiList::setSelectedItem(int nSelected)
{
    _selectedItem = nSelected;
}


int GuiList::getSelectedItem()
{
    return _selectedItem;
}
