#ifndef GUISTATICTEXT_H
#define GUISTATICTEXT_H


#include "baseguielem.h"


using namespace irr;
using namespace video;
using namespace std;



    class GuiStaticText : public BaseGuiElem
    {
    public:
        GuiStaticText();

        void draw(IVideoDriver* _driver);

        void setText(std::string nText, COLOR nColor);
    private:
        std::string _text;
        COLOR _textColor;
    };



#endif // GUISTATICTEXT_H
