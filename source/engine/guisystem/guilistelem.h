#ifndef GUILISTELEM_H
#define GUILISTELEM_H


#include <string>

#include "../colors/colors.h"


using namespace std;


    struct GuiListElem
    {
        std::string text;
        COLOR textColor;
    };


#endif // GUILISTELEM_H
