

#include "guiprogressbar.h"

using namespace irr;
using namespace video;
using namespace std;


GuiProgressBar::GuiProgressBar()
{
    _progressMax = 0;
    _progressNow = 0;
    _doRise      = false;
    _speed       = 0;
    _text        = " ";
}


void GuiProgressBar::draw(IVideoDriver* _driver)
{
    if (!_draw)
        return;

    if (_doRise) {
        if (_progressNow + _speed <= _progressMax + 1) {
            _progressNow += _speed;
        } else {
            _progressNow = _progressMax;
        }
    }

    for (int i = 0; i < _progressMax; i++)
    {
        if (i < _progressNow)
        {
            _driver->draw2DRectangle(CTSC(100, _main), core::rect < s32 >(_leftX + i, _leftY, _leftX + i+1, _leftY + 12));
        }
        else
            _driver->draw2DRectangle(CTSC(100, _addit), core::rect < s32 >(_leftX + i, _leftY, _leftX + i+1, _leftY + 12));
    }

    if (_text != " ") {
            Font::drawSmall ((_leftX+_progressMax/2) - (_text.length()*2), _leftY+1, _text, CTSC(GRAY));
    }
}


void GuiProgressBar::setProgressMax(int nProgressMax)
{
    _progressMax = nProgressMax;
}


void GuiProgressBar::setProgressNow(int nProgressNow)
{
    _progressNow = nProgressNow;
}


void GuiProgressBar::setColors(COLOR main, COLOR addit) {
    _main  = main;
    _addit = addit;
}


void GuiProgressBar::setRising(bool doRise, int speed) {
    _doRise = doRise;
    _speed  = speed;
}


int GuiProgressBar::getProgressNow() {
    return _progressNow;
}


int GuiProgressBar::getProgressMax() {
    return _progressMax;
}


void GuiProgressBar::setText(string nText) {
    _text = nText;
}
