#ifndef GUIPROGRESSBAR_H
#define GUIPROGRESSBAR_H


#include "baseguielem.h"
#include "../colors/colors.h"


using namespace irr;
using namespace video;
using namespace std;


    class GuiProgressBar : public BaseGuiElem
    {
    public:
        GuiProgressBar();

        void draw(IVideoDriver* _driver);

        void setProgressMax(int nProgressMax);
        void setProgressNow(int nProgressNow);

        void setColors(COLOR main, COLOR addit);

        void setRising(bool doRise, int speed);

        void setText(string nText);

        int getProgressNow();
        int getProgressMax();
    private:
        int _progressMax;
        int _progressNow;

        string _text;

        bool _doRise;
        int _speed;

        COLOR _main;
        COLOR _addit;
    };


#endif // GUIPROGRESSBAR_H
