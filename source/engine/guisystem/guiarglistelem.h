#ifndef GUIARGLISTELEM_H
#define GUIARGLISTELEM_H


#include <string>

#include "../colors/colors.h"


using namespace std;


    struct GuiArgListElem
    {
        std::string text;
        COLOR textColor;
        int value;
    };


#endif // GUIARGLISTELEM_H
