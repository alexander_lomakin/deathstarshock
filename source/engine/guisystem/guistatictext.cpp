

#include "guistatictext.h"


using namespace irr;
using namespace video;
using namespace std;


GuiStaticText::GuiStaticText()
{
    _text      = "";
    _textColor = WHITE;
}


void GuiStaticText::draw(IVideoDriver* _driver)
{
    if (!_draw)
        return;

    Font::drawBig(_leftX, _leftY, _text, CTSC(_textColor));
}


void GuiStaticText::setText(string nText, COLOR nColor)
{
    _text      = nText;
    _textColor = nColor;
}
