

#include "guimanager.h"


using namespace irr;
using namespace video;
using namespace std;


GuiManager::~GuiManager()
{
    for (int i = 0; i < _elements.size(); i++)
        delete _elements[i];
}


void GuiManager::draw(IVideoDriver* _driver)
{
    for (int i = 0; i < _elements.size(); i++)
        _elements[i]->draw(_driver);
}


void GuiManager::addElem(BaseGuiElem* elem)
{
    _elements.push_back(elem);
}
