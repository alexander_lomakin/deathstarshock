#ifndef BASEGUIELEM_H
#define BASEGUIELEM_H


#include <vector>
#include <string>

#include "../irrlichtbase.h"
#include "../font.h"

#include "../colors/colors.h"


using namespace irr;
using namespace video;
using namespace std;


    class BaseGuiElem
    {
    public:
        BaseGuiElem();

        virtual void draw(IVideoDriver* _driver) = 0;
        void drawElem(bool drawElem);
        void setPosition(int nX, int nY);
    protected:
        int _leftX;
        int _leftY;
        bool _draw;
    };



#endif // BASEGUIELEM_H
