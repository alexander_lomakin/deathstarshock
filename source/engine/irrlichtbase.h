#ifndef IRRLICHTBASE_H
#define IRRLICHTBASE_H


#include <irrlicht.h>

#include <stdlib.h>
#include <iostream>
#include <string>

#include "eventreceiver.h"

#include "audiere.h"


using namespace irr;
using namespace video;
using namespace std;
using namespace audiere;


    class IrrlichtBase
    {
    public:
        IrrlichtBase(int width, int height, bool fullscreen);

        static void setFramesPerSecond(int nFps);

        static void setCaption(string caption);

        static void startCalcFrames();
        static void checkFramesPerSecond();

        static IrrlichtDevice* getDevice();
        static IVideoDriver*   getDriver();
        static AudioDevicePtr* getAudioDevice();
        static void drop();

        static int getWindowWidth();
        static int getWindowHeight();


        static void sound(string soundFilePath);

        static ITexture* loadTexture(string path);
    private:
        static IrrlichtDevice* _device;
        static IVideoDriver* _driver;
        static EventReceiver _evReceiver;
        static AudioDevicePtr _soundDevice;

        static int _windowWidth;
        static int _windowHeight;

        static int _framesPerSecond;
    };



#endif // IRRLICHTBASE_H
