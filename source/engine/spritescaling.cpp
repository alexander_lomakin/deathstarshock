

#include "spritescaling.h"


using namespace irr;
using namespace video;
using namespace core;



IImage* scaleImage(IImage* oldImage, int scaleSize) {
    // SYS INIT
    video::IVideoDriver* driver    = IrrlichtBase::getDriver();
    core::dimension2d<u32> oldSize = oldImage->getDimension();
    int oldWidth  = oldSize.Width;
    int oldHeight = oldSize.Height;

    // CHECKING SCALE SIZE. NEED 2 POW
    bool correctSize = false;
    for (int i = 2; i <= 256; i *= 2) {
        if (scaleSize == i) {
            correctSize = true;
            break;
        }
    }

    if (correctSize == false) {
        scaleSize = 1;
    }

    // CREATE RESULT IMAGE
    int newWidth  = oldWidth  * scaleSize;
    int newHeight = oldHeight * scaleSize;
    IImage* resultImage = driver->createImage(ECF_A8R8G8B8, position2d< u32 >(newWidth, newHeight));

    // SCALE PIXELS FROM OLD TO NEW
    for (int i = 0; i < oldWidth; i++) {
        for (int j = 0; j < oldHeight; j++) {
            int locMaxX = (i+1) * scaleSize;
            int locMaxY = (j+1) * scaleSize;
            for (int locX = i * scaleSize; locX < locMaxX; locX++) {
                for (int locY = j * scaleSize; locY < locMaxY; locY++) {
                    resultImage->setPixel(locX, locY, oldImage->getPixel(i, j));
                }
            }
        }
    }

    return resultImage;
}
