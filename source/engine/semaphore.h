#ifndef SEMAPHORE_H
#define SEMAPHORE_H


#include "irrlichtbase.h"


class Semaphore
{
    public:
        Semaphore();
        void init(int usersNum);
        void enter();
        void leave();
    private:
        int _users;
        int _now;
};

#endif // SEMAPHORE_H
