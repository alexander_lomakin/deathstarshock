

#include "font.h"


using namespace std;

gui::IGUIFont* Font::_smallFont = 0;
gui::IGUIFont* Font::_bigFont = 0;


void Font::loadAndInitFont(string pathToFont)
{
     IrrlichtDevice* device = IrrlichtBase::getDevice();
    _smallFont = device->getGUIEnvironment()->getBuiltInFont();
    _bigFont = device->getGUIEnvironment()->getFont(pathToFont.c_str());
}


void Font::drawSmall(int x, int y, string txt, video::SColor color)
{
    _smallFont->draw( txt.c_str(), core::rect < s32 >(x, y, x + 500, y + 40), color);
}


void Font::drawBig(int x, int y, string txt, video::SColor color)
{
    _bigFont->draw( txt.c_str(), core::rect < s32 >(x, y, x + 500, y + 40), color);
}


