#ifndef FONT_H_INCLUDED
#define FONT_H_INCLUDED


#include <string>
#include "irrlichtbase.h"


using namespace irr;
using namespace std;



    class Font
    {
    public:
        static void loadAndInitFont(string pathToFont);
        static void drawSmall(int x, int y, string txt, video::SColor color);
        static void drawBig(int x, int y, string txt, video::SColor color);
    private:
        static gui::IGUIFont* _smallFont;
        static gui::IGUIFont* _bigFont;
    };



#endif // FONT_H_INCLUDED
