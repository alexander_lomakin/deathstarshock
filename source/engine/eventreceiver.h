#ifndef EVENTRECEIVER_H_INCLUDED
#define EVENTRECEIVER_H_INCLUDED


#include <irrlicht.h>
#include <ctime>

#include "boost/date_time/gregorian/gregorian.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"


using namespace irr;


    class EventReceiver : public IEventReceiver
    {
    public:
        EventReceiver();

        bool OnEvent(const SEvent& event);
        static bool isKeyDown(EKEY_CODE keyCode);
        static void setTimer(ITimer* timer);
    private:
        static ITimer* _timer;
        static int  _keyRepeatTime;
        static int  _keyDelayTime;
        static bool _keysIsDown[KEY_KEY_CODES_COUNT];
        static boost::posix_time::ptime  _keysPressTime[KEY_KEY_CODES_COUNT];
        static bool _keysPressedInPressTime[KEY_KEY_CODES_COUNT];
        static boost::posix_time::ptime  _keysRepeatTime[KEY_KEY_CODES_COUNT];
    };



#endif // EVENTRECEIVER_H_INCLUDED
