#ifndef TURNCONTROLLER_H_
#define TURNCONTROLLER_H_


#include <iostream>


#include "demonmind/demonmind.h"
#include "monsters/controlledcreature.h"


using namespace std;


class GameEnd : public WaitingCarcass {
    public:
        GameEnd(Loaders* loader);
};


void beastsActionLoop(Loaders* loader, GameLogic* gmLogic, bool *isGameEnd);
bool isEndOfGame(Loaders* loader);
void gameLoop(Loaders* loader, GameLogic* gmLogic, DemonMind* demonMind, bool* endOfWork);


#endif
