

#include "waitingcarcass.h"


void WaitingCarcass::_waitingForDrawer(Loaders* loader) {
    loader->semaphore.leave();

    while (true) {
        if (EventStack::canPopModelEvent()) {
            ModelEvent event = EventStack::popModelEvent();

            if (event.type == CONTINUE_WORK || event.type == END_OF_WORK)
                break;
        }

        boost::this_thread::sleep(boost::posix_time::milliseconds(1));
    }

    loader->semaphore.enter();
}


void WaitingCarcass::_waitingForPlayerAction(Loaders* loader) {
    loader->semaphore.leave();

    while (true) {
        if (EventStack::canPopModelEvent()) {
            ModelEvent event = EventStack::popModelEvent();
            if (event.type != CONTINUE_WORK) {
                EventStack::pushEvent(event);
                break;
            }
        }

        boost::this_thread::sleep(boost::posix_time::milliseconds(1));
    }

    loader->semaphore.enter();
}


void WaitingCarcass::_pause(int milliseconds) {
    IrrlichtDevice* device = IrrlichtBase::getDevice();
    device->sleep(milliseconds);
}
