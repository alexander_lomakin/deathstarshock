#ifndef MODELEVENT_H
#define MODELEVENT_H


#include <string>


using namespace std;



    enum ModelEventType { MOVE_PLAYER, WAIT, HARD_PUNCH, RUN_MODE, GET_ITEM,
                          INVENTORY, DROP_ITEM, EQUIP_ITEM, GUNSHOT, RELOAD, SWAP_WEAPON,
                          CONTINUE_WORK, END_OF_WORK, ASSAULT_FORWARD, CLOSE, USE_ITEM, ENTER,
                          HACK_TERMINAL, CHAR_LIST };


    class ModelEvent
    {
    public:
        ModelEvent(ModelEventType nType, int nA, int nB, int nC, int nD, string nS)
        {
            type = nType;
            argA = nA;
            argB = nB;
            argC = nC;
            argD = nD;
            argS = nS;
        }

        ModelEventType type;

        int argA;
        int argB;
        int argC;
        int argD;
        string argS;
    };



#endif // MODELEVENT_H
