#ifndef WAITINGCARCASS_H
#define WAITINGCARCASS_H


#include <boost/thread/thread.hpp>

#include "eventstack.h"

#include "loaders/loaders.h"


    class WaitingCarcass {
    protected:
        void _waitingForDrawer(Loaders* loader);
        void _waitingForPlayerAction(Loaders* loader);
        void _pause(int milliseconds);
    };


#endif // WAITINGCARCASS_H
