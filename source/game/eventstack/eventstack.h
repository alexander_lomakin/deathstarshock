#ifndef EVENTSTACK_H
#define EVENTSTACK_H


#include <list>

#include "viewevent.h"
#include "modelevent.h"


using namespace std;


    class EventStack
    {
    public:
        static ModelEvent popModelEvent();
        static bool canPopModelEvent();

        static ViewEvent popViewEvent();
        static bool canPopViewEvent();

        static void pushEvent(ViewEvent event);
        static void pushEvent(ModelEvent event);

        static void clearModelStack();

        static void clearStacks();
    private:
        static list<ViewEvent>  _viewStack;
        static list<ModelEvent> _modelStack;
    };



#endif // EVENTSTACK_H
