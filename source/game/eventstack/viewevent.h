#ifndef VIEWEVENT_H
#define VIEWEVENT_H


#include <string>


using namespace std;


    enum ViewEventType { AREA_LOADED, ACTIVEOBJ_CREATED, MOVE_BEAST, CHANGE_BEAST_DIR, BEAST_ATTACK, CHAT_MSG,
                        DELETE_BEAST, SET_TARGET_BEAST, RECALC_LOS, CHANGE_BEAST_POS,
                        BEAST_INJURED, BEAST_GET_EXP, PLAYER_TIREDNESS, PLAYER_STATUS,
                        ITEM_CREATED, ITEM_DELETED, DRAW_ITEM_LIST, ADD_ITEM_ELEM, SELECT_NEW_ELEM,
                        DELETE_LIST_ITEM, EQUIP_WEAPON, EQUIP_ARMOR, SINGLE_SHOOT,
                        SHRAPNEL_SHOOT, UPDATE_CELL_SPRITE, SINGLE_SHOOT_MISS, PLAY_SOUND,
                        BEAST_DEAD, SHRAPNEL_SHOOT_MISS, START_HACK, DRAW_CHARLIST };


    class ViewEvent
    {
    public:
        ViewEvent(ViewEventType nType, int nA, int nB, int nC, int nD, string nS)
        {
            type = nType;
            argA = nA;
            argB = nB;
            argC = nC;
            argD = nD;
            argS = nS;
        }

        ViewEventType type;

        int argA;
        int argB;
        int argC;
        int argD;
        string argS;
    };



#endif // VIEWEVENT_H
