

#include "eventstack.h"


using namespace std;


list<ViewEvent> EventStack::_viewStack;
list<ModelEvent> EventStack::_modelStack;


ModelEvent EventStack::popModelEvent()
{
     ModelEvent mv = _modelStack.back();
    _modelStack.pop_back();
    return mv;
}


bool EventStack::canPopModelEvent()
{
    return (!_modelStack.empty());
}



ViewEvent EventStack::popViewEvent()
{
    ViewEvent mv = _viewStack.back();
    _viewStack.pop_back();
    return mv;
}


bool EventStack::canPopViewEvent()
{
    return (!_viewStack.empty());
}


void EventStack::pushEvent(ViewEvent event)
{
    _viewStack.push_front(event);
}


void EventStack::pushEvent(ModelEvent event)
{
    _modelStack.push_back(event);
}


void EventStack::clearModelStack()
{
    _modelStack.clear();
}


void EventStack::clearStacks() {
    _modelStack.clear();
    _viewStack .clear();
}
