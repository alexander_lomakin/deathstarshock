#ifndef GHOUL_H
#define GHOUL_H


#include "controlledcreature.h"
#include "mechanics/gamelogic.h"
#include "math/dice.h"


#include <iostream>


class Ghoul : public ControlledCreature {
    public:
        Ghoul(GameLogic* gmLogic);
        void step(GameLogic* gmLogic, Loaders* loader);
        void onDeath();
    private:
        int _followPlayer;
};


#endif // GHOUL_H
