#ifndef PLAYER_H
#define PLAYER_H


#include "controlledcreature.h"
#include "mechanics/gamelogic.h"
#include "math/coord.h"
#include "math/lineequation.h"


class Player : public ControlledCreature {
    public:
        void step(GameLogic* gmLogic, Loaders* loader);
        void onDeath();
    protected:
        void _inventory(GameLogic* gmLogic, Loaders* loader);
        void _updateInventoryInfoAndGetItemSum(const int curPos);
        void _moveInventoryPointer(int& curPointer, const Direction direction);
        void _recheckInventoryPointer(int& curPointer);

        void _getItem(GameLogic* gmLogic, Loaders* loader);

        void _updatePlayerInfo();
        void _charList(GameLogic* gmLogic, Loaders* loader);

        bool _runMode();
        bool _movePlayer(GameLogic* gmLogic, Direction direction);
        void _updateRunMode();
        bool _shooting(GameLogic* gmLogic, Loaders* loader);
        bool _hackTerminal(GameLogic* gmLogic, Loaders* loader);
};


#endif // PLAYER_H
