#ifndef CONTROLLEDCREATURE_H_
#define CONTROLLEDCREATURE_H_


#include "objects/creature.h"
#include "mechanics/gamelogic.h"
#include "eventstack/waitingcarcass.h"


class ControlledCreature : public Creature, public WaitingCarcass {
    public:
        ControlledCreature() {
            ;
        }

        virtual void onDeath() {
            ;
        }

        virtual void step(GameLogic* gmLogic, Loaders* loader) {
            ;
        }
};


#endif
