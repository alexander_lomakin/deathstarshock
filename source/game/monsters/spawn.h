#ifndef SPAWN_H
#define SPAWN_H


#include <string>

#include "loaders/beastloader.h"

#include "commandos.h"
#include "scout.h"
#include "ghoul.h"


using namespace std;


void spawnMonster(string name, int x, int y, Loaders* loader, GameLogic* gmLogic);
void spawnPlayer(string name, int x, int y, Loaders* loader, GameLogic* gmLogic);
int  spawnMonsterAndGetId(string name, int x, int y, Loaders* loader, GameLogic* gmLogic);

#endif // SPAWN_H
