

#include "player.h"


void Player::step(GameLogic* gmLogic, Loaders* loader) {
    // PLAYER PHASE LOOP
    _updateRunMode();

    while (true) {
        _updatePlayerInfo();
        _waitingForPlayerAction(loader);

        ModelEvent event = EventStack::popModelEvent();

        EventStack::clearModelStack();

        // MOVEMENT
        if (event.type == MOVE_PLAYER) {
            concentration -= 50;
            stamina       -= 50;
            if (_movePlayer(gmLogic, (Direction)event.argA)) {
                break;
            } else {
                continue;
            }
        }

        // WAITING
        if (event.type == WAIT) {
            ;
        }

        // END OF GAME
        if (event.type == END_OF_WORK) {
            status = DEAD;
            break;
        }

        // INVENTORY
        if (event.type == INVENTORY) {
            _inventory(gmLogic, loader);
            break;
        }

        // CHAR_LIST
        if (event.type == CHAR_LIST) {
            _charList(gmLogic, loader);
            continue;
        }

        // GET ITEM
        if (event.type == GET_ITEM) {
            _getItem(gmLogic, loader);
            break;
        }

        // HACK
        if (event.type == HACK_TERMINAL) {
            _hackTerminal(gmLogic, loader);
            break;
        }

        // SHOOT
        if (event.type == GUNSHOT) {
            if (_shooting(gmLogic, loader)) {
                break;
            } else {
                continue;
            }
        }

        // RELOAD
        if (event.type == RELOAD) {
            gmLogic->reloadWeapon(id);
            break;
        }

        // SWAP_WEAPON
        if (event.type == SWAP_WEAPON) {
            swapWeapon();
            EventStack::pushEvent(ViewEvent(PLAY_SOUND, 0, 0, 0, 0, "swapweapon"));
            continue;
        }

        // RUN_MODE
        if (event.type == RUN_MODE) {
            if (_runMode()) {
                break;
            } else {
                continue;
            }
        }

        // HARD_PUNCH
        if (event.type == HARD_PUNCH) {
            gmLogic->closeCombat.hardPunch(id);
            continue;
        }

        // ASSAULT FORWARD
        if (event.type == ASSAULT_FORWARD) {
            gmLogic->assaultForward(this);
            break;
        }
    }

    _updatePlayerInfo();
}


void Player::onDeath() {
    ;
}


void Player::_inventory(GameLogic* gmLogic, Loaders* loader) {
    // SYS INIT
    int curPointer = 0;
    _recheckInventoryPointer(curPointer);

    // MAIN INVENTORY CYCLE
    while (true) {
        // EXIT IF NO ITEMS
        if (inventory.isEmpty()) {
            break;
        }

        // DRAW
        _updateInventoryInfoAndGetItemSum(curPointer);
        _updatePlayerInfo();

        // WAIT FOR ACTIONS
        _waitingForDrawer(loader);
        _waitingForPlayerAction(loader);

        ModelEvent event = EventStack::popModelEvent();

        EventStack::clearModelStack();

        // CHANGE POINTER POSITION
        if (event.type == MOVE_PLAYER) {
            if ((Direction)event.argA == TOP) {
                _moveInventoryPointer(curPointer, TOP);
            } else if ((Direction)event.argA == BOTTOM) {
                _moveInventoryPointer(curPointer, BOTTOM);
            }
            continue;
        }

        // EXIT FROM INVENTORY
        if (event.type == INVENTORY || event.type == CLOSE) {
            break;
        }

        // DROP ITEM
        if (event.type == DROP_ITEM) {
            gmLogic->dropItemFromInventoryByPos(this, curPointer);
            _recheckInventoryPointer(curPointer);
            continue;
        }

        // EQUIP ITEM
        if (event.type == EQUIP_ITEM  || event.type == ENTER) {
            gmLogic->equipItemFromInventoryByPos(this, curPointer);
            _recheckInventoryPointer(curPointer);
            continue;
        }

        // USE ITEM
        if (event.type == USE_ITEM) {
            gmLogic->useItemFromInventoryByPos(this, curPointer);
            _recheckInventoryPointer(curPointer);
            continue;
        }
    }

    // STOP DRAWING INVENTORY
    EventStack::pushEvent(ViewEvent(DRAW_ITEM_LIST, false, 0, 0, 0, ""));

    // UPDATE LOS
    gmLogic->updateLos(this);
}


void Player::_updateInventoryInfoAndGetItemSum(const int curPos) {
    // SYS INIT
    int listElemSum = 0;
    int currentPos = 0;

    // DRAW LIST
    EventStack::pushEvent(ViewEvent(DRAW_ITEM_LIST, 1, 0, 0, 0, "-=-=-Inventory-=-=-"));

    for (int itemId = 0; itemId < INVENTORY_SIZE; itemId++) {
        CommonItem* item = inventory.getItemByPos(itemId);

        if (item == 0) {
            continue;
        }

        if (itemId == curPos) {
            currentPos = listElemSum;
        }

        EventStack::pushEvent(ViewEvent(ADD_ITEM_ELEM, item->libraryId, 0, 0, 0, item->getSpecialName()));

        listElemSum++;
    }

    // SET CURRENT POS IN LIST
    EventStack::pushEvent(ViewEvent(SELECT_NEW_ELEM, currentPos, 0, 0, 0, ""));
}


void Player::_updatePlayerInfo() {
    string statusStr;

    switch (status) {
        case DEAD: {
            statusStr = "Dead";
        } break;
        case NORMAL: {
            statusStr = "Normal";
        } break;
        case RUN: {
            statusStr = "Running";
        } break;
        case TIRED: {
            statusStr = "Tired";
        } break;
    }

    EventStack::pushEvent(ViewEvent(PLAYER_STATUS, 0, 0, 0, 0, statusStr));

    EventStack::pushEvent(ViewEvent(PLAYER_TIREDNESS, concentrationMax, concentration, 0, 0, ""));

    EventStack::pushEvent(ViewEvent(BEAST_INJURED, id, staminaMax, stamina, 0, ""));


    if (leftHand != 0) {
        EventStack::pushEvent(ViewEvent(EQUIP_WEAPON, leftHand->libraryId, 0, 0 , 0, leftHand->getSpecialName()));
    } else {
        EventStack::pushEvent(ViewEvent(EQUIP_WEAPON, -1, 0, 0 , 0, "No Weapon"));
    }

    if (armor != 0) {
        Armor* arm = static_cast<Armor*> (armor);

        EventStack::pushEvent(ViewEvent(EQUIP_ARMOR,20, 12, 0 , 0, armor->getSpecialName()));

    } else {
        EventStack::pushEvent(ViewEvent(EQUIP_ARMOR, 0, 0, 0 , 0, "No Armor"));
    }
}


void Player::_getItem(GameLogic* gmLogic, Loaders* loader) {
    // SYS INIT
    int itemSum   = 0;
    int curPos    = 0;
    int curItemId = 0;

    // MAIN GET ITEM CYCLE
    while (true) {
        // DRAW
        gmLogic->updateGetItemInfoAndGetItemSum(this, curPos, itemSum, curItemId);
        _updatePlayerInfo();

        // EXIT IF NO ITEMS
        if (itemSum == 0) {
            break;
        }

        // WAIT FOR ACTIONS
        _waitingForPlayerAction(loader);

        ModelEvent event = EventStack::popModelEvent();

        EventStack::clearModelStack();

        // CHANGE POINTER POSITION
        if (event.type == MOVE_PLAYER) {
            if ((Direction)event.argA == TOP) {
                if (curPos > 0) {
                    curPos--;
                }
            } else if ((Direction)event.argA == BOTTOM) {
                if (curPos < itemSum - 1) {
                    curPos++;
                }
            }
            continue;
        }

        // EXIT FROM GET ITEM MENU
        if (event.type == CLOSE) {
            break;
        }

        // GET ITEM
        if (event.type == GET_ITEM || event.type == ENTER) {
            gmLogic->putToInventoryActiveItemById(this, curItemId);
            break;
        }
    }

    // STOP DRAWING GET ITEM LIST
    EventStack::pushEvent(ViewEvent(DRAW_ITEM_LIST, false, 0, 0, 0, ""));
}


void Player::_recheckInventoryPointer(int& curPointer) {
    int temporaryPointer = -1;

    for (int i = 0; i <= curPointer; i++) {
        if (inventory.getItemByPos(i) == 0) {
            continue;
        }

        temporaryPointer = i;
    }

    if (temporaryPointer == -1) {
        for (int i = INVENTORY_SIZE-1; i > curPointer; i--) {
            if (inventory.getItemByPos(i) == 0) {
                continue;
            }

        temporaryPointer = i;
        }
    }

    curPointer = temporaryPointer;
}


void Player::_moveInventoryPointer(int& curPointer, const Direction direction) {
    int curPos = curPointer;

    if (inventory.isEmpty()) {
        return;
    }

    if (direction == TOP) {
        do {
            curPos--;
            if (curPos < 0) {
                curPos++;
                while (inventory.getItemByPos(curPos) == 0) { curPos++; }
                break;
            }
        } while (inventory.getItemByPos(curPos) == 0);
    }

    if (direction == BOTTOM) {
        do {
            curPos++;

            if (curPos >= INVENTORY_SIZE) {
                curPos--;
                while (inventory.getItemByPos(curPos) == 0) { curPos--; }
                break;
            }
        } while (inventory.getItemByPos(curPos) == 0);
    }

    curPointer = curPos;
}


bool Player::_runMode() {

        return false;

}


bool Player::_movePlayer(GameLogic* gmLogic, Direction direction) {
    gmLogic->changeBeastDirection(this, direction);
    return gmLogic->movePlayer(this);
}

void Player::_updateRunMode() {

}


bool Player::_shooting(GameLogic* gmLogic, Loaders* loader) {
    int globX = x;
    int globY = y;


    // SELECT NEAREST ENEMY IF EXIST
    int nearestPos = 9000;

    for(int i = 0; i < MAX_BEASTS; i++) {
        Creature* cr = loader->beastLoader->getBeastById(i);

        if (cr == 0 || i == PLAYER_ID) {
            continue;
        }

        if (cr->status != DEAD) {
            ExtendedCell* cell = loader->areaLoader->getExtendedCell(cr->x, cr->y);
            if (cell->isVisible) {
                if (cell->combatZoneLevel <= nearestPos) {
                    nearestPos = cell->combatZoneLevel;
                    globX = cr->x;
                    globY = cr->y;
                }
            }
        }
    }

    // DRAW TARGET LINE
    LineEquation lnEq(x, y, globX, globY);
    for (int i = 0; i <= lnEq.getStepSum(); i++) {
        int trX, trY;
        lnEq.setCoordByStep(trX, trY, i);
        ExtendedCell* trCell = loader->areaLoader->getExtendedCell(trX, trY);
        trCell->drawBorder = true;
    }

    // MAIN TARGET MENU CYCLE
    while (true) {
        // CHANGE PLAYER DIRECTION

        gmLogic->changeBeastDirection(this, getDirectionBetweenCoord(x, y, globX, globY));

        // WAIT FOR ACTIONS
        _waitingForDrawer(loader);
        _waitingForPlayerAction(loader);

        ModelEvent event = EventStack::popModelEvent();

        EventStack::clearModelStack();

        // EXIT FROM TARGET MENU
        if (event.type == CLOSE) {
            lnEq.init(x, y, globX, globY);
            for (int i = 0; i <= lnEq.getStepSum(); i++) {
                int trX, trY;
                lnEq.setCoordByStep(trX, trY, i);
                ExtendedCell* trCell = loader->areaLoader->getExtendedCell(trX, trY);
                trCell->drawBorder = false;
            }
            _waitingForDrawer(loader);
            return false;
        }

        // SHOOT
        if (event.type == GUNSHOT || event.type == ENTER) {
            break;
        }

        // CHANGE POSITION
        if (event.type == MOVE_PLAYER) {
            int tmpGlobX = globX;
            int tmpGlobY = globY;
            updateCoordByDir(tmpGlobX, tmpGlobY, (Direction)event.argA);
            ExtendedCell* cell = loader->areaLoader->getExtendedCell(tmpGlobX, tmpGlobY);
            if (cell->isVisible == true) {
                lnEq.init(x, y, globX, globY);
                for (int i = 0; i <= lnEq.getStepSum(); i++) {
                    int trX, trY;
                    lnEq.setCoordByStep(trX, trY, i);
                    ExtendedCell* trCell = loader->areaLoader->getExtendedCell(trX, trY);
                    trCell->drawBorder = false;
                }

                globX = tmpGlobX;
                globY = tmpGlobY;

                //cout << "stPosX: " << x << "stPosY: " << y << " targetX: " << globX << " targetY: " << globY << endl;
                lnEq.init(x, y, globX, globY);
                for (int i = 0; i <= lnEq.getStepSum(); i++) {
                    int trX, trY;
                    lnEq.setCoordByStep(trX, trY, i);
                    //cout << "Step " << i << ": x: " << trX << " y: " << trY << endl;
                    ExtendedCell* trCell = loader->areaLoader->getExtendedCell(trX, trY);
                    trCell->drawBorder = true;
                }
            }
        }
    }

    lnEq.init(x, y, globX, globY);
    for (int i = 0; i <= lnEq.getStepSum(); i++) {
        int trX, trY;
        lnEq.setCoordByStep(trX, trY, i);
        ExtendedCell* trCell = loader->areaLoader->getExtendedCell(trX, trY);
        trCell->drawBorder = false;
    }

    _waitingForDrawer(loader);

    if (x == globX && y == globY) {
        updateCoordByDir(globX, globY, viewDirection);
    }

    gmLogic->shooting.shoot(this, globX, globY, 8, 7);
    return true;
}


bool Player::_hackTerminal(GameLogic* gmLogic, Loaders* loader) {
    // SYS INIT
    /*
    int globX = x;
    int globY = y;

    updateCoordByDir(globX, globY, viewDirection);

    AreaCell* cell = loader->areaLoader->getCell(globX, globY);

    if (!loader->areaLoader->isCellTerminal(cell->type)) {
        return false;
    }

    // HACKING
    EventStack::pushEvent(ViewEvent(START_HACK, 0, 0, 0 , 0, ""));
    _waitingForDrawer(loader);


    // HACKING RESULT
    if (Dice::D100() >= 60) {
        // SUCCESS
        cell->type = loader->areaLoader->getClosedTerminalType();
        EventStack::pushEvent(ViewEvent(CHAT_MSG, (int)CYAN, 0, 0, 0, "You're hacked the terminal"));

        computers.increase();
    } else {
        // FAILTURE
        cell->type = loader->areaLoader->getFailedTerminalType();
        EventStack::pushEvent(ViewEvent(CHAT_MSG, (int)RED, 0, 0, 0, "Hacking failed"));
    }

    // UPDATE CELL INFO
    EventStack::pushEvent(ViewEvent(UPDATE_CELL_SPRITE, globX, globY, 0, 0, ""));
    */
}


void Player::_charList(GameLogic* gmLogic, Loaders* loader) {
    // SYS INIT

    EventStack::pushEvent(ViewEvent(DRAW_CHARLIST, id, true, 0, 0, ""));

    // MAIN CHARLIST CYCLE
    while (true) {
        // WAIT FOR ACTIONS
        _waitingForDrawer(loader);
        _waitingForPlayerAction(loader);

        ModelEvent event = EventStack::popModelEvent();

        EventStack::clearModelStack();

        // EXIT FROM TARGET MENU
        if (event.type == CLOSE || event.type == ENTER) {
            EventStack::pushEvent(ViewEvent(DRAW_CHARLIST, id, false, 0, 0, ""));
            break;
        }
    }

    _waitingForPlayerAction(loader);
}
