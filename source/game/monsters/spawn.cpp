

#include "spawn.h"


void spawnMonster(string name, int x, int y, Loaders* loader, GameLogic* gmLogic) {
    Creature* creature = 0;

    if (name == "Commandos") {
        creature = new Commandos(gmLogic);
    }

    if (name == "Scout") {
        creature = new Scout(gmLogic);
    }

    if (name == "Ghoul") {
        creature = new Ghoul(gmLogic);
    }

    creature->x = x;
    creature->y = y;

    loader->beastLoader->makeBeastByPointer(creature);
}


void spawnPlayer(string name, int x, int y, Loaders* loader, GameLogic* gmLogic) {
    Creature* creature = 0;

    if (name == "Commandos") {
        creature = new Commandos(gmLogic);
    }

    if (name == "Scout") {
        creature = new Scout(gmLogic);
    }

    if (name == "Ghoul") {
        creature = new Ghoul(gmLogic);
    }

    if (creature == 0) {
        throw 69;
    }

    creature->x = x;
    creature->y = y;

    loader->beastLoader->makeBeastByPointer(creature);
}


int spawnMonsterAndGetId(string name, int x, int y, Loaders* loader, GameLogic* gmLogic) {
    Creature* creature = 0;

    cout << "name: " << name << endl;

    if (name == "Commandos") {
        creature = new Commandos(gmLogic);
    }

    if (name == "Scout") {
        creature = new Scout(gmLogic);
    }

    if (name == "Ghoul") {
        creature = new Ghoul(gmLogic);
    }

    if (creature == 0) {
        throw 69;
    }

    creature->x = x;
    creature->y = y;

    return loader->beastLoader->makeBeastByPointer(creature);
}
