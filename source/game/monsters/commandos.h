#ifndef COMMANDOS_H
#define COMMANDOS_H


#include "controlledcreature.h"
#include "mechanics/gamelogic.h"

#include "player.h"


class Commandos : public Player {
    public:
        Commandos(GameLogic* gmLogic);
};


#endif // COMMANDOS_H
