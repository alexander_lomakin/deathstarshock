#ifndef SCOUT_H
#define SCOUT_H


#include "controlledcreature.h"
#include "mechanics/gamelogic.h"

#include "player.h"

class Scout : public Player {
    public:
        Scout(GameLogic* gmLogic);
};


#endif // SCOUT_H
