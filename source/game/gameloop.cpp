

#include "gameloop.h"


using namespace std;


GameEnd::GameEnd(Loaders* loader) {
    EventStack::pushEvent(ViewEvent(CHAT_MSG, (int)CYAN, 0, 0, 0, "End of game. Press ENTER to exit..."));
     while (true) {
        _waitingForDrawer(loader);
        _waitingForPlayerAction(loader);

        ModelEvent event = EventStack::popModelEvent();

        EventStack::clearModelStack();

        if (event.type == ENTER) {
            break;
        }
    }
}


// beasts per turn actions loop
void beastsActionLoop(Loaders* loader, GameLogic* gmLogic, DemonMind* demonMind, bool *isGameEnd) {
    // ACTIONS LOOP
    for(int i = 0; i < MAX_BEASTS; i++) {
        Creature* cr = loader->beastLoader->getBeastById(i);

        if (isEndOfGame(loader)) {
            return;
        }

        if ((*isGameEnd) == true) {
            return;
        }

        if (cr == 0) {
            continue;
        }

        ControlledCreature* creature = dynamic_cast<ControlledCreature*> (cr);

        if (creature->status != DEAD) {
            creature->step(gmLogic, loader);
            if (creature->id == PLAYER_ID) {
                demonMind->combatZoneStep(cr->x, cr->y);
            }
        }
    }
}

// end of game if player die
bool isEndOfGame(Loaders* loader) {
	Creature* cr = loader->beastLoader->getBeastById(PLAYER_ID);

	if (cr == 0)
        return true;

    if (cr->status == DEAD)
        return true;

    return false;
}


// main game logic thread func
void gameLoop(Loaders* loader, GameLogic* gmLogic, DemonMind* demonMind, bool* endOfWork) {
    // SYS INIT
	Creature* cr = loader->beastLoader->getBeastById(PLAYER_ID);

    // IN-GAME LOOP
    while ((*endOfWork) == false) {
        // IS GAME END PHASE
        if (isEndOfGame(loader)) {
            GameEnd gmEnd(loader);
            break;
        }

        loader->semaphore.enter();

        // DEMONMIND PHASE
        // combat zone subphase
        demonMind->combatZoneStep(cr->x, cr->y);
        // demonmind phase
        demonMind->step();

        // BEASTS PHASE
        beastsActionLoop(loader, gmLogic, demonMind, endOfWork);

        loader->semaphore.leave();
    }

    // END  OF GAME
    *endOfWork = true;
}


void onGameEnd() {

}
