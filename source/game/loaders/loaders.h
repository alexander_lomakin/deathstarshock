#ifndef LOADERS_H
#define LOADERS_H


#include "arealoader.h"
#include "beastloader.h"
#include "itemloader.h"
#include "optionloader.h"

#include "semaphore.h"


struct Loaders {
        Loaders() {
           itemLoader  = 0;
           areaLoader  = 0;
           beastLoader = 0;
           semaphore.init(1);
        }

        ~Loaders() {
            destroy();
        }

        DropGenerator dropGen;
        OptionLoader  optionLoader;
        ItemLoader    *itemLoader;
        BeastLoader   *beastLoader;
        AreaLoader    *areaLoader;
        Semaphore     semaphore;

        void init() {
            areaLoader  = new AreaLoader;
            itemLoader  = new ItemLoader;
            beastLoader = new BeastLoader;

            semaphore.init(1);

            dropGen.init(itemLoader);

            dropGen.loadCells(optionLoader.getStrKey("Loaders", "DropGenCellsFilePath"));

            areaLoader ->init(itemLoader, &dropGen);
            beastLoader->init(itemLoader, &dropGen);
            itemLoader ->init();
        }

        void destroy() {
            if (areaLoader != 0) {
                delete areaLoader;
            }

            if (beastLoader != 0) {
                delete beastLoader;
            }

            if (itemLoader != 0) {
                delete itemLoader;
            }
        }
    };



#endif // LOADERS_H
