

#include "itemloader.h"


using namespace std;
using namespace irr;
using namespace io;


ItemLoader::ItemLoader()
{
    _libraryItemId    = -1;
    _activeItemLastId = -1;
    _saveLastIdToFile = true;

    for (int i = 0; i < 9; i++)
        _activeItemSumInArea[i] = 0;
}


ItemLoader::~ItemLoader()
{
    if (_saveLastIdToFile)
        _saveActiveLastIdToFile();

    for (int i = 0; i < _itemLibrary.size(); i++)
        delete _itemLibrary[i];
}


void ItemLoader::init()
{
    if (_saveLastIdToFile)
        _loadActiveLastIdFromFile();
}


void ItemLoader::saveLastIdToFile(bool save)
{
    _saveLastIdToFile = save;
}


void ItemLoader::loadItemLibraryFromFile()
{
    IrrXMLReader* xml = createIrrXMLReader(OptionLoader::getStrKey("ItemLoader", "itemLibraryFilePath").c_str());

    while(xml && xml->read())
    {
        if (xml->getNodeType() == EXN_ELEMENT)
        {
            if (!strcmp("weapon", xml->getNodeName()))
            {
                _loadWeaponFromFile(xml);
                continue;
            }

            if (!strcmp("armor", xml->getNodeName()))
            {
                _loadArmorFromFile(xml);
                continue;
            }

            if (!strcmp("ammo", xml->getNodeName()))
            {
                _loadAmmoFromFile(xml);
                continue;
            }

            if (!strcmp("component", xml->getNodeName()))
            {
                _loadComponentFromFile(xml);
                continue;
            }

            if (!strcmp("medicament", xml->getNodeName()))
            {
                _loadMedicamentFromFile(xml);
                continue;
            }

            if (!strcmp("food", xml->getNodeName()))
            {
                _loadFoodFromFile(xml);
                continue;
            }
        }


   }

   delete xml;
}


void ItemLoader::_loadWeaponFromFile(IrrXMLReader* xml) {
    Weapon* weapon = new Weapon;

    _libraryItemId++;

    weapon->name = xml->getAttributeValue("name");
    weapon->id   = _libraryItemId;

    weapon->libraryId = _libraryItemId;

    weapon->magazineCapacity = STI(xml->getAttributeValue("magazineCapacity"));
    weapon->capacityNow      = weapon->magazineCapacity;

    weapon->isTwoHanded = (bool)STI(xml->getAttributeValue("isTwoHanded"));

    weapon->needAmmo = (bool)STI(xml->getAttributeValue("useAmmo"));
    if (weapon->needAmmo == true) {
        weapon->ammoType = xml->getAttributeValue("ammoType");
    }

    weapon->rate         = STI(xml->getAttributeValue("rate"));
    weapon->accuracy     = STI(xml->getAttributeValue("accuracy"));
    weapon->mass         = STI(xml->getAttributeValue("mass"));
    weapon->camoBonus    = STI(xml->getAttributeValue("camoBonus"));

    _itemLibrary.push_back(weapon);
}


void ItemLoader::_loadArmorFromFile(IrrXMLReader* xml) {
    Armor* armor = new Armor;

    _libraryItemId++;

    armor->name = xml->getAttributeValue("name");
    armor->id   = _libraryItemId;

    armor->libraryId = _libraryItemId;


    armor->armorClass     = STI(xml->getAttributeValue("class"));
    armor->armorMass      = STI(xml->getAttributeValue("mass"));
    armor->armorState     = STI(xml->getAttributeValue("state"));
    armor->armorCamoBonus = STI(xml->getAttributeValue("camoBonus"));

    string armorType = xml->getAttributeValue("type");

    if (armorType == "HELMET") {
        armor->type = HELMET;
    }

    if (armorType == "BODY_ARMOR") {
        armor->type = BODY_ARMOR;
    }

    if (armorType == "LEGGINGS") {
        armor->type = LEGGINGS;
    }

    if (armorType == "GLOVES") {
        armor->type = GLOVES;
    }

    if (armorType == "SPACE_SUIT") {
        armor->type = SPACE_SUIT;
    }

    _itemLibrary.push_back(armor);
}


void ItemLoader::_loadAmmoFromFile(IrrXMLReader* xml) {
    Ammo* ammo = new Ammo;

    _libraryItemId++;

    ammo->name = xml->getAttributeValue("name");
    ammo->id   = _libraryItemId;

    ammo->libraryId = _libraryItemId;


    ammo->ammoType = xml->getAttributeValue("ammoType");


    string ammoClass = xml->getAttributeValue("ammoClass");

    if (ammoClass == "STANDARD") {
        ammo->ammoClass = STANDARD_AMMO;
    }

    if (ammoClass == "EXTENSIVE") {
        ammo->ammoClass = EXTENSIVE_AMMO;
    }

    if (ammoClass == "PIERCING") {
        ammo->ammoClass = PIERCING_AMMO;
    }

    ammo->bulletMass  = STI(xml->getAttributeValue("bulletMass"));
    ammo->bulletSpeed = STI(xml->getAttributeValue("bulletSpeed"));


    ammo->containerMass = STI(xml->getAttributeValue("containerMass"));

    ammo->cageMaxSize = STI(xml->getAttributeValue("cageMaxSize"));
    ammo->cageNow     = ammo->cageMaxSize;

    _itemLibrary.push_back(ammo);
}


void ItemLoader::_loadComponentFromFile(IrrXMLReader* xml)
{
    Component* component = new Component;

    _libraryItemId++;

    component->name = xml->getAttributeValue("name");
    component->id   = _libraryItemId;

    component->libraryId = _libraryItemId;

    component->cageMax = STI(xml->getAttributeValue("cageMax"));
    component->cageNow = component->cageMax;

    _itemLibrary.push_back(component);
}


void ItemLoader::_loadMedicamentFromFile(IrrXMLReader* xml)
{
    Medicament* medicament = new Medicament;

    _libraryItemId++;

    medicament->name = xml->getAttributeValue("name");
    medicament->id   = _libraryItemId;

    medicament->libraryId = _libraryItemId;

    medicament->restoredHpSum    = STI(xml->getAttributeValue("restoredHpSum"));
    medicament->restoredTiredSum = STI(xml->getAttributeValue("restoredTiredSum"));

    _itemLibrary.push_back(medicament);
}


void ItemLoader::_loadFoodFromFile(IrrXMLReader* xml) {
    Food* food = new Food;

    _libraryItemId++;

    food->name = xml->getAttributeValue("name");
    food->id   = _libraryItemId;

    food->libraryId = _libraryItemId;

    food->restoredStamina       = STI(xml->getAttributeValue("restoredStamina"));
    food->restoredConcentration = STI(xml->getAttributeValue("restoredConcentration"));
    food->mass                  = STI(xml->getAttributeValue("mass"));

    _itemLibrary.push_back(food);
}



void ItemLoader::makeActiveItemFromLibrary(int libraryId, short localX, short localY, Direction area)
{
    CommonItem* item = 0;

    for (int i = 0; i < _itemLibrary.size(); i++)
    {
        item = _itemLibrary[i];
        if (item->id == libraryId)
            break;
        else
        item = 0;
    }

    if (item == 0)
        return;

    if (typeid(*item) == typeid(Weapon))
        _makeActiveItem(item, Weapon(), localX, localY, area);

    if (typeid(*item) == typeid(Armor))
        _makeActiveItem(item, Armor(), localX, localY, area);

    if (typeid(*item) == typeid(Ammo))
        _makeActiveItem(item, Ammo(), localX, localY, area);

    if (typeid(*item) == typeid(Component))
        _makeActiveItem(item, Component(), localX, localY, area);

    if (typeid(*item) == typeid(Medicament))
        _makeActiveItem(item, Medicament(), localX, localY, area);

    if (typeid(*item) == typeid(Food))
        _makeActiveItem(item, Food(), localX, localY, area);
}


void ItemLoader::makeActiveItemFromLibrary(string libItemName, short localX, short localY, Direction area)
{
    CommonItem* item = 0;

    for (int i = 0; i < _itemLibrary.size(); i++)
    {
        item = _itemLibrary[i];
        if (item->name == libItemName)
            break;
        else
            item = 0;
    }

    if (item == 0)
        return;

    if (typeid(*item) == typeid(Weapon))
        _makeActiveItem(item, Weapon(), localX, localY, area);

    if (typeid(*item) == typeid(Armor))
        _makeActiveItem(item, Armor(), localX, localY, area);

    if (typeid(*item) == typeid(Ammo))
        _makeActiveItem(item, Ammo(), localX, localY, area);

    if (typeid(*item) == typeid(Component))
        _makeActiveItem(item, Component(), localX, localY, area);

    if (typeid(*item) == typeid(Medicament))
        _makeActiveItem(item, Medicament(), localX, localY, area);

    if (typeid(*item) == typeid(Food))
        _makeActiveItem(item, Food(), localX, localY, area);
}



template <class X>
void ItemLoader::_makeActiveItem(CommonItem* item, X i, short localX, short localY, Direction area)
{
    X* nItem = new X;
    X* dp = static_cast<X*> (item);

    *nItem = *dp;

    _activeItemLastId++;

    nItem->id = _activeItemLastId;

    nItem->localX = localX;
    nItem->localY = localY;
    nItem->area   = area;

    EventStack::pushEvent(ViewEvent(ITEM_CREATED, nItem->libraryId, nItem->localX, nItem->localY, (int)area, ""));

    _activeItemSumInArea[area]++;

    _activeItems.push_back(nItem);
}


void ItemLoader::saveActiveItemsToArea(string areaFilePath, Direction area)
{
    string itemFilePath = areaFilePath + ".it";
	ofstream itemFile(itemFilePath.c_str());

    itemFile << (_activeItemSumInArea[area]) << endl;

	for (int i = 0; i < _activeItems.size(); i++)
    {
        if (_activeItems[i] == 0)
        {
            _removeEmptyActiveItems();
            continue;
        }

        if (_activeItems[i]->area != area)
            continue;

        itemFile << _activeItems[i]->libraryId << endl;
        itemFile << _activeItems[i]->id        << endl;
        itemFile << _activeItems[i]->localX    << endl;
        itemFile << _activeItems[i]->localY    << endl;

        _activeItems[i]->saveData(itemFile);

        int localX = _activeItems[i]->localX;
        int localY = _activeItems[i]->localY;
        Direction direction = _activeItems[i]->area;

        delete _activeItems[i];
        _activeItems[i] = 0;

        _deleteItemIcon(localX, localY, direction);
    }

	itemFile.close();

    _activeItemSumInArea[area] = 0;

    _removeEmptyActiveItems();
}


void ItemLoader::loadActiveItemsFromArea(string areaFilePath, Direction area)
{
    string itemFilePath = areaFilePath + ".it";
	ifstream itemFile(itemFilePath.c_str());

    if (!itemFile)
    {
        itemFile.close();
        return;
    }

    itemFile >> _activeItemSumInArea[area];

	for (int i = 0; i < _activeItemSumInArea[area]; i++)
    {
        short localX, localY;
        int id, libraryId;

        itemFile >> libraryId;
        itemFile >> id;
        itemFile >> localX;
        itemFile >> localY;

        makeActiveItemFromLibrary(libraryId, localX, localY, area);

        _activeItemSumInArea[area]--;
        _activeItemLastId--;

        CommonItem* item = _activeItems[_activeItems.size()-1];

        item->id = id;

        item->loadData(itemFile);
    }

	itemFile.close();
}


void ItemLoader::moveActiveItemsArea(Direction source, Direction destination)
{
    _activeItemSumInArea[source]      = 0;
    _activeItemSumInArea[destination] = 0;

    for (int i = 0; i < _activeItems.size(); i++)
    {
         if (_activeItems[i]->area == source)
         {
             _activeItems[i]->area = destination;

            EventStack::pushEvent(ViewEvent(ITEM_CREATED, _activeItems[i]->libraryId,
                                        _activeItems[i]->localX, _activeItems[i]->localY, (int)destination, ""));

            EventStack::pushEvent(ViewEvent(ITEM_DELETED, _activeItems[i]->localX, _activeItems[i]->localY, (int)source, 0, ""));
            _deleteItemIcon(_activeItems[i]->localX, _activeItems[i]->localY, source);

             _activeItemSumInArea[destination]++;
             continue;
         }

         if (_activeItems[i]->area == destination)
         {
             _activeItems[i]->area = source;

             EventStack::pushEvent(ViewEvent(ITEM_CREATED, _activeItems[i]->libraryId,
                                        _activeItems[i]->localX, _activeItems[i]->localY, (int)source, ""));

            _deleteItemIcon(_activeItems[i]->localX, _activeItems[i]->localY, destination);

             _activeItemSumInArea[source]++;
             continue;
         }
    }
}


void ItemLoader::_removeEmptyActiveItems()
{
    vector<CommonItem*>::iterator p = _activeItems.begin();
	for (short i = 0; i < _activeItems.size(); i++)
    {
		if (_activeItems[i] == 0)
		{

			_activeItems.erase(p, p+1);
		}
        p += 1;
    }
}


CommonItem* ItemLoader::getActiveItemIdOnPos(int num, short localX, short localY, Direction area) {
    int nowItem = 0;

    for (short i = 0; i < _activeItems.size(); i++) {
        if (_activeItems[i] == 0) {
            continue;
        }

        if (_activeItems[i]->area != area) {
            continue;
        }

        if (_activeItems[i]->localX == localX && _activeItems[i]->localY == localY) {
            if (nowItem == num) {
                return _activeItems[i];
            }

            nowItem++;
        }
    }
}


void ItemLoader::getActiveItemsIdSumOnPos(int& lstSize, short localX, short localY, Direction area) {
    lstSize = 0;

    for (short i = 0; i < _activeItems.size(); i++) {
        if (_activeItems[i] == 0) {
            continue;
        }

        if (_activeItems[i]->area != area) {
            continue;
        }

        if (_activeItems[i]->localX == localX && _activeItems[i]->localY == localY) {
            lstSize++;
        }
    }
}


CommonItem* ItemLoader::getActiveItemById(int id)
{
    for (short i = 0; i < _activeItems.size(); i++)
    {
        if (_activeItems[i]->id == id)
            return _activeItems[i];
    }

    return 0;
}


CommonItem* ItemLoader::cutActiveItemByid(int id) {
    int localX, localY;

    CommonItem* item = 0;

    for (short i = 0; i < _activeItems.size(); i++)
        if (_activeItems[i]->id == id)
         {
             item = _activeItems[i];
             _activeItemSumInArea[_activeItems[i]->area]--;
             _activeItems[i] = 0;
             _deleteItemIcon(item->localX, item->localY, item->area);
             break;
         }



    _removeEmptyActiveItems();
    return item;
}


void ItemLoader::pushActiveItem(CommonItem* item, short localX, short localY, Direction area)
{
    item->localX = localX;
    item->localY = localY;
    item->area   = area;

    _activeItemSumInArea[area]++;

     EventStack::pushEvent(ViewEvent(ITEM_CREATED, item->libraryId, localX, localY, (int)area, ""));

    _activeItems.push_back(item);
}


CommonItem* ItemLoader::createAndCutActiveItemByName(string name)
{
    int localX, localY;
    Direction area;

    localX = 0;
    localY = 0;

    area = CENTER;

    CommonItem* item = 0;

    for (int i = 0; i < _itemLibrary.size(); i++)
    {
        item = _itemLibrary[i];
        if (item->name == name)
            break;
        else
            item = 0;
    }

    if (item == 0)
        return 0;

    if (typeid(*item) == typeid(Weapon))
        _makeActiveItem(item, Weapon(), localX, localY, area);

    if (typeid(*item) == typeid(Armor))
        _makeActiveItem(item, Armor(), localX, localY, area);

    if (typeid(*item) == typeid(Ammo))
        _makeActiveItem(item, Ammo(), localX, localY, area);

    if (typeid(*item) == typeid(Component))
        _makeActiveItem(item, Component(), localX, localY, area);

    if (typeid(*item) == typeid(Medicament))
        _makeActiveItem(item, Medicament(), localX, localY, area);

    if (typeid(*item) == typeid(Food))
        _makeActiveItem(item, Food(), localX, localY, area);

    return cutActiveItemByid(_activeItemLastId);
}


void ItemLoader::resetAndSaveLastActiveId()
{
    _activeItemLastId = -1;
    if (_saveLastIdToFile)
        _saveActiveLastIdToFile();
}


void ItemLoader::_saveActiveLastIdToFile()
{
    ofstream file(OptionLoader::getStrKey("ItemLoader", "itemDataFilePath").c_str());

    file << _activeItemLastId;

    file.close();
}


void ItemLoader::_loadActiveLastIdFromFile()
{
    ifstream file(OptionLoader::getStrKey("ItemLoader", "itemDataFilePath").c_str());

    if (file)
        file >>  _activeItemLastId;

    file.close();
}


void ItemLoader::_deleteItemIcon(int localX, int localY, Direction direction) {
    int itemSum = 0;
    getActiveItemsIdSumOnPos(itemSum, localX, localY, direction);

    EventStack::pushEvent(ViewEvent(ITEM_DELETED, localX, localY, (int)direction, 0, ""));

    if (itemSum == 0) {
        return;
    }

    for (int i = 0; i < itemSum; i++) {
        CommonItem* item = getActiveItemIdOnPos(i, localX, localY, direction);

        EventStack::pushEvent(ViewEvent(ITEM_CREATED, item->libraryId, localX, localY, (int)direction, ""));

        if (itemSum > 1) {
            EventStack::pushEvent(ViewEvent(ITEM_CREATED, item->libraryId, localX, localY, (int)direction, ""));
        }

        break;
    }
}
