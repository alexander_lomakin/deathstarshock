

#include "arealoader.h"


using namespace std;
using namespace irr;
using namespace io;


AreaLoader::AreaLoader()
{
    _itemLoader = 0;
    _dropGen    = 0;

    _calculateAreaVisits = false;
    _saveAreasPaths      = false;

	_centralAreaX = 0;
	_centralAreaY = 0;
	_currentLevel = 0;

    _currentAreaDirectory  = "";
    _areaSessionDirectory  = OptionLoader::getStrKey("AreaLoader", "areaSessionDirectory");
    _areaOriginalDirectory = OptionLoader::getStrKey("AreaLoader", "areaOriginalDirectory");

    _logFilePath = OptionLoader::getStrKey("AreaLoader", "areaLogFilePath");

    _masterCellsFilePath  = OptionLoader::getStrKey("AreaLoader", "masterCellsFilePath");
    _masterLightsFilePath = OptionLoader::getStrKey("AreaLoader", "lightingFilePath");

	for (int i = 0; i < 9; i++)
        _currentAreas[i] = new Area;
}


AreaLoader::~AreaLoader()
{
    for (int i = 0; i < 9; i++)
    {
        _saveAndUnloadArea((Direction)i);
        delete _currentAreas[i];
    }
}


void AreaLoader::init(ItemLoader* itemLoader, DropGenerator* dropGen)
{
    _itemLoader = itemLoader;
    _dropGen    = dropGen;

    _loadMasterCells();
    _loadMasterLights();

    _cellGenerator.init(&_masterCells, OptionLoader::getStrKey("AreaLoader", "cellGeneratorFilePath"));
}


void AreaLoader::initAndLoadAreas(int nLevel, int nX, int nY) {
	_centralAreaX = nX;
	_centralAreaY = nY;
	_currentLevel = nLevel;

	for (int i = 0; i < 9; i++)
        _loadAndInitArea((Direction)i);
}


void AreaLoader::shiftCurrentAreas(Direction direction) {
    _saveAreaLine(LEFT, LEFT_BOTTOM, LEFT_TOP);
    _saveAreaLine(CENTER, BOTTOM, TOP);
    _saveAreaLine(RIGHT, RIGHT_BOTTOM, RIGHT_TOP);

	switch (direction) {
		case LEFT:
			_centralAreaX--;
			break;
		case RIGHT:
			_centralAreaX++;
			break;
		case TOP:
			_centralAreaY--;
			break;
		case BOTTOM:
			_centralAreaY++;
			break;
        case LEFT_BOTTOM: {
            _centralAreaY++;
            _centralAreaX--;
        } break;
        case RIGHT_BOTTOM: {
            _centralAreaY++;
            _centralAreaX++;
        } break;
        case LEFT_TOP: {
            _centralAreaY--;
            _centralAreaX--;
        } break;
        case RIGHT_TOP: {
            _centralAreaY--;
            _centralAreaX++;
        } break;
	}

	_loadAreaLine(LEFT, LEFT_BOTTOM, LEFT_TOP);
    _loadAreaLine(CENTER, BOTTOM, TOP);
    _loadAreaLine(RIGHT, RIGHT_BOTTOM, RIGHT_TOP);
}


void AreaLoader::saveAreaByDirection(Direction direction) {
    _currentAreas[direction]->saveAreaToFile(_getAreaPathByDirection(direction));
}


AreaCell* AreaLoader::getCell(int globX, int globY) {
	Direction dir = getDirectionByGlobCoord(globX, globY);
    int localX, localY;
    setLocalCoordByDir(globX, globY, dir, localX, localY);
    if (!isLocalCoordCorrect(localX, localY)) {
        return 0;
    }

    return &(_currentAreas[dir]->table[localX][localY]);
}


AreaCell* AreaLoader::getCell(int localX, int localY, Direction direction) {
	return &_currentAreas[direction]->table[localX][localY];
}


GraphicCell* AreaLoader::getGraphicCell(int globX, int globY) {
    Direction dir = getDirectionByGlobCoord(globX, globY);
    int localX, localY;
    setLocalCoordByDir(globX, globY, dir, localX, localY);
    if (!isLocalCoordCorrect(localX, localY)) {
        return 0;
    }
    return &_currentAreas[dir]->graphicTable[localX][localY];
}


GraphicCell* AreaLoader::getGraphicCell(int localX, int localY, Direction direction) {
    return &_currentAreas[direction]->graphicTable[localX][localY];
}


ExtendedCell* AreaLoader::getExtendedCell(int globX, int globY) {
    Direction dir = getDirectionByGlobCoord(globX, globY);
    int localX, localY;
    setLocalCoordByDir(globX, globY, dir, localX, localY);
    if (!isLocalCoordCorrect(localX, localY)) {
        return 0;
    }

    return &(_currentAreas[dir]->extTable[localX][localY]);
}


ExtendedCell* AreaLoader::getExtendedCell(int localX, int localY, Direction direction) {
    return &_currentAreas[direction]->extTable[localX][localY];
}


int AreaLoader::getClosedTerminalType() {
    for (int i = 0; i < _masterCells.size(); i++) {
        if (_masterCells[i].addit == "closedTerminal") {
            return i;
        }
    }

    return 0;
}


int AreaLoader::getFailedTerminalType() {
    for (int i = 0; i < _masterCells.size(); i++) {
        if (_masterCells[i].addit == "failedTerminal") {
            return i;
        }
    }

    return 0;
}


MasterAreaCell AreaLoader::getMasterCell(int type) {
    return _masterCells[type];
}


void AreaLoader::setAreaVisitsTracking(bool calc) {
    _calculateAreaVisits = calc;
}


void AreaLoader::setAreaPathsSaving(bool save) {
    _saveAreasPaths = save;
}


void AreaLoader::copyOriginalAreasToSession() {
    ifstream log(_logFilePath.c_str());

    int x, y, level;
    string path;

    while (!log.eof())
    {
        log >> x;
        log >> y;
        log >> level;
        log >> path;

        Area area;
        area.loadAreaFromFile(path);
        area.saveAreaToFile(_areaSessionDirectory+ITS(x)+"."+ITS(y)+"."+ITS(level));

        ofstream f((_areaSessionDirectory+ITS(x)+"."+ITS(y)+"."+ITS(level) + ".it").c_str());

        f << 0;

        f.close();
    }
}


void AreaLoader::useOriginalAreas() {
    _currentAreaDirectory = _areaOriginalDirectory;
}


void AreaLoader::useSessionAreas() {
    _currentAreaDirectory = _areaSessionDirectory;
}


void AreaLoader::destroyCellAndDrop(int globX, int globY) {
    AreaCell* cell = getCell(globX, globY);

    _dropGen->pushCellItemsOnFloor(globX, globY, _masterCells[cell->type].name);

    cell->type = cell->additCell;

    EventStack::pushEvent(ViewEvent(UPDATE_CELL_SPRITE, globX, globY, 0, 0, ""));
}


string AreaLoader::_getAreaPathByDirection(Direction direction) {
    int areaX, areaY;
    setAreaCoordByDirectionAndCentralCoord(_centralAreaX, _centralAreaY, direction, areaX, areaY);
    return _currentAreaDirectory + ITS(areaX) + "." + ITS(areaY) + "." + ITS(_currentLevel);
}


void AreaLoader::_loadAreaLine(Direction a, Direction b, Direction c) {
    _loadAndInitArea(a);
    _loadAndInitArea(b);
    _loadAndInitArea(c);
}


void AreaLoader::_saveAreaLine(Direction a, Direction b, Direction c) {
    _saveAndUnloadArea(a);
    _saveAndUnloadArea(b);
    _saveAndUnloadArea(c);
}


void AreaLoader::_saveAndUnloadArea(Direction areaDir) {
    string areaPath = _getAreaPathByDirection(areaDir);

    _currentAreas[areaDir]->saveAreaToFile(areaPath);

    _itemLoader->saveActiveItemsToArea(areaPath, areaDir);
}


void AreaLoader::_loadAndInitArea(Direction areaDir) {
    int x, y;
    x = _centralAreaX;
    y = _centralAreaY;

    switch (areaDir)
    {
        case LEFT_TOP:
            x--;
            y--;
            break;
        case TOP:
            y--;
            break;
        case RIGHT_TOP:
            y--;
            x++;
            break;
        case LEFT:
            x--;
            break;
        case RIGHT:
            x++;
            break;
        case LEFT_BOTTOM:
            x--;
            y++;
            break;
        case BOTTOM:
            y++;
            break;
        case RIGHT_BOTTOM:
            x++;
            y++;
    }

    string areaPath = _getAreaPathByDirection(areaDir);

    if (_saveAreasPaths)
        _currentAreas[areaDir]->loadAreaFromFile(x, y, _currentLevel,  areaPath, _logFilePath);
    else
        _currentAreas[areaDir]->loadAreaFromFile(areaPath);

    if (_currentAreas[areaDir]->isFirstlyVisited() && _calculateAreaVisits)
    {
        _currentAreas[areaDir]->makeAreaOldVisited();
        _cellGenerator.generateCells(_currentAreas[areaDir]);
    }

    EventStack::pushEvent(ViewEvent(AREA_LOADED, (int)areaDir, 0, 0, 0, ""));

    _itemLoader->loadActiveItemsFromArea(areaPath, areaDir);
}


void AreaLoader::_loadMasterCells() {
    IrrXMLReader* xml = createIrrXMLReader(_masterCellsFilePath.c_str());

    while(xml && xml->read())
    {
        if (xml->getNodeType() == EXN_ELEMENT)
        {
            if (!strcmp("cell", xml->getNodeName()))
            {
                MasterAreaCell cell;

                cell.name  = xml->getAttributeValue("name");
                cell.desc  = xml->getAttributeValue("desc");
                cell.addit = xml->getAttributeValue("addit");

                cell.isPassable    = (bool)STI(xml->getAttributeValue("isPass"));
                cell.isTransparent = (bool)STI(xml->getAttributeValue("isTrans"));

                cell.lightType    = getLightId(xml->getAttributeValue("lightType"));
                cell.additCell = STI(xml->getAttributeValue("additCell"));

                cell.hitPoints  = STI(xml->getAttributeValue("hitPoints"));
                cell.armorClass = STI(xml->getAttributeValue("armorClass"));

                _masterCells.push_back(cell);
             }
        }
   }

   delete xml;
}


void AreaLoader::_loadMasterLights() {
    IrrXMLReader* xml = createIrrXMLReader(_masterLightsFilePath.c_str());

    while(xml && xml->read())
    {
        if (xml->getNodeType() == EXN_ELEMENT)
        {
            if (!strcmp("light", xml->getNodeName()))
            {
                Light light;

                light.name  = xml->getAttributeValue("name");
                light.power = STI(xml->getAttributeValue("power"));
                light.red   = STI(xml->getAttributeValue("red"  ));
                light.green = STI(xml->getAttributeValue("green"));
                light.blue  = STI(xml->getAttributeValue("blue" ));

                _masterLights.push_back(light);
             }
        }
   }

   delete xml;
}


vector<string> AreaLoader::getMasterCellsNames() {
    vector<string> lst;

    for (int i = 0; i < _masterCells.size(); i++)
        lst.push_back(_masterCells[i].name);

    return lst;
}


vector<string> AreaLoader::getMasterLightsNames() {
    vector<string> lst;

    for (int i = 0; i < _masterLights.size(); i++)
        lst.push_back(_masterLights[i].name);

    return lst;
}


vector<string> AreaLoader::getCellGroupsNames() {
    return _cellGenerator.getCellGroupsNames();
}


Light AreaLoader::getMasterLightById(int id) {
    return _masterLights[id];
}


int AreaLoader::getLightId(string name) {
    if (name == "none" || name == " ") {
        return -1;
    }

    for (int i = 0; i < _masterLights.size(); i++) {
        if (_masterLights[i].name == name) {
            return i;
        }
    }

    return -1;
}

