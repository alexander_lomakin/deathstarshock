

#include "optionloader.h"


using namespace std;


map<string, Section> OptionLoader::_strList;
map<string, NumSection> OptionLoader::_intList;


OptionLoader::OptionLoader()
{
    if (!_loadInfoFromFile("gamesettings.ini"))
        cout << "Warning: cannot load gamesettings.ini file" << endl;
}


int OptionLoader::getNumKey(string section, string name)
{
    if(_intList.find(section) == _intList.end())
        return 0;
    if(_intList[section].members.find(name) == _intList[section].members.end())
        return 0;
    return _intList[section].members[name];
}


string OptionLoader::getStrKey(string section, string name)
{
    if(_strList.find(section) == _strList.end())
        return "";
    if(_strList[section].members.find(name) == _strList[section].members.end())
        return "";
    return _strList[section].members[name];
}


void OptionLoader::setStrKey(string section, string name, string value)
{
    _strList[section].members[name] = value;
}


void OptionLoader::setNumKey(string section, string name, int value)
{
    _intList[section].members[name] = value;
}


string OptionLoader::getCommonSectionName()
{
    return "COMMON";
}


bool OptionLoader::_loadInfoFromFile(string fileName)
{
    ifstream parFile(fileName.c_str());
    string tokenNow;
    string nameNow;
    string sectionNow = getCommonSectionName();
    bool intSection = false;
    if (!parFile)
        return false;

    while (!parFile.eof())
    {
        parFile >> tokenNow;
        if (tokenNow == "[") // SECTION CODE
        {
            parFile >> sectionNow;
            continue;
        }

        if (tokenNow == "-int:") // INT PARAMS
        {
            intSection = true;
            continue;
        }

        if (tokenNow == "-end")
            return true;

        if (tokenNow == "-str:") // INT PARAMS
        {
            intSection = false;
            continue;
        }

        nameNow = tokenNow;

        parFile >> tokenNow;
        if (tokenNow != "=")
        {
            cout << "Error in Config file: Unknown symbol: " << tokenNow << endl;
            return true;
        }

        parFile >> tokenNow;
        if (intSection)
        {
            setNumKey(sectionNow, nameNow, atoi(tokenNow.c_str()));
        }
        else
        {
            setStrKey(sectionNow, nameNow, tokenNow);
        }
        tokenNow = "";
    }
    parFile.close();
    return true;
}


bool OptionLoader::isExistSection(string section)
{
    if(_strList.find(section) == _strList.end() && _intList.find(section) == _intList.end())
        return false;
    return true;
}


bool OptionLoader::isExistKey(string section, string name)
{
    if(_strList.find(section) == _strList.end() && _intList.find(section) == _intList.end())
        return false;
    if(_strList[section].members.find(name) == _strList[section].members.end()
        && _intList[section].members.find(name) == _intList[section].members.end())
        return false;
    return true;
}
