

#include "beastloader.h"


using namespace std;


BeastLoader::BeastLoader() {
    _itemLoader = 0;
    _dropGen    = 0;

    _employedCells = 0;

    for (int i = 0; i < MAX_BEASTS; i++) {
        _pool[i] = 0;
    }
}


BeastLoader::~BeastLoader() {
    for (int i = 0; i < MAX_BEASTS; i++) {
        if (_pool[i] != 0) {
            delete _pool[i];
        }
    }
}


void BeastLoader::init(ItemLoader* itemLoader,DropGenerator* dropGen) {
    _itemLoader = itemLoader;
    _dropGen    = dropGen;

    for (int i = 0; i < MAX_BEASTS; i++) {
        _pool[i] = 0;
    }
}


Creature* BeastLoader::getBeastById(int id) {
    if (_pool[id] != 0) {
        return _pool[id];
    } else {
        return 0;
    }
}


Creature* BeastLoader::getBeastByPos(int globX, int globY) {
    for (int i = 0; i < MAX_BEASTS; i++) {
        if (_pool[i] != 0) {
            if (_pool[i]->x == globX && _pool[i]->y == globY) {
                return _pool[i];
            }
        }
    }

	return 0;
}


bool BeastLoader::canMakeBeast() {
    if (_employedCells < MAX_BEASTS - 1) {
        return true;
    } else {
        return false;
    }
}


int BeastLoader::makeBeastByPointer(Creature* creature) {
    int freeCell = -1;

    for (int i = 0; i < MAX_BEASTS; i++) {
        if (_pool[i] == 0) {
            freeCell = i;
            break;
        }
    }

    if (freeCell == -1) {
        throw 19;
    }

    _employedCells++;

    _pool[freeCell] = creature;

    _pool[freeCell]->id = freeCell;

    return freeCell;
}




void BeastLoader::removeBeast(int id) {
    if (_pool[id] == 0) {
        throw 18;
    }

    delete _pool[id];
    _pool[id] = 0;

    _employedCells--;
}


