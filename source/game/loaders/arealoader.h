#ifndef AREALOADER_H_
#define AREALOADER_H_


#include <string>
#include <vector>
#include <cstring>
#include <irrXML.h>

#include "optionloader.h"
#include "itemloader.h"

#include "eventstack/eventstack.h"

#include "objects/area.h"

#include "objects/masterareacell.h"
#include "objects/light.h"

#include "its.h"

#include "math/coord.h"

#include "mechanics/cellgenerator.h"
#include "mechanics/dropgenerator.h"


using namespace std;


class AreaLoader {
    public:
        AreaLoader();
        ~AreaLoader();

        void init(ItemLoader* itemLoader, DropGenerator* dropGen);
        void initAndLoadAreas(int nLevel, int nX, int nY);

        void shiftCurrentAreas(Direction direction);

        void saveAreaByDirection(Direction direction);

        AreaCell* getCell(int globX, int globY);
        AreaCell* getCell(int localX, int localY, Direction direction);

        GraphicCell* getGraphicCell(int globX, int globY);
        GraphicCell* getGraphicCell(int localX, int localY, Direction direction);

        ExtendedCell* getExtendedCell(int globX, int globY);
        ExtendedCell* getExtendedCell(int localX, int localY, Direction direction);


        int getClosedTerminalType();
        int getFailedTerminalType();

        MasterAreaCell getMasterCell(int type);

        void setAreaVisitsTracking(bool calc);
        void setAreaPathsSaving(bool save);

        void copyOriginalAreasToSession();

        void useOriginalAreas();
        void useSessionAreas();

        vector<string> getMasterCellsNames();
        vector<string> getMasterLightsNames();
        vector<string> getCellGroupsNames();

        Light getMasterLightById(int id);
        int   getLightId(string name);

        void destroyCellAndDrop(int globX, int globY);
    private:
        ItemLoader* _itemLoader;

        vector<MasterAreaCell> _masterCells;
        vector<Light>          _masterLights;

        CellGenerator _cellGenerator;
        DropGenerator* _dropGen;

        Area* _currentAreas[9];

        /* Central area coordinate */
        int _centralAreaX;
        int _centralAreaY;
        int _currentLevel;


        bool _calculateAreaVisits;
        bool _saveAreasPaths;

        string _currentAreaDirectory;
        string _masterCellsFilePath;
        string _masterLightsFilePath;

        string _areaSessionDirectory;
        string _areaOriginalDirectory;

        string _logFilePath;

        string _getAreaPathByDirection(Direction direction);

        void _loadAreaLine(Direction a, Direction b, Direction c);
        void _saveAreaLine(Direction a, Direction b, Direction c);

        void _saveAndUnloadArea(Direction areaDir);
        void _loadAndInitArea(Direction areaDir);

        void _loadMasterCells();
        void _loadMasterLights();
};


#endif
