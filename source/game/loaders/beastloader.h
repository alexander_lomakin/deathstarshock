#ifndef BEASTLOADER_H_
#define BEASTLOADER_H_


#include <vector>
#include <list>
#include <iostream>
#include <string>
#include <cstring>

#include "mechanics/dropgenerator.h"

#include "objects/creature.h"
#include "its.h"
#include "loaders/itemloader.h"


using namespace std;


#define MAX_BEASTS 10
#define PLAYER_ID 0


class BeastLoader {
    public:
        BeastLoader();
        ~BeastLoader();

        void init(ItemLoader* itemLoader, DropGenerator* dropGen);

        Creature* getBeastById(int id);
        Creature* getBeastByPos(int globX, int globY);

        bool canMakeBeast();
        int makeBeastByPointer(Creature* creature);

        void removeBeast(int id);
        unsigned long popDeadBeastsAndGetDemonMindExp();
    private:
        int _employedCells;

        Creature*      _pool[MAX_BEASTS];
        ItemLoader*    _itemLoader;
        DropGenerator* _dropGen;
    };


#endif
