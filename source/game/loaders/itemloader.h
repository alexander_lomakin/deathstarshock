#ifndef ITEMLOADER_H
#define ITEMLOADER_H


#include <vector>
#include <list>
#include <string>
#include <cstring>
#include <iostream>
#include <fstream>
#include <typeinfo>
#include <irrXML.h>

#include "objects/items/commonitem.h"
#include "objects/items/weapon.h"
#include "objects/items/armor.h"
#include "objects/items/ammo.h"
#include "objects/items/component.h"
#include "objects/items/medicament.h"
#include "objects/items/food.h"
#include "its.h"

#include "eventstack/eventstack.h"

#include "math/coord.h"

#include "loaders/optionloader.h"


using namespace std;
using namespace irr;
using namespace irr::io;


    class ItemLoader
    {
    public:
        ItemLoader();
        ~ItemLoader();

        void init();

        void loadItemLibraryFromFile();

        void makeActiveItemFromLibrary(int libraryId, short localX, short localY, Direction area);
        void makeActiveItemFromLibrary(string libItemName, short localX, short localY, Direction area);

        void saveActiveItemsToArea(string areaFilePath, Direction area);
        void loadActiveItemsFromArea(string areaFilePath, Direction area);
        void moveActiveItemsArea(Direction source, Direction destination);

        CommonItem* getActiveItemIdOnPos(int num, short localX, short localY, Direction area);
        void getActiveItemsIdSumOnPos(int& lstSize, short localX, short localY, Direction area);
        CommonItem* getActiveItemById(int id);
        CommonItem* cutActiveItemByid(int id);

        void pushActiveItem(CommonItem* item, short localX, short localY, Direction area);

        CommonItem* createAndCutActiveItemByName(string name);

        void saveLastIdToFile(bool save);
        void resetAndSaveLastActiveId();
    private:
        vector<CommonItem*> _itemLibrary;
        vector<CommonItem*> _activeItems;

        int _activeItemSumInArea[9];

        int _activeItemLastId;
        int _libraryItemId;

        bool _saveLastIdToFile;

        void _loadWeaponFromFile(IrrXMLReader* xml);
        void _loadArmorFromFile(IrrXMLReader* xml);
        void _loadAmmoFromFile(IrrXMLReader* xml);
        void _loadComponentFromFile(IrrXMLReader* xml);
        void _loadMedicamentFromFile(IrrXMLReader* xml);
        void _loadFoodFromFile(IrrXMLReader* xml);

        void _removeEmptyActiveItems();

        void _saveActiveLastIdToFile();
        void _loadActiveLastIdFromFile();

        template <class X> void _makeActiveItem(CommonItem* item, X i, short localX, short localY, Direction area);

        void _recheckActiveItemLastId();

        void _deleteItemIcon(int localX, int localY, Direction direction);
    };



#endif // ITEMLOADER_H
