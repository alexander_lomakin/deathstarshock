#ifndef OPTIONLOADER_H
#define OPTIONLOADER_H


#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>


using namespace std;


namespace
{
    struct Section
    {
        map<string, string> members;
    };

    struct NumSection
    {
        map<string, int> members;
    };
}




    class OptionLoader
    {
    public:
        OptionLoader();
        static int getNumKey(string section, string name);
        static string getStrKey(string section, string name);
        static void setStrKey(string section, string name, string value);
        static void setNumKey(string section, string name, int value);
        static string getCommonSectionName();
        static bool isExistSection(string section);
        static bool isExistKey(string section, string name);
    private:
        static map<string, Section> _strList;
        static map<string, NumSection> _intList;
        static bool _loadInfoFromFile(string fileName);
    };



#endif // OPTIONLOADER_H
