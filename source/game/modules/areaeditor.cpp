

#include "areaeditor.h"


using namespace gui;
using namespace irr;
using namespace core;
using namespace std;


AreaEditor::AreaEditor() {
    // SYS INIT
    _loaders.init();
    _mode         = AE_STD;
    _itemPos      = 0;
    _itemPosMax   = 0;
    _cellGroupNow = -1;
    _cellNow      = 0;
    _lightNow     = -1;
    _targetX      = 5;
    _targetY      = 8;
    _drawGenZone  = true;

    // AREAS LOADING
    _loaders.areaLoader->setAreaVisitsTracking(false);
    _loaders.areaLoader->setAreaPathsSaving(true);
    _loaders.areaLoader->useOriginalAreas();
	_loaders.areaLoader->initAndLoadAreas(0, 0, 0);

    // MAIN LOOP
    _mainLoop();
}



void AreaEditor::_mainLoop() {
    // SYS INIT
    AreaEditorStdDrawer drawer(&_loaders);

    // MAIN LOOP
    IrrlichtDevice* device = IrrlichtBase::getDevice();
    while(device->run() && _continueWork)  {
		IrrlichtBase::startCalcFrames();

        // RENDERING
        drawer.setTarget(_targetX, _targetY);
        drawer.setCombatZoneDrawing(_drawGenZone);
        drawer.step();

        // IO HANDLING
        _onEvent();

		IrrlichtBase::checkFramesPerSecond();
	}
}


void AreaEditor::_onEvent() {
    if (EventReceiver::isKeyDown(KEY_ESCAPE)) {
        _continueWork = false;
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_C)) {
        _pushCell();
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_B)) {
        _pushLight();
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_V)) {
        _pushGenerationGroup();
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_N)) {
        _cleanGenerationGroup();
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_Q)) {
        _updateCellsList();
        _mode = AE_MCELLS;
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_W)) {
        _updateCellGroupList();
        _mode = AE_CELLGROUPS;
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_E)) {
        _updateLightList();
        _mode = AE_LIGHTS;
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_U)) {
        _doPoint();
        _mode = AE_STD;
        return;
    }

    if (EventReceiver::isKeyDown(KEY_DOWN)) {
        _moveCursor(BOTTOM);
        return;
    }

    if (EventReceiver::isKeyDown(KEY_UP)) {
        _moveCursor(TOP);
        return;
    }

    if (EventReceiver::isKeyDown(KEY_LEFT)) {
        _moveCursor(LEFT);
        return;
    }

    if (EventReceiver::isKeyDown(KEY_RIGHT)) {
        _moveCursor(RIGHT);
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_S)) {
        if (_drawGenZone == true) {
            _drawGenZone = false;
        } else {
            _drawGenZone = true;
        }

        return;
    }
}


void AreaEditor::_changeCoord(Direction direction) {
	int localX = _targetX;
	int localY = _targetY;

    // CALCULATE NEW TARGET POSITON
	switch (direction) {
		case LEFT: {
            localX--;
		} break;
		case RIGHT: {
            localX++;
		} break;
		case TOP: {
            localY--;
		} break;
		case BOTTOM: {
            localY++;
		} break;
	}

	if (localX < 0) {
        _targetX = AREA_WIDTH + localX;
	}

	if (localX >= 0 && localX < AREA_WIDTH) {
        _targetX = localX;
	}

	if (localX >= AREA_WIDTH) {
        _targetX = localX - AREA_WIDTH;
	}

	if (localY < 0) {
        _targetY = AREA_HEIGHT + localY;
	}

	if (localY >= 0 && localY < AREA_HEIGHT) {
        _targetY = localY;
	}

	if (localY >= AREA_HEIGHT) {
        _targetY = localY - AREA_HEIGHT;
	}

    // SHIFT AREAS IF TARGET RESEARCH CENTRAL AREA BORDER
	if (getDirectionByGlobCoord(localX, localY) != getDirectionByGlobCoord(_targetX, _targetY)) {
        _loaders.areaLoader->shiftCurrentAreas(direction);
	}
}


void AreaEditor::_pushCell() {
    int globX, globY;
    AreaCell* cell;

	setGlobalCoordByDir(_targetX, _targetY, CENTER, globX, globY);

	cell = _loaders.areaLoader->getCell(globX, globY);
    MasterAreaCell masterCell = _loaders.areaLoader->getMasterCell(cell->type);

	cell->type      = _cellNow;
	cell->additCell = masterCell.additCell;

    _loaders.areaLoader->saveAreaByDirection(CENTER);

    EventStack::pushEvent(ViewEvent(AREA_LOADED, (int)CENTER, 0, 0, 0, ""));
}



void AreaEditor::_pushLight() {
    int globX, globY;
    AreaCell* cell;

	setGlobalCoordByDir(_targetX, _targetY, CENTER, globX, globY);

	cell = _loaders.areaLoader->getCell(globX, globY);

	cell->lightType = _lightNow;

    _loaders.areaLoader->saveAreaByDirection(CENTER);

    EventStack::pushEvent(ViewEvent(AREA_LOADED, (int)CENTER, 0, 0, 0, ""));
}


void AreaEditor::_pushGenerationGroup() {
    int globX, globY;
    AreaCell* cell;

	setGlobalCoordByDir(_targetX, _targetY, getDirectionByGlobCoord(_targetX, _targetY), globX, globY);

	cell = _loaders.areaLoader->getCell(globX, globY);

	cell->generationGroup = _cellGroupNow;

	_loaders.areaLoader->saveAreaByDirection(getDirectionByGlobCoord(_targetX, _targetY));
}


void AreaEditor::_cleanGenerationGroup() {
    int globX, globY;
    AreaCell* cell;

	setGlobalCoordByDir(_targetX, _targetY, getDirectionByGlobCoord(_targetX, _targetY), globX, globY);

	cell = _loaders.areaLoader->getCell(globX, globY);

	cell->generationGroup = -1;
	cell->lightType       = -1;

	_loaders.areaLoader->saveAreaByDirection(getDirectionByGlobCoord(_targetX, _targetY));
}


void AreaEditor::_updateCellGroupList() {
    vector<std::string> lst;
    lst = _loaders.areaLoader->getCellGroupsNames();

    _itemPos    = 0;
    _itemPosMax = lst.size();

    EventStack::pushEvent(ViewEvent(DRAW_ITEM_LIST, 1, 0, 0, 0, "Cell Generation groups"));

    for (int i = 0; i < lst.size(); i++) {
        EventStack::pushEvent(ViewEvent(ADD_ITEM_ELEM, (int)GRAY, 0, 0, 0, lst[i]));
    }
}


void AreaEditor::_updateLightList() {
    vector<std::string> lst = _loaders.areaLoader->getMasterLightsNames();

    EventStack::pushEvent(ViewEvent(DRAW_ITEM_LIST, 1, 0, 0, 0, "Lights"));

    _itemPos    = 0;
    _itemPosMax = lst.size();

    for (int i = 0; i < lst.size(); i++) {
        EventStack::pushEvent(ViewEvent(ADD_ITEM_ELEM, (int)GRAY, 0, 0, 0, lst[i]));
    }
}


void AreaEditor::_updateCellsList() {
    vector<std::string> lst = _loaders.areaLoader->getMasterCellsNames();

    EventStack::pushEvent(ViewEvent(DRAW_ITEM_LIST, 1, 0, 0, 0, "Master Cells"));

    _itemPos    = 0;
    _itemPosMax = lst.size();

    for (int i = 0; i < lst.size(); i++) {
        EventStack::pushEvent(ViewEvent(ADD_ITEM_ELEM, (int)GRAY, 0, 0, 0, lst[i]));
    }
}


void AreaEditor::_moveCursor(Direction direction) {
    switch (_mode) {
        case AE_STD: {
            _changeCoord(direction);
        } break;
        case AE_CELLGROUPS:
        case AE_MCELLS:
        case AE_LIGHTS: {
            _moveMenuPointer(direction);
        } break;
    }
}


void AreaEditor::_moveMenuPointer(Direction dir) {
    if (dir == TOP && _itemPos >= 1) {
        _itemPos--;
    }

    if (dir == BOTTOM && _itemPos < _itemPosMax-1) {
        _itemPos++;
    }

    EventStack::pushEvent(ViewEvent(SELECT_NEW_ELEM, _itemPos, 0, 0, 0, ""));
}


void AreaEditor::_doPoint() {
    switch (_mode) {
        case AE_CELLGROUPS: {
            _cellGroupNow = _itemPos;
        } break;
        case AE_MCELLS: {
            _cellNow = _itemPos;
        } break;
        case AE_LIGHTS: {
            _lightNow = _itemPos;
        } break;
    }

    EventStack::pushEvent(ViewEvent(DRAW_ITEM_LIST, 0, 0, 0, 0, " "));
}
