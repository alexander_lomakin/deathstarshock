#ifndef AREAEDITOR_H
#define AREAEDITOR_H


#include <string>
#include <vector>

#include "modulecarcass.h"

#include "areaeditorstddrawer.h"

#include "math/coord.h"


using namespace std;


    enum AreaEditorMode { AE_STD, AE_MCELLS, AE_CELLGROUPS, AE_LIGHTS };


    class AreaEditor : protected ModuleCarcass {
    public:
        AreaEditor();
    private:
        int _itemPos;
        int _itemPosMax;

        int _cellNow;
        int _lightNow;
        int _cellGroupNow;

        AreaEditorMode _mode;
        int _targetX;
        int _targetY;

        bool _drawGenZone;

        void _changeCoord(Direction direction);

        void _pushCell();

        void _pushLight();

        void _updateCellGroupList();
        void _updateLightList();
        void _updateCellsList();

        void _moveCursor(Direction direction);

        void _moveMenuPointer(Direction dir);

        void _pushGenerationGroup();

        void _cleanGenerationGroup();

        void _doPoint();

        void _mainLoop();
        void _onEvent();
    };



#endif // AREAEDITOR_H
