#ifndef CORE_H_
#define CORE_H_


#include <boost/thread/thread.hpp>
#include "modulecarcass.h"

#include "gameloop.h"
#include "demonmind/demonmind.h"
#include "monsters/spawn.h"

#include "eventstack/eventstack.h"
#include "drawer/stddrawer.h"


    class GameCore : protected ModuleCarcass {
    public:
        GameCore(string selectedClass);
    private:
        string _playerClass;

        void _mainLoop();
        void _onEvent();

        bool _drawCombatZone;
    };


#endif
