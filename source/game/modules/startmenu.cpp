

#include "startmenu.h"


using namespace irr;


StartMenu::StartMenu() {
    // sys init
    _itemPos    = 0;
    _itemPosMax = 3;
    _updateMenu();

    // MAIN LOOP
    _mainLoop();
}


void StartMenu::_mainLoop() {
    // SYS INIT
    StartMenuDrawer drawer(&_loaders);

    // MAIN LOOP
    IrrlichtDevice* device = IrrlichtBase::getDevice();
    while(device->run() && _continueWork) {
		IrrlichtBase::startCalcFrames();

        // RENDERING
        drawer.step();

        // IO HANDLING
        _onEvent();

		IrrlichtBase::checkFramesPerSecond();
	}
}


void StartMenu::_onEvent() {
    if (EventReceiver::isKeyDown(KEY_UP)) {
        _moveMenuPointer(TOP);
        return;
    }

    if (EventReceiver::isKeyDown(KEY_DOWN)) {
        _moveMenuPointer(BOTTOM);
        return;
    }

    if (EventReceiver::isKeyDown(KEY_RETURN)) {
        _doPoint();
        return;
    }

    if (EventReceiver::isKeyDown(KEY_ESCAPE)) {
        _updateMenu();
        _itemPosMax = 3;
        _itemPos    = 0;
    }
}


void StartMenu::_updateMenu() {
    _status = MAINMENU;
    EventStack::pushEvent(ViewEvent(DRAW_ITEM_LIST, 1, 0, 0, 0, "DEATHSTARSHOCK"));
    EventStack::pushEvent(ViewEvent(ADD_ITEM_ELEM, (int)GRAY, 0, 0, 0, "NEW GAME"));
    EventStack::pushEvent(ViewEvent(ADD_ITEM_ELEM, (int)DARK_RED, 0, 0, 0, "AREA EDITOR"));
    EventStack::pushEvent(ViewEvent(ADD_ITEM_ELEM, (int)GRAY, 0, 0, 0, "QUIT"));
}


void StartMenu::_updateClasses() {
    _status = CLASSES;
    EventStack::pushEvent(ViewEvent(DRAW_ITEM_LIST, 1, 0, 0, 0, "Choose your class: "));
    EventStack::pushEvent(ViewEvent(ADD_ITEM_ELEM, (int)GRAY, 0, 0, 0, "COMMANDOS (knife)"));
    EventStack::pushEvent(ViewEvent(ADD_ITEM_ELEM, (int)GRAY, 0, 0, 0, "SCOUT (Core Pistol, armor and ammo)"));
}


void StartMenu::_moveMenuPointer(Direction dir) {
    if (dir == TOP) {
        if (_itemPos >= 1) {
            _itemPos--;
        }
    }

    if (dir == BOTTOM) {
        if (_itemPos < _itemPosMax-1) {
            _itemPos++;
        }
    }

    IrrlichtBase::sound("data\\audio\\sounds\\swapweapon.wav");
    EventStack::pushEvent(ViewEvent(SELECT_NEW_ELEM, _itemPos, 0, 0, 0, ""));
}


void StartMenu::_doPoint() {
    IrrlichtBase::sound("data\\audio\\sounds\\opendoor.wav");
    switch (_status) {
        case MAINMENU: {
            if (_itemPos == 0) {
                _status     = CLASSES;
                _updateClasses();
                _itemPosMax = 2;
                _itemPos    = 0;
                return;
            }

            if (_itemPos == 1) {
                //return;
                AreaEditor* editor =  new AreaEditor;
                delete editor;
            }

            if (_itemPos == 2) {
                _continueWork = false;
            }
        } break;
        case CLASSES: {
            if (_itemPos == 0) {
                GameCore* core =  new GameCore("Commandos");
                delete core;
                _continueWork = false;
            }

            if (_itemPos == 1) {
                GameCore* core =  new GameCore("Scout");
                delete core;
                _continueWork = false;
            }

            _updateMenu();
            _itemPosMax = 3;
            _itemPos    = 0;
        } break;
    }
}
