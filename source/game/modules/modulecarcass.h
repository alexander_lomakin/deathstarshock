#ifndef MODULECARCASS_H_
#define MODULECARCASS_H_


#include "irrlichtbase.h"
#include "eventreceiver.h"
#include "font.h"

#include "loaders/loaders.h"


using namespace std;


    class ModuleCarcass {
    public:
        ModuleCarcass();
    protected:
        Loaders _loaders;

        bool _continueWork;

        virtual void _mainLoop() = 0;
        virtual void _onEvent()  = 0;
    };



#endif
