

#include "areaeditorstddrawer.h"


using namespace irr;
using namespace gui;


AreaEditorStdDrawer::AreaEditorStdDrawer(Loaders* loader) : StdDrawer(loader) {
    _targetX = 8;
    _targetY = 8;

    _initGui();
}


void AreaEditorStdDrawer::_checkEvents()
{
    while (EventStack::canPopViewEvent())
    {
        ViewEvent event = EventStack::popViewEvent();

        if (event.type == AREA_LOADED)
            _recheckAreaSprites(event);

        if (event.type == DRAW_ITEM_LIST)
            _drawItemList(event);

        if (event.type == ADD_ITEM_ELEM)
            _addItemElem(event);

        if (event.type == SELECT_NEW_ELEM)
            _selectNewItem(event);
    }
}



void AreaEditorStdDrawer::setTarget(int nTargetX, int nTargetY)
{
    _targetX = nTargetX;
    _targetY = nTargetY;
}


void AreaEditorStdDrawer::_initGui()
{
    int width  = IrrlichtBase::getWindowWidth();
    int height = IrrlichtBase::getWindowHeight();

    _guiList = new GuiList;

    _guiManager.addElem(_guiList);

    _guiList->setSize(200, 500);

    _guiList->drawElem(false);

    _guiList->setPosition(width/2 - 100, 10);
}


void AreaEditorStdDrawer::_drawGameArea()
{
    GraphicCell *grCell;

    int globX, globY;
    int animOffsetX, animOffsetY;
    int sx = 0;

    _setTargetPos(globX, globY);
    _setAnimOffset(animOffsetX, animOffsetY);


	for (int i = globX - _drawingRadiusX; i <= globX + _drawingRadiusX; i++)
	{
		int sy = 0;
		for (int j = globY - _drawingRadiusY; j <= globY + _drawingRadiusY; j++)
		{
			grCell  = _loader->areaLoader->getGraphicCell(i, j);

            if (grCell->additSpriteId != -1)
				_drawTail(sx+_offsetX+animOffsetX, sy+_offsetY+animOffsetY, grCell->additSpriteId);

			if (grCell->spriteId != -1)
				_drawTail(sx+_offsetX+animOffsetX, sy+_offsetY+animOffsetY, grCell->spriteId);


			sy += _stdTailY;
		}
		sx += _stdTailX;
	}
}


void AreaEditorStdDrawer::_drawActiveObjects()
{
    int globX, globY;
    int animOffsetX, animOffsetY;
    int sx = 0;

    _setTargetPos(globX, globY);
    _setAnimOffset(animOffsetX, animOffsetY);

    for (int i = globX - _drawingRadiusX; i <= globX + _drawingRadiusX; i++)
	{
		int sy = 0;
		for (int j = globY - _drawingRadiusY; j <= globY + _drawingRadiusY; j++)
		{

            if (i == _targetX && j == _targetY)
            {
                _drawExtTail(sx+_offsetX+animOffsetX, sy+_offsetY+animOffsetY, 0);
            }


			sy += _stdTailY;
		}
		sx += _stdTailX;
	}
}


void AreaEditorStdDrawer::_drawFogOfWar()
{

}


void AreaEditorStdDrawer::_drawComZone()
{
    ExtendedCell *extCell;

	int globX, globY;
    int animOffsetX, animOffsetY;
    int sx = 0;

    _setTargetPos(globX, globY);
    _setAnimOffset(animOffsetX, animOffsetY);

	for (int i = globX - _drawingRadiusX; i <= globX + _drawingRadiusX; i++)
	{
		int sy = 0;
		for (int j = globY - _drawingRadiusY; j <= globY + _drawingRadiusY; j++)
		{
			AreaCell* cell = _loader->areaLoader->getCell(i, j);

			if (cell->generationGroup >= 0)
            {
                _drawMask(sx+_offsetX+animOffsetX, 5+sy+_offsetY+animOffsetY, CTSC(80, (COLOR)cell->generationGroup));
                Font::drawBig(5+sx+_offsetX+animOffsetX, 5+sy+_offsetY+animOffsetY,
                               ITS(cell->generationGroup), irr::video::SColor(255,255,255,255));
            }

            if (cell->lightType != -1)
            {
                _drawMask(sx+_offsetX+animOffsetX, 5+sy+_offsetY+animOffsetY, CTSC(40, DARK_BLUE));
                Font::drawBig(15+sx+_offsetX+animOffsetX, 15+sy+_offsetY+animOffsetY,
                               ITS(cell->lightType), irr::video::SColor(255,255,255,255));
            }

			sy += _stdTailY;
		}
		sx += _stdTailX;
	}
}


void AreaEditorStdDrawer::_setTargetPos(int& globX, int& globY)
{
    globX = _targetX;
    globY = _targetY;
}


void AreaEditorStdDrawer::_drawInterface()
{
    _guiManager.draw(_driver);
}


void AreaEditorStdDrawer::_drawItemList(ViewEvent event)
{
    _guiList->drawElem((bool)event.argA);
    _guiList->clearList();
    _guiList->setSelectedItem(0);
    _guiList->setTitle(event.argS, YELLOW);
}


void AreaEditorStdDrawer::_addItemElem(ViewEvent event)
{
    _guiList->addItem(event.argS, (COLOR)event.argA);
}


void AreaEditorStdDrawer::_selectNewItem(ViewEvent event)
{
   _guiList->setSelectedItem(event.argA);
}


void AreaEditorStdDrawer::_recheckAreaSprites(ViewEvent event)
{
    SpriteDefiner definer(_loader->areaLoader);
    int globX, globY;

    for (int i = 0; i < AREA_WIDTH; i++)
		for (int j = 0; j < AREA_HEIGHT; j++)
		{
			GraphicCell* cell         = _loader->areaLoader->getGraphicCell(i, j, (Direction)event.argA);
			AreaCell* areacell        = _loader->areaLoader->getCell(i, j, (Direction)event.argA);
			MasterAreaCell masterCell = _loader->areaLoader->getMasterCell(areacell->type);
			MasterAreaCell addMsCell  = _loader->areaLoader->getMasterCell(areacell->additCell);

            setGlobalCoordByDir(i, j , (Direction)event.argA, globX, globY);

			if (masterCell.addit == "wall")
                cell->spriteId = definer.getWallSprite(globX, globY, _masterCellsView[areacell->type].spriteOffset);
            else
            {
                if (masterCell.addit == "door")
                    cell->spriteId = definer.getDoorSprite(globX, globY, _masterCellsView[areacell->type].spriteOffset);
                else
                    cell->spriteId = _masterCellsView[areacell->type].spriteOffset;
            }

            if (addMsCell.addit == "wall")
                cell->additSpriteId = definer.getWallSprite(globX, globY, _masterCellsView[areacell->additCell].spriteOffset);
            else
            {
                if (addMsCell.addit == "door")
                    cell->additSpriteId = definer.getDoorSprite(globX, globY, _masterCellsView[areacell->additCell].spriteOffset);
                else
                    cell->additSpriteId = _masterCellsView[areacell->additCell].spriteOffset;
            }
		}
}
