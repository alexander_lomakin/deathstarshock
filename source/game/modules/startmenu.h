#ifndef STARTMENU_H
#define STARTMENU_H


#include "modulecarcass.h"
#include "core.h"
#include "areaeditor.h"

#include "startmenudrawer.h"

    enum StartMenuStatus { MAINMENU, CLASSES };


    class StartMenu : protected ModuleCarcass {
    public:
        StartMenu();
    private:
        int _itemPos;
        int _itemPosMax;

        StartMenuStatus _status;

        void _mainLoop();
        void _onEvent();

        void _updateMenu();
        void _updateClasses();

        void _moveMenuPointer(Direction dir);
        void _doPoint();
    };


#endif // STARTMENU_H
