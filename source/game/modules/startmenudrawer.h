#ifndef STARTMENUDRAWER_H
#define STARTMENUDRAWER_H


#include "drawer/drawercarcass.h"

#include "loaders/arealoader.h"

#include "spritescaling.h"



    class StartMenuDrawer : public DrawerCarcass
    {
    public:
        StartMenuDrawer(Loaders* loader);
    private:
        GuiList*       _guiList;
        GuiStaticText* _guiGameVersion;

        video::ITexture* _background;

        void _initGui();

        void _checkEvents();
        void _drawGameArea();
        void _drawActiveObjects();
        void _drawFogOfWar();
        void _drawInterface();
        void _drawComZone();
        void _calcActiveObjects();

        void _drawItemList(ViewEvent event);
        void _addItemElem(ViewEvent event);
        void _selectNewItem(ViewEvent event);
    };



#endif // STARTMENUDRAWER_H
