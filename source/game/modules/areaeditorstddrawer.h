#ifndef AREAEDITORSTDDRAWER_H
#define AREAEDITORSTDDRAWER_H


#include "drawer/stddrawer.h"

#include "drawer/spritedefiner.h"


#include "loaders/arealoader.h"


    class AreaEditorStdDrawer : public StdDrawer
    {
    public:
        AreaEditorStdDrawer(Loaders* loader);

        void setTarget(int nTargetX, int nTargetY);
    private:

        void _initGui();

        int _targetX;
        int _targetY;

        GuiList* _guiList;

        void _setTargetPos(int& globX, int& globY);

        void _checkEvents();
        void _drawGameArea();
        void _drawActiveObjects();
        void _drawFogOfWar();
        void _drawComZone();
        void _drawInterface();

        void _recheckAreaSprites(ViewEvent event);
        void _drawItemList(ViewEvent event);
        void _addItemElem(ViewEvent event);
        void _selectNewItem(ViewEvent event);
    };



#endif // AREAEDITORSTDDRAWER_H
