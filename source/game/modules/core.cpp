

#include "core.h"


using namespace std;


GameCore::GameCore(string selectedClass) {
    // BASE INIT
    _loaders.init();
    _drawCombatZone = false;
    _playerClass = selectedClass;

    // ITEMS LOADING
	_loaders.itemLoader->loadItemLibraryFromFile();
	_loaders.itemLoader->resetAndSaveLastActiveId();

    // AREAS LOADING
    _loaders.areaLoader->setAreaVisitsTracking(true);
    _loaders.areaLoader->setAreaPathsSaving(false);
    _loaders.areaLoader->useSessionAreas();
	_loaders.areaLoader->copyOriginalAreasToSession();
	_loaders.areaLoader->initAndLoadAreas(0, 0, 0);

    // MAIN CYCLE
	_mainLoop();
}


void GameCore::_mainLoop() {
    // SYS INIT
    bool      endOfWork = false; // need for correct exit from game cycle
	StdDrawer draw(&_loaders);
	LosCalc   losCalc(_loaders.areaLoader);
	GameLogic gameLogic(&_loaders);
    DemonMind demonMind(&_loaders, &gameLogic);

    // CREATE PLAYER
    spawnPlayer(_playerClass, 5, 8, &_loaders, &gameLogic);


    //spawnMonster("Ghoul", 5, 11, &_loaders, &gameLogic);

    // PUSHING BASE VIEW EVENTS
    EventStack::pushEvent(ViewEvent(SET_TARGET_BEAST, PLAYER_ID, 0, 0, 0, ""));
    EventStack::pushEvent(ViewEvent(RECALC_LOS, 5, 8, 0, 0, ""));
    EventStack::pushEvent(ViewEvent(ACTIVEOBJ_CREATED, PLAYER_ID, 0, 5, 8, "beast"));

    // START GAME LOOP
    boost::thread workerThread(&gameLoop, &_loaders, &gameLogic, &demonMind, &endOfWork);

    // START CORE LOOP - RENDERING AND IO HANDLING
    IrrlichtDevice* device = IrrlichtBase::getDevice();
	while(device->run() && _continueWork) {
        IrrlichtBase::startCalcFrames();

        // RENDERING
        draw.setCombatZoneDrawing(_drawCombatZone);
        draw.step();

        // IO HANDLING
        _onEvent();

        // CORRECT CYCLE TERMINATE
        if (endOfWork) {
            break;
        }

		IrrlichtBase::checkFramesPerSecond();
	}

	endOfWork = true;

    // TERMINTATE GAME LOOP AND WAITING FOR IT END
    EventStack::pushEvent(ModelEvent(END_OF_WORK, 0, 0, 0, 0, ""));
    EventStack::pushEvent(ModelEvent(END_OF_WORK, 0, 0, 0, 0, ""));
    EventStack::pushEvent(ModelEvent(END_OF_WORK, 0, 0, 0, 0, ""));
    EventStack::pushEvent(ModelEvent(END_OF_WORK, 0, 0, 0, 0, ""));
    EventStack::pushEvent(ModelEvent(END_OF_WORK, 0, 0, 0, 0, ""));
	workerThread.join();

	// CLEAR EVENT STACKS
	EventStack::clearStacks();
}


void GameCore::_onEvent() {
    if (EventReceiver::isKeyDown(KEY_F10)) {
        if (_drawCombatZone) {
            _drawCombatZone = false;
        } else {
            _drawCombatZone = true;
        }
        return;
    }

    if (EventReceiver::isKeyDown(KEY_DOWN) || EventReceiver::isKeyDown(KEY_NUMPAD2)) {
        EventStack::pushEvent(ModelEvent(MOVE_PLAYER, BOTTOM, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_UP) || EventReceiver::isKeyDown(KEY_NUMPAD8)) {
        EventStack::pushEvent(ModelEvent(MOVE_PLAYER, TOP, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_LEFT) || EventReceiver::isKeyDown(KEY_NUMPAD4)) {
        EventStack::pushEvent(ModelEvent(MOVE_PLAYER, LEFT, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_RIGHT) || EventReceiver::isKeyDown(KEY_NUMPAD6)) {
        EventStack::pushEvent(ModelEvent(MOVE_PLAYER, RIGHT, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_NUMPAD1)) {
        EventStack::pushEvent(ModelEvent(MOVE_PLAYER, LEFT_BOTTOM, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_NUMPAD7)) {
        EventStack::pushEvent(ModelEvent(MOVE_PLAYER, LEFT_TOP, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_NUMPAD9)) {
        EventStack::pushEvent(ModelEvent(MOVE_PLAYER, RIGHT_TOP, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_NUMPAD3)) {
        EventStack::pushEvent(ModelEvent(MOVE_PLAYER, RIGHT_BOTTOM, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_H)) {
        EventStack::pushEvent(ModelEvent(HACK_TERMINAL, 0, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_C)) {
        EventStack::pushEvent(ModelEvent(CHAR_LIST, 0, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_G)) {
        EventStack::pushEvent(ModelEvent(GET_ITEM, 0, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_H)) {
        EventStack::pushEvent(ModelEvent(HACK_TERMINAL, 0, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_U)) {
        EventStack::pushEvent(ModelEvent(USE_ITEM, 0, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_ESCAPE)) {
        EventStack::pushEvent(ModelEvent(CLOSE, 0, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_TAB)) {
        EventStack::pushEvent(ModelEvent(RUN_MODE, 0, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_A)) {
        EventStack::pushEvent(ModelEvent(ASSAULT_FORWARD, 0, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_W)) {
        EventStack::pushEvent(ModelEvent(WAIT, 0, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_I)) {
        EventStack::pushEvent(ModelEvent(INVENTORY, 0, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_D)) {
        EventStack::pushEvent(ModelEvent(DROP_ITEM, 0, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_ESCAPE)) {
        EventStack::pushEvent(ModelEvent(CLOSE, 0, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_RETURN)) {
        EventStack::pushEvent(ModelEvent(ENTER, 0, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_E) || EventReceiver::isKeyDown(KEY_RETURN)) {
        EventStack::pushEvent(ModelEvent(EQUIP_ITEM, 0, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_F)) {
        EventStack::pushEvent(ModelEvent(GUNSHOT, 0, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_R)) {
        EventStack::pushEvent(ModelEvent(RELOAD, 0, 0, 0, 0, ""));
        return;
    }

    if (EventReceiver::isKeyDown(KEY_KEY_Z)) {
        EventStack::pushEvent(ModelEvent(SWAP_WEAPON, 0, 0, 0, 0, ""));
        return;
    }
}

