

#include "modulecarcass.h"



ModuleCarcass::ModuleCarcass()
{
    Font::loadAndInitFont(OptionLoader::getStrKey("ModuleCarcass", "FontPath"));
    _continueWork = true;
    IrrlichtBase::setFramesPerSecond(38);
}
