

#include "startmenudrawer.h"



StartMenuDrawer::StartMenuDrawer(Loaders* loader) : DrawerCarcass(loader) {
    _initGui();
}


void StartMenuDrawer::_checkEvents()
{
    while (EventStack::canPopViewEvent())
    {
        ViewEvent event = EventStack::popViewEvent();

        if (event.type == DRAW_ITEM_LIST)
            _drawItemList(event);

        if (event.type == ADD_ITEM_ELEM)
            _addItemElem(event);

        if (event.type == SELECT_NEW_ELEM)
            _selectNewItem(event);
    }
}


void StartMenuDrawer::_drawGameArea()
{

}


void StartMenuDrawer::_drawActiveObjects()
{

}


void StartMenuDrawer::_drawFogOfWar()
{

}


void StartMenuDrawer::_drawInterface()
{


    int width  = IrrlichtBase::getWindowWidth();
    int height = IrrlichtBase::getWindowHeight();

    _driver->draw2DImage(_background, core::position2d< s32 >((width/2)-962, (height/2)-512),core::rect < s32 >(0, 0, 1924, 1024), 0,
                video::SColor(255,255, 255, 255), true);



    _guiManager.draw(_driver);
}


void StartMenuDrawer::_drawComZone()
{

}


void StartMenuDrawer::_calcActiveObjects()
{

}


void StartMenuDrawer::_drawItemList(ViewEvent event)
{
    _guiList->drawElem((bool)event.argA);
    _guiList->clearList();
    _guiList->setSelectedItem(0);
    _guiList->setTitle(event.argS, YELLOW);
}


void StartMenuDrawer::_addItemElem(ViewEvent event)
{
    _guiList->addItem(event.argS, (COLOR)event.argA);
}


void StartMenuDrawer::_selectNewItem(ViewEvent event)
{
   _guiList->setSelectedItem(event.argA);
}


void StartMenuDrawer::_initGui()
{
    int width  = IrrlichtBase::getWindowWidth();
    int height = IrrlichtBase::getWindowHeight();

    _guiList = new GuiList;
    _guiGameVersion = new GuiStaticText;

    _guiManager.addElem(_guiList);
    _guiManager.addElem(_guiGameVersion);

    _guiList->setSize(200, 200);
    _guiList->setPosition(width/2 - 100, height/2);
    _guiList->useDoublePointer(true);

    _guiGameVersion->drawElem(true);
    _guiGameVersion->setPosition(2, height - 20);
    _guiGameVersion->setText("demo, ver_18_80s", DARK_CYAN);

    _background = IrrlichtBase::loadTexture("data/graphic/background_st.png");

}

