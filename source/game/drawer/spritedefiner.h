#ifndef SPRITEDEFINER_H_
#define SPRITEDEFINER_H_


#include "loaders/arealoader.h"


 class SpriteDefiner
    {
        AreaLoader* _areaLoader;
        bool isWall(int type, int globX, int globY);
        bool isWall(int globX, int globY);
    public:
        SpriteDefiner(AreaLoader* areaLoader);
        int getWallSprite(int globX, int globY, int offset);
        int getDoorSprite(int globX, int globY, int offset);
    };



#endif
