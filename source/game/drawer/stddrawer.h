#ifndef DRAWER_H_
#define DRAWER_H_


#include "objects/graphicobject.h"


#include "loaders/arealoader.h"
#include "loaders/optionloader.h"

#include "its.h"

#include "irrlichtbase.h"
#include "spritesheet.h"
#include "font.h"

#include "eventstack/eventstack.h"
#include "math/coord.h"
#include "mechanics/loscalc.h"

#include "spritedefiner.h"
#include "mastercellview.h"
#include "drawercarcass.h"
#include "losdrawer.h"
#include "interfacevars.h"
#include "dolldrawer.h"

#include "gunshotparticle.h"


using namespace irr;
using namespace std;


    class StdDrawer : public DrawerCarcass {
    public:
        StdDrawer(Loaders* loader);

    private:
        SpriteSheet _areaTiles;
        SpriteSheet _extTiles;
        SpriteSheet _creatureTiles;
        SpriteSheet _bloodTiles;
        SpriteSheet _itemTiles;
        SpriteSheet _gunshotTiles;

        DollDrawer _dollDrawer;

        int _targetObjectId;

        InterfaceVars _interfaceVars;

        LosDrawer _losDrawer;

        void _initGuiElems();

        Animation _fire;
        Animation _terminal;

        // MAIN BLOCK
        void _checkEvents();
        void _drawGameArea();
        void _drawActiveObjects();
        void _drawFogOfWar();
        void _drawComZone();
        void _drawInterface();
        void _calcActiveObjects();


        // EVENT WORKERS
        void _recheckAreaSprites(ViewEvent event);
        void _createActiveObject(ViewEvent event);
        void _moveBeast(ViewEvent event);
        void _changeBeastDir(ViewEvent event);
        void _beastAttack(ViewEvent event);
        void _chatMsg(ViewEvent event);
        void _setTargetBeast(ViewEvent event);
        void _recalcLos(ViewEvent event);
        void _beastInjured(ViewEvent event);
        void _beastGetExp(ViewEvent event);
        void _playerTiredness(ViewEvent event);
        void _playerStatus(ViewEvent event);
        void _itemCreated(ViewEvent event);
        void _itemDeleted(ViewEvent event);
        void _drawItemList(ViewEvent event);
        void _addItemElem(ViewEvent event);
        void _selectNewItem(ViewEvent event);
        void _equipWeapon(ViewEvent event);
        void _equipArmor(ViewEvent event);
        void _singleShoot(ViewEvent event);
        void _singleShootMiss(ViewEvent event);
        void _shrapnelShoot(ViewEvent event);
        void _updateCellSprite(ViewEvent event);
        void _playSound(ViewEvent event);
        void _deadBeast(ViewEvent event);
        void _startHack(ViewEvent event);
        void _drawCharList(ViewEvent event);

        protected:

        int _drawingRadiusX;
        int _drawingRadiusY;

        int _offsetX;
        int _offsetY;

        int _stdTailX;
        int _stdTailXHalf;
        int _stdTailY;
        int _stdTailYHalf;

        int _scaleSize;

        int _oldX;
        int _oldY;

        bool _isHacking;

        // DRAWING ZONE POSTION BLOCK
        virtual void _setTargetPos(int& globX, int& globY);
        void _setAnimOffset(int& animX, int& animY);


        // TAIL DRAWING GROUP
        void _drawTail(int globX, int globY, int spriteId);
        void _drawExtTail(int globX, int globY, int spriteId);
        void _drawBloodTail(int globX, int globY, int spriteId);
        void _drawCreature(int globX, int globY, int spriteId);
        void _drawItem(int globX, int globY, int spriteId);
        void _drawParticle(int globX, int globY, int spriteId);
        void _drawMask(int globX, int globY, video::SColor color);

        void _drawBulletParticle  (int globX, int globY, int stepNow, LineEquation* lnEq);
        void _drawShrapnelParticle(int globX, int globY, int stepNow, LineEquation* lnEq);
    };


#endif
