#ifndef CHATMSG_H
#define CHATMSG_H


#include <string>

#include "colors/colors.h"


using namespace std;


    struct ChatMsg
    {
        std::string msg;
        COLOR color;
    };


#endif // CHATMSG_H
