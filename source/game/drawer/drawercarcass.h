#ifndef DRAWERCARCASS_H
#define DRAWERCARCASS_H


#include <irrXML.h>
#include <iostream>
#include <vector>
#include <list>

#include "irrlichtbase.h"
#include "spritesheet.h"
#include "font.h"

#include "loaders/loaders.h"

#include "colors/colors.h"

#include "objects/graphicobject.h"

#include "mastercellview.h"
#include "chatmsg.h"
#include "itemview.h"
#include "gunshotparticle.h"

#include "guisystem/guimanager.h"
#include "guisystem/guilist.h"
#include "guisystem/guistatictext.h"
#include "guisystem/guiprogressbar.h"


using namespace irr;
using namespace std;


class DrawerCarcass {
    public:
        DrawerCarcass(Loaders* loader);

        void step();

        void setCombatZoneDrawing(bool draw);
    protected:
        video::IVideoDriver* _driver;

        Loaders* _loader;

        GuiManager _guiManager;

        list<GunshotParticle> _gunshotParticles;
        vector<ChatMsg>          _chat;

        vector<MasterCellView>   _masterCellsView;
        vector<ItemView>         _itemView;

        bool _drawCombatZone;

        virtual void _checkEvents()       = 0;

        virtual void _drawGameArea()      = 0;
        virtual void _drawActiveObjects() = 0;
        virtual void _drawFogOfWar()      = 0;
        virtual void _drawComZone()       = 0;

        virtual void _drawInterface()     = 0;

        virtual void _calcActiveObjects() = 0;

        void _loadMasterCellsFromFile(string filePath);
        void _loadMasterItemsFromFile(string filePath);
    };


#endif // DRAWERCARCASS_H
