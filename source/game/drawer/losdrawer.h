#ifndef LOSDRAWER_H
#define LOSDRAWER_H


#include "loaders/arealoader.h"

#include "irrlichtbase.h"


namespace
{
    struct Matrix
    {
        short alpha, red, green, blue;
    };
};



    class LosDrawer
    {
    public:
        LosDrawer();

        void init(AreaLoader* _loader, video::IVideoDriver* driver, int scaleSize);

        void initMatrix(int stGlobX, int stGlobY, int radiusX, int radiusY);

        void pushCell(int globX, int globY, int alpha, int red, int green, int blue);

        void scaleMatrix(int aX, int aY, int bX, int bY);

        void drawLos(int stPosX, int stPosY);

    private:
        Matrix _matrixA[208][208];
        Matrix _matrixB[208][208];

        video::IImage* _additImg;

        video::ITexture* _darkedZone;

        int _scaleSize;


        void _render();

        video::IVideoDriver* _driver;

        void _getSumFromMatrixB(short x, short y, short bX, short bY);

        AreaLoader* _areaLoader;
    };



#endif // LOSDRAWER_H
