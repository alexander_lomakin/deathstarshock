

#include "spritedefiner.h"


bool SpriteDefiner::isWall(int type, int globX, int globY)
{
	AreaCell* cell            = _areaLoader->getCell(globX, globY);
    MasterAreaCell masterCell = _areaLoader->getMasterCell(cell->type);

	if (cell == 0)
		return false;

	if (cell->type == type || masterCell.addit == "wall")
		return true;
	return false;
}


bool SpriteDefiner::isWall(int globX, int globY)
{
    AreaCell* cell            = _areaLoader->getCell(globX, globY);
    MasterAreaCell masterCell = _areaLoader->getMasterCell(cell->type);

	if (cell == 0)
		return false;

	if (masterCell.addit == "wall")
		return true;
	return false;
}


SpriteDefiner::SpriteDefiner(AreaLoader* areaLoader)
{
	_areaLoader = areaLoader;
}


int SpriteDefiner::getWallSprite(int globX, int globY, int offset)
{
    int sprite =  0 + offset;
	AreaCell* cell = _areaLoader->getCell(globX, globY);
	int tp = cell->type;

    // вертикальный тунель
    if (isWall(tp, globX, globY-1) || isWall(tp, globX, globY+1))
         sprite = 4 + offset;

    // тупик верт. стены сверху
    if (isWall(tp, globX, globY+1) && !isWall(tp, globX, globY-1))
         sprite = 12 + offset;

    // тупик верт. стены снизу
    if (isWall(tp, globX, globY-1) && !isWall(tp, globX, globY+1))
        sprite = 5 + offset;

    // тупик гор. стены слева
    if (isWall(tp, globX+1, globY) && !isWall(tp, globX-1, globY))
         sprite = 13 + offset;

    // тупик гор. стены слева
    if (isWall(tp, globX-1, globY) && !isWall(tp, globX+1, globY))
         sprite = 14 + offset;

    // поворот верт. стены влево
    if (isWall(tp, globX-1, globY) && isWall(tp, globX, globY+1))
         sprite = 1 + offset;

    // поворот верт. стены вправо
    if (isWall(tp, globX+1, globY) && isWall(tp, globX, globY+1))
         sprite = 2 + offset;

    //поворот гор. стены вправо
    if (isWall(tp, globX, globY-1) && isWall(tp, globX+1, globY))
         sprite = 10 + offset;

    //поворот гор. стены влево
    if (isWall(tp, globX, globY-1) && isWall(tp, globX-1, globY))
        sprite = 11 + offset;

    // треугольник - вершина вниз
    if (isWall(tp, globX-1, globY) && isWall(tp, globX+1, globY) && isWall(tp, globX, globY+1))
         sprite = 3 + offset;

    // треугольник - вершина вверх
    if (isWall(tp, globX-1, globY) && isWall(tp, globX+1, globY) && isWall(tp, globX, globY-1))
        sprite = 7 + offset;

    // треугольник - вершина влево
    if (isWall(tp, globX, globY+1) && isWall(tp, globX, globY-1) && isWall(tp, globX-1, globY))
        sprite = 8 + offset;

    // треугольник - вершина вправо
    if (isWall(tp, globX, globY+1) && isWall(tp, globX, globY-1) && isWall(tp, globX+1, globY))
         sprite = 9 + offset;

    // перекресток стен
    if (isWall(tp, globX+1, globY) && isWall(tp, globX, globY+1)
         && isWall(tp, globX-1, globY) && isWall(tp, globX, globY-1))
         sprite = 6 + offset;

	return sprite;
}


int SpriteDefiner::getDoorSprite(int globX, int globY, int offset)
{
    if (isWall(globX-1, globY) || isWall(globX+1, globY))
        return offset;

    if (isWall(globX, globY+1) || isWall(globX, globY+1))
        return offset+1;
}

