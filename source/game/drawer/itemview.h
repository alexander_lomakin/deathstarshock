#ifndef ITEMVIEW_H
#define ITEMVIEW_H


#include "colors/colors.h"


struct ItemView
    {
        int spriteOffset;
        int id;
        COLOR color;
    };



#endif // ITEMVIEW_H
