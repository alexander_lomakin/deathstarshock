#include "dolldrawer.h"

DollDrawer::DollDrawer()
{
    //ctor
}


void DollDrawer::loadAndInit(int nLeftX, int nLeftY) {
    _leftX = nLeftX;
    _leftY = nLeftY;

    _head      = IrrlichtBase::loadTexture("data/graphic/head.png");
    _body      = IrrlichtBase::loadTexture("data/graphic/body.png");
    _leftLeg   = IrrlichtBase::loadTexture("data/graphic/left_leg.png");
    _rightLeg  = IrrlichtBase::loadTexture("data/graphic/right_leg.png");
    _leftHand  = IrrlichtBase::loadTexture("data/graphic/left_hand.png");
    _rightHand = IrrlichtBase::loadTexture("data/graphic/right_hand.png");
}


void DollDrawer::draw(video::IVideoDriver* _driver) {
    // HEAD
    _driver->draw2DImage(_head, core::position2d<s32>(_leftX, _leftY), core::rect<s32>(0, 0, 20, 22), 0, video::SColor(255,255, 255, 255), true);

    // BODY
    _driver->draw2DImage(_body, core::position2d<s32>(_leftX - 3, _leftY + 24), core::rect<s32>(0, 0, 26, 44), 0, video::SColor(255,255, 255, 255), true);

    // LEFT HAND
    _driver->draw2DImage(_leftHand, core::position2d<s32>(_leftX + 24, _leftY + 24), core::rect<s32>(0, 0, 19, 48), 0, video::SColor(255,255, 255, 255), true);

    // RIGHT HAND
    _driver->draw2DImage(_rightHand, core::position2d<s32>(_leftX - 24, _leftY + 24), core::rect<s32>(0, 0, 19, 48), 0, video::SColor(255,255, 255, 255), true);

    // LEFT LEG
    _driver->draw2DImage(_leftLeg, core::position2d<s32>(_leftX - 5, _leftY + 69), core::rect<s32>(0, 0, 14, 50), 0, video::SColor(255,255, 255, 255), true);

    // RIGHT LEG
    _driver->draw2DImage(_rightLeg, core::position2d<s32>(_leftX + 11, _leftY + 69), core::rect<s32>(0, 0, 14, 50), 0, video::SColor(255,255, 255, 255), true);
}
