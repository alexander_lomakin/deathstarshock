

#include "stddrawer.h"


using namespace irr;
using namespace io;
using namespace std;

void StdDrawer::_drawGameArea()
{
    AreaCell *cell;
    ExtendedCell *extCell;
    GraphicCell *grCell;

    int globX, globY;
    int animOffsetX, animOffsetY;
    int sx = 0;

    _setTargetPos(globX, globY);
    _setAnimOffset(animOffsetX, animOffsetY);

	for (int i = globX - _drawingRadiusX; i <= globX + _drawingRadiusX; i++)
	{
		int sy = 0;
		for (int j = globY - _drawingRadiusY; j <= globY + _drawingRadiusY; j++)
		{
			cell    = _loader->areaLoader ->getCell(i, j);
			extCell = _loader->areaLoader->getExtendedCell(i, j);
			grCell  = _loader->areaLoader->getGraphicCell(i, j);


			if (!cell->isExplored && !extCell->isVisible)
            {
                sy += _stdTailY;
                continue;
            }

            if (grCell->isFire == true) {

                _drawTail(sx+_offsetX+animOffsetX, sy+_offsetY+animOffsetY, grCell->additSpriteId);

                _drawTail(sx+_offsetX+animOffsetX, sy+_offsetY+animOffsetY, _fire.getFrameNow());

                sy += _stdTailY;
                continue;
            }

            if (grCell->isTerminal == true) {

                _drawTail(sx+_offsetX+animOffsetX, sy+_offsetY+animOffsetY, grCell->additSpriteId);

                _drawTail(sx+_offsetX+animOffsetX, sy+_offsetY+animOffsetY, _terminal.getFrameNow());

                sy += _stdTailY;
                continue;
            }


            if (grCell->additSpriteId != -1)
				_drawTail(sx+_offsetX+animOffsetX, sy+_offsetY+animOffsetY, grCell->additSpriteId);

			if (grCell->spriteId != -1)
                _drawTail(sx+_offsetX+animOffsetX, sy+_offsetY+animOffsetY, grCell->spriteId);

            if (cell->bloodLevel > -1)
                _drawBloodTail(sx+_offsetX+animOffsetX, sy+_offsetY+animOffsetY, cell->bloodLevel);

            if (grCell->itemSpriteId != -1)
				_drawItem(sx+_offsetX+animOffsetX, sy+_offsetY+animOffsetY, grCell->itemSpriteId);

			sy += _stdTailY;
		}
		sx += _stdTailX;
	}
}


void StdDrawer::_drawActiveObjects()
{
    ExtendedCell *extCell;
    GraphicCell *grCell;

    int globX, globY;
    int animOffsetX, animOffsetY;
    int sx = 0;

    _setTargetPos(globX, globY);
    _setAnimOffset(animOffsetX, animOffsetY);


	for (int i = globX - _drawingRadiusX; i <= globX + _drawingRadiusX; i++)
	{
		int sy = 0;
		for (int j = globY - _drawingRadiusY; j <= globY + _drawingRadiusY; j++)
		{
			extCell = _loader->areaLoader->getExtendedCell(i, j);
			grCell  = _loader->areaLoader->getGraphicCell(i, j);

			if (!extCell->isVisible)
            {
                sy += _stdTailY;
                continue;
            }

            for(int id = 0; id < MAX_BEASTS; id++) {
                Creature* cr = _loader->beastLoader->getBeastById(id);

                if (cr == 0) {
                    continue;
                }

                if (cr->x == i && cr->y == j) {
                    int offsetX, offsetY;
                    cr->getOffsetNow(offsetX, offsetY);
                    _drawCreature(sx+_offsetX+offsetX+animOffsetX, sy+_offsetY+offsetY+animOffsetY, cr->spriteIdNow);
                }
            }

            std::list<GunshotParticle>::iterator gp;
            for(gp = _gunshotParticles.begin(); gp != _gunshotParticles.end(); ++gp)
                if ((*gp).startX == i && (*gp).startY == j)
                {
                    int curX, curY;
                    (*gp).lnEq.setCoordByStep(curX, curY, (*gp).stepNow);
                    curX = ((*gp).startX + (curX / (32*_scaleSize)));
                    curY = ((*gp).startY + (curY / (32*_scaleSize)));

                    ExtendedCell* eCell = _loader->areaLoader->getExtendedCell(curX, curY);
                    if (eCell == 0) {
                        continue;
                    }

                    if (eCell->isVisible == false) {
                        continue;
                    }


                    if ((*gp).type == "shrapnel")
                        _drawShrapnelParticle(sx+_offsetX+animOffsetX+(_stdTailXHalf-2*_scaleSize), sy+_offsetY+animOffsetY+(_stdTailYHalf-2*_scaleSize), (*gp).stepNow, &(*gp).lnEq);

                    if ((*gp).type == "single")
                        _drawBulletParticle(sx+_offsetX+animOffsetX+(_stdTailXHalf-2*_scaleSize), sy+_offsetY+animOffsetY+(_stdTailYHalf-2*_scaleSize), (*gp).stepNow, &(*gp).lnEq);
                }


			sy += _stdTailY;
		}
		sx += _stdTailX;
	}
}


void StdDrawer::_drawFogOfWar()
{
    ExtendedCell *extCell;
    AreaCell *cell;

    int globX, globY;
    int animOffsetX, animOffsetY;
    int sx = 0;

    _setTargetPos(globX, globY);
    _setAnimOffset(animOffsetX, animOffsetY);


	for (int i = globX - _drawingRadiusX; i <= globX + _drawingRadiusX; i++)
	{
		int sy = 0;
		for (int j = globY - _drawingRadiusY; j <= globY + _drawingRadiusY; j++)
		{
			cell    = _loader->areaLoader->getCell(i, j);
			extCell = _loader->areaLoader->getExtendedCell(i, j);

            if (extCell->drawBorder == true) {
                _drawExtTail(sx+_offsetX+animOffsetX, sy+_offsetY+animOffsetY, 0);
            }

            if (i >= globX -6 && i <= globX + 7 && j >= globY -6 && j <= globY + 7 )
            {
                if (i == globX -6 && j == globY -6)
                    _losDrawer.drawLos(sx+_offsetX+animOffsetX, sy+_offsetY+animOffsetY);

                sy += _stdTailY;
                continue;
            }

			if (cell->isExplored)
                _drawMask(sx+_offsetX+animOffsetX, sy+_offsetY+animOffsetY, irr::video::SColor(extCell->darknessLv, 0, 0, 0));

			sy += _stdTailY;
		}
		sx += _stdTailX;
	}
}


void StdDrawer::_drawComZone()
{
    ExtendedCell *extCell;

	int globX, globY;
    int animOffsetX, animOffsetY;
    int sx = 0;

    _setTargetPos(globX, globY);
    _setAnimOffset(animOffsetX, animOffsetY);

	for (int i = globX - _drawingRadiusX; i <= globX + _drawingRadiusX; i++)
	{
		int sy = 0;
		for (int j = globY - _drawingRadiusY; j <= globY + _drawingRadiusY; j++)
		{
			extCell = _loader->areaLoader->getExtendedCell(i, j);

			if (extCell->combatZoneLevel >= 0)
            {
                _drawMask(sx+_offsetX+animOffsetX, sy+_offsetY+animOffsetY, irr::video::SColor(80, 0, 0, 0));
                Font::drawBig(5+sx+_offsetX+animOffsetX, 5+sy+_offsetY+animOffsetY,
                               ITS(extCell->combatZoneLevel), irr::video::SColor(255,255,255,255));
            }
			sy += _stdTailY;
		}
		sx += _stdTailX;
	}
}


void StdDrawer::_drawInterface()
{
    // draw chat
    for (int i = 0; i < _chat.size(); i++)
    {
        Font::drawSmall(5, 150+i*12,  _chat[i].msg, CTSC(_chat[i].color));
    }

    _dollDrawer.draw(_driver);

    _guiManager.draw(_driver);
}


void StdDrawer::_drawTail(int globX, int globY, int spriteId)
{
    _driver->draw2DImage(_areaTiles.getSheetTexture(), core::position2d< s32 >(globX, globY),
        _areaTiles.getSpriteById(spriteId), 0,
        video::SColor(255,255,255,255), true);
}


void StdDrawer::_drawExtTail(int globX, int globY, int spriteId)
{
    _driver->draw2DImage(_extTiles.getSheetTexture(), core::position2d< s32 >(globX, globY),
        _extTiles.getSpriteById(spriteId), 0,
        video::SColor(255,255,255,255), true);
}


void StdDrawer::_drawBloodTail(int globX, int globY, int spriteId)
{
    _driver->draw2DImage(_bloodTiles.getSheetTexture(), core::position2d< s32 >(globX, globY),
        _bloodTiles.getSpriteById(spriteId), 0,
        video::SColor(255,255,255,255), true);
}


void StdDrawer::_drawCreature(int globX, int globY, int spriteId)
{
    _driver->draw2DImage(_creatureTiles.getSheetTexture(), core::position2d< s32 >(globX, globY),
        _creatureTiles.getSpriteById(spriteId), 0,
        video::SColor(255,255,255,255), true);
}


void StdDrawer::_drawItem(int globX, int globY, int spriteId)
{
    _driver->draw2DImage(_itemTiles.getSheetTexture(), core::position2d< s32 >(globX, globY),
        _itemTiles.getSpriteById(spriteId), 0,
        video::SColor(255,255,255,255), true);
}


void StdDrawer::_drawParticle(int globX, int globY, int spriteId)
{
    _driver->draw2DImage(_gunshotTiles.getSheetTexture(), core::position2d< s32 >(globX, globY),
        _gunshotTiles.getSpriteById(spriteId), 0,
        video::SColor(255,255,255,255), true);
}


void StdDrawer::_drawMask(int globX, int globY, video::SColor color)
{
    _driver->draw2DRectangle(color, core::rect < s32 >(globX, globY, globX+_stdTailX, globY+_stdTailY));
}


void StdDrawer::_drawBulletParticle(int globX, int globY, int stepNow, LineEquation* lnEq) {
    _driver->draw2DRectangle(CTSC(20, YELLOW), core::rect < s32 >(globX-4*_scaleSize, globY-4*_scaleSize, globX+6*_scaleSize, globY+6*_scaleSize));

    int colorBright = 4;
    for (int i = stepNow - 16*_scaleSize; i < stepNow; i += 4*_scaleSize) {
        if (i < 0) {
            continue;
        }

        int curX, curY;
        lnEq->setCoordByStep(curX, curY, i);

        curX += globX;
        curY += globY;

        colorBright--;

        _driver->draw2DRectangle(CTSC(230-colorBright*70, YELLOW), core::rect < s32 >(curX, curY, curX+3*_scaleSize, curY+3*_scaleSize));
        _driver->draw2DRectangle(CTSC(230-colorBright*70, RED),    core::rect < s32 >(curX+1*_scaleSize, curY+1*_scaleSize, curX+2*_scaleSize, curY+2*_scaleSize));
    }
}



void StdDrawer::_drawShrapnelParticle(int globX, int globY, int stepNow, LineEquation* lnEq) {
    _driver->draw2DRectangle(CTSC(20, YELLOW), core::rect < s32 >(globX-1*_scaleSize, globY-1*_scaleSize, globX+1*_scaleSize, globY+1*_scaleSize));

    int colorBright = 4;
    for (int i = stepNow - 8*_scaleSize; i < stepNow; i += 2*_scaleSize) {
        if (i < 0) {
            continue;
        }

        int curX, curY;
        lnEq->setCoordByStep(curX, curY, i);

        curX += globX;
        curY += globY;

        colorBright--;

        _driver->draw2DRectangle(CTSC(230-colorBright*70, YELLOW), core::rect < s32 >(curX-1*_scaleSize, curY-1*_scaleSize, curX+1*_scaleSize, curY+1*_scaleSize));
    }
}
