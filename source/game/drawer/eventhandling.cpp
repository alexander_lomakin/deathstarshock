

#include "stddrawer.h"


using namespace irr;
using namespace io;
using namespace std;


void StdDrawer::_checkEvents() {
    while (EventStack::canPopViewEvent()) {
        ViewEvent event = EventStack::popViewEvent();

        if (event.type == AREA_LOADED) {
            _recheckAreaSprites(event);
            continue;
        }

        if (event.type == ACTIVEOBJ_CREATED) {
            _createActiveObject(event);
            continue;
        }

        if (event.type == MOVE_BEAST) {
            _moveBeast(event);
            continue;
        }

        if (event.type == CHANGE_BEAST_DIR) {
            _changeBeastDir(event);
            continue;
        }

        if (event.type == BEAST_ATTACK) {
            _beastAttack(event);
            continue;
        }

        if (event.type == CHAT_MSG) {
            _chatMsg(event);
            continue;
        }

        if (event.type == SET_TARGET_BEAST) {
             _setTargetBeast(event);
             continue;
        }

        if (event.type == RECALC_LOS) {
             _recalcLos(event);
             continue;
        }

        if (event.type == BEAST_GET_EXP) {
            _beastGetExp(event);
            continue;
        }

        if (event.type == BEAST_INJURED) {
            _beastInjured(event);
            continue;
        }

        if (event.type == PLAYER_STATUS) {
            _playerStatus(event);
            continue;
        }

        if (event.type == PLAYER_TIREDNESS) {
            _playerTiredness(event);
            continue;
        }

        if (event.type == ITEM_CREATED) {
            _itemCreated(event);
            continue;
        }

        if (event.type == ITEM_DELETED) {
            _itemDeleted(event);
            continue;
        }

        if (event.type == DRAW_ITEM_LIST) {
            _drawItemList(event);
            continue;
        }

        if (event.type == ADD_ITEM_ELEM) {
            _addItemElem(event);
            continue;
        }

        if (event.type == SELECT_NEW_ELEM) {
            _selectNewItem(event);
            continue;
        }

        if (event.type == EQUIP_ARMOR) {
            _equipArmor(event);
            continue;
        }

        if (event.type == EQUIP_WEAPON) {
            _equipWeapon(event);
            continue;
        }

        if (event.type == SINGLE_SHOOT) {
            _singleShoot(event);
            continue;
        }

        if (event.type == SINGLE_SHOOT_MISS) {
            _singleShootMiss(event);
            continue;
        }

        if (event.type == SHRAPNEL_SHOOT) {
            _shrapnelShoot(event);
            continue;
        }

        if (event.type == UPDATE_CELL_SPRITE) {
            _updateCellSprite(event);
            continue;
        }

        if (event.type == PLAY_SOUND) {
            _playSound(event);
            continue;
        }

        if (event.type == BEAST_DEAD) {
            _deadBeast(event);
            continue;
        }

        if (event.type == START_HACK) {
            _startHack(event);
            continue;
        }

        if (event.type == DRAW_CHARLIST) {
            _drawCharList(event);
            continue;
        }
    }
}


void StdDrawer::_recheckAreaSprites(ViewEvent event) {
    SpriteDefiner definer(_loader->areaLoader);
    int globX, globY;

    for (int i = 0; i < AREA_WIDTH; i++) {
		for (int j = 0; j < AREA_HEIGHT; j++) {
			GraphicCell* cell         = _loader->areaLoader->getGraphicCell(i, j, (Direction)event.argA);
			AreaCell* areacell        = _loader->areaLoader->getCell(i, j, (Direction)event.argA);
            MasterAreaCell masterCell = _loader->areaLoader->getMasterCell(areacell->type);
            MasterAreaCell addMsCell  = _loader->areaLoader->getMasterCell(areacell->additCell);

            setGlobalCoordByDir(i, j , (Direction)event.argA, globX, globY);

			if (masterCell.addit == "wall") {
                cell->spriteId = definer.getWallSprite(globX, globY, _masterCellsView[areacell->type].spriteOffset);
			} else {
                if (masterCell.addit == "door") {
                    cell->spriteId = definer.getDoorSprite(globX, globY, _masterCellsView[areacell->type].spriteOffset);
                } else {
                    cell->spriteId = _masterCellsView[areacell->type].spriteOffset;
                }
            }

            if (_masterCellsView[areacell->additCell].isWall) {
                cell->additSpriteId = definer.getWallSprite(globX, globY, _masterCellsView[areacell->additCell].spriteOffset);
            } else {
                if (addMsCell.addit == "door") {
                    cell->additSpriteId = definer.getDoorSprite(globX, globY, _masterCellsView[areacell->additCell].spriteOffset);
                } else {
                    cell->additSpriteId = _masterCellsView[areacell->additCell].spriteOffset;
                }
            }

            if (_masterCellsView[areacell->type].isFire) {
                cell->isFire = true;
            } else {
                cell->isFire = false;
            }

            if (_masterCellsView[areacell->type].isTerminal) {
                cell->isTerminal = true;
            } else {
                cell->isTerminal = false;
            }
		}
    }
}


void StdDrawer::_createActiveObject(ViewEvent event) {
    Creature* creature = _loader->beastLoader->getBeastById(event.argA);

    creature->spriteIdNow  = creature->spriteOffset;

    Animation* anim;

    anim = creature->getAnimationById(CA_WALK_BOTTOM);
    anim->setAnimationParameters(6, 2, creature->spriteOffset, 3, true);

    anim = creature->getAnimationById(CA_WALK_TOP);
    anim->setAnimationParameters(6, 2, creature->spriteOffset+2, 3, true);

    anim = creature->getAnimationById(CA_WALK_LEFT);
    anim->setAnimationParameters(6, 2, creature->spriteOffset+4, 3, true);

    anim = creature->getAnimationById(CA_WALK_RIGHT);
    anim->setAnimationParameters(6, 2, creature->spriteOffset+6, 3, true);

    anim = creature->getAnimationById(CA_ATTACK_BOTTOM);
    anim->setAnimationParameters(7, 2, creature->spriteOffset+8, 3, false);

    anim = creature->getAnimationById(CA_ATTACK_TOP);
    anim->setAnimationParameters(7, 2, creature->spriteOffset+10, 3, false);

    anim = creature->getAnimationById(CA_ATTACK_LEFT);
    anim->setAnimationParameters(7, 2, creature->spriteOffset+12, 3, false);

    anim = creature->getAnimationById(CA_ATTACK_RIGHT);
    anim->setAnimationParameters(7, 2, creature->spriteOffset+14, 3, false);

    anim = creature->getAnimationById(CA_DEAD);
    anim->setAnimationParameters(6, 2, creature->spriteOffset+16, 3, true);
}


void StdDrawer::_moveBeast(ViewEvent event) {
    Creature* creature = _loader->beastLoader->getBeastById(event.argA);

    switch (creature->viewDirection) {
        case BOTTOM: {
            creature->startAnimation(CA_WALK_BOTTOM);
            creature->startMovement(0, 0, 0, (-1*_scaleSize*32), (4*_scaleSize), BOTTOM);
        } break;
        case TOP: {
            creature->startAnimation(CA_WALK_TOP);
            creature->startMovement(0, 0, 0, (_scaleSize*32), (4*_scaleSize), TOP);
        } break;
        case LEFT_BOTTOM: {
            creature->startAnimation(CA_WALK_LEFT);
            creature->startMovement(0, 0, (_scaleSize*32), (-1*_scaleSize*32), (4*_scaleSize), LEFT_BOTTOM);
        } break;
        case LEFT_TOP: {
            creature->startAnimation(CA_WALK_LEFT);
            creature->startMovement(0, 0, (_scaleSize*32), (_scaleSize*32), (4*_scaleSize), LEFT_TOP);
        } break;
        case LEFT: {
            creature->startAnimation(CA_WALK_LEFT);
            creature->startMovement(0, 0, (_scaleSize*32), 0, (4*_scaleSize), LEFT);
        } break;
        case RIGHT_BOTTOM: {
            creature->startAnimation(CA_WALK_RIGHT);
            creature->startMovement(0, 0, (-1*_scaleSize*32), (-1*_scaleSize*32), (4*_scaleSize), RIGHT_BOTTOM);
        } break;
        case RIGHT_TOP: {
            creature->startAnimation(CA_WALK_RIGHT);
            creature->startMovement(0, 0, (-1*_scaleSize*32), (_scaleSize*32), (4*_scaleSize), RIGHT_TOP);
        } break;
        case RIGHT: {
            creature->startAnimation(CA_WALK_RIGHT);
            creature->startMovement(0, 0, (-1*_scaleSize*32), 0, (4*_scaleSize), RIGHT);
        } break;
    }
}


void StdDrawer::_changeBeastDir(ViewEvent event) {
    Creature* creature = _loader->beastLoader->getBeastById(event.argA);

    if (creature == 0) {
        return;
    }

    switch (creature->viewDirection) {
        case BOTTOM: {
            creature->spriteIdNow = creature->spriteOffset;
        } break;
        case TOP: {
            creature->spriteIdNow = creature->spriteOffset + 2;
        } break;
        case LEFT_BOTTOM:
        case LEFT_TOP:
        case LEFT: {
            creature->spriteIdNow = creature->spriteOffset + 4;
        } break;
        case RIGHT_BOTTOM:
        case RIGHT_TOP:
        case RIGHT: {
            creature->spriteIdNow = creature->spriteOffset + 6;
        } break;
    }
}


void StdDrawer::_beastAttack(ViewEvent event) {
    Creature* creature = _loader->beastLoader->getBeastById(event.argA);

    switch (creature->viewDirection) {
        case BOTTOM: {
            creature->startAnimation(CA_ATTACK_BOTTOM);
        } break;
        case TOP: {
            creature->startAnimation(CA_ATTACK_TOP);
        } break;
        case LEFT_BOTTOM:
        case LEFT_TOP:
        case LEFT: {
            creature->startAnimation(CA_ATTACK_LEFT);
        } break;
        case RIGHT_BOTTOM:
        case RIGHT_TOP:
        case RIGHT: {
            creature->startAnimation(CA_ATTACK_RIGHT);
        } break;
    }
}


void StdDrawer::_chatMsg(ViewEvent event) {
    if (_chat.size() > 20) {
        _chat.erase(_chat.begin(), _chat.begin()+1);
    }

    ChatMsg msg;
    msg.msg   = event.argS;
    msg.color = (COLOR)event.argA;
    _chat.push_back(msg);
}


void StdDrawer::_setTargetBeast(ViewEvent event) {
    _targetObjectId = event.argA;
}


void StdDrawer::_recalcLos(ViewEvent event) {
    LosCalc los(_loader->areaLoader);
    los.darkLevel();
    los.calcLos(event.argA, event.argB, 6);

    _losDrawer.initMatrix(event.argA, event.argB, 6, 6);
}


void StdDrawer::_beastInjured(ViewEvent event) {
    if (event.argA  != _targetObjectId)
        return;

    _interfaceVars.hitPointsBar->setProgressMax(100);
    _interfaceVars.hitPointsBar->setText(ITS(event.argC)+"/"+ITS(event.argB));

    if (event.argB == event.argC) {
        _interfaceVars.hitPointsBar->setProgressNow(100);
    } else {
        if (event.argC == 0) {
            _interfaceVars.hitPointsBar->setProgressNow(0);
        } else {
            if (event.argB / 100 != 0) {
                _interfaceVars.hitPointsBar->setProgressNow(event.argC/(event.argB / 100));
            } else {
                _interfaceVars.hitPointsBar->setProgressNow(0);
            }
        }
    }
}


void StdDrawer::_beastGetExp(ViewEvent event) {
     if (event.argA  != _targetObjectId)
        return;

    _interfaceVars.experienceBar->setProgressNow(event.argB/3);
}


void StdDrawer::_playerTiredness(ViewEvent event) {
    _interfaceVars.tirednessBar->setProgressMax(100);
    _interfaceVars.tirednessBar->setText(ITS(event.argB)+"/"+ITS(event.argA));

    if (event.argA == event.argB) {
        _interfaceVars.tirednessBar->setProgressNow(100);
    } else {
        if (event.argB == 0) {
            _interfaceVars.tirednessBar->setProgressNow(0);
        } else {
            if (event.argA / 100 != 0) {
                _interfaceVars.tirednessBar->setProgressNow(event.argB/(event.argA / 100));
            } else {
                _interfaceVars.tirednessBar->setProgressNow(0);
            }
        }
    }
}


void StdDrawer::_playerStatus(ViewEvent event) {
    _interfaceVars.guiStatus->setText(event.argS, GRAY);
}


void StdDrawer::_itemCreated(ViewEvent event) {
    GraphicCell* grCell = _loader->areaLoader->getGraphicCell(event.argB, event.argC, (Direction)event.argD);

    for (int i = 0; i < _itemView.size(); i++) {
        if (_itemView[i].id == event.argA) {
            if (grCell->itemSpriteId == -1) {
                grCell->itemSpriteId = _itemView[i].spriteOffset;
            } else {
                grCell->itemSpriteId = 0;
            }

            break;
        }
    }
}


void StdDrawer::_itemDeleted(ViewEvent event) {
    GraphicCell* grCell  = _loader->areaLoader->getGraphicCell(event.argA, event.argB, (Direction)event.argC);
    grCell->itemSpriteId = -1;
}


void StdDrawer::_drawItemList(ViewEvent event)
{
    _interfaceVars.guiList->drawElem((bool)event.argA);
    _interfaceVars.guiList->clearList();
    _interfaceVars.guiList->setSelectedItem(0);
    _interfaceVars.guiList->setTitle(event.argS, YELLOW);
}


void StdDrawer::_addItemElem(ViewEvent event)
{
    COLOR color = WHITE;

    for (int i = 0; i < _itemView.size(); i++)
    {
        if (_itemView[i].id == event.argA)
        {
            color = _itemView[i].color;
            break;
        }
    }

    _interfaceVars.guiList->addItem(event.argS, color);
}


void StdDrawer::_selectNewItem(ViewEvent event)
{
    _interfaceVars.guiList->setSelectedItem(event.argA);
}


void StdDrawer::_equipWeapon(ViewEvent event)
{
    if (event.argA == -1)
    {
        _interfaceVars.guiWeaponInfo->setText(event.argS, GRAY);
        return;
    }

    for (int i = 0; i < _itemView.size(); i++)
    {
        if (_itemView[i].id == event.argA)
        {
            _interfaceVars.guiWeaponInfo->setText(event.argS, _itemView[i].color);
            return;
        }
    }
}


void StdDrawer::_equipArmor(ViewEvent event)
{
    _interfaceVars.armorStatusBar->setProgressMax(100);
    _interfaceVars.armorStatusBar->setText(ITS(event.argB)+"/"+ITS(event.argA));

    if (event.argA == event.argB) {
        _interfaceVars.armorStatusBar->setProgressNow(100);
    } else {
        _interfaceVars.armorStatusBar->setProgressNow(event.argB*(100 / event.argA));
    }

    if (event.argA == 0)
        _interfaceVars.armorStatusBar->drawElem(false);
    else
        _interfaceVars.armorStatusBar->drawElem(true);
}


void StdDrawer::_singleShoot(ViewEvent event) {
    /*
    GunshotParticle part();

    Creature* creature = _loader->beastLoader->getBeastById(event.argA);

    part.startX      = creature->x;
    part.startY      = creature->y;
    part.endX        = event.argB;
    part.endY        = event.argC;
    part.type        = "single";
    part.deadBeastId = event.argD;

    int offsetX = -1*(part.startX - event.argB);
    int offsetY = -1*(part.startY - event.argC);

    part.lnEq.init(0, 0, offsetX*(_scaleSize*32), offsetY*(_scaleSize*32));
    part.stepNow = 0;

    _gunshotParticles.push_back(part);
    */
}


void StdDrawer::_singleShootMiss(ViewEvent event) {
    /*
    GunshotParticle part;

    Creature* creature = _loader->beastLoader->getBeastById(event.argA);

    part.startX      = creature->x;
    part.startY      = creature->y;
    part.vdDirection = creature->viewDirection;
    part.type        = "single";

    int offsetX = abs(part.startX - event.argB);
    int offsetY = abs(part.startY - event.argC);


    switch (part.vdDirection) {
        case BOTTOM: {
            part.startAMovement(rand()%8 - 4, 0, 32*offsetX + rand()%32, 32 * offsetY+32, 29);
        } break;
        case TOP: {
            part.startAMovement(rand()%8 - 4, 0, 32*offsetX + rand()%32, (32 * offsetY+32) *-1, 29);
        } break;
        case LEFT: {
            part.startAMovement(0, rand()%8 - 4, (32*offsetX+32) * -1, 32 * offsetY  + rand()%32, 29);
        } break;
        case RIGHT: {
            part.startAMovement(0, rand()%8 - 4, 32*offsetX+32, 32 * offsetY  + rand()%32, 29);
        } break;
    }

    _gunshotParticles.push_back(part);
    */
}


void StdDrawer::_shrapnelShoot(ViewEvent event) {
    /*
    GunshotParticle part;

    Creature* creature = _loader->beastLoader->getBeastById(event.argA);

    part.startX      = creature->x;
    part.startY      = creature->y;
    part.endX        = event.argB;
    part.endY        = event.argC;
    part.type        = "shrapnel";
    part.deadBeastId = event.argD;

    int offsetX = -1*(part.startX - event.argB);
    int offsetY = -1*(part.startY - event.argC);

    part.lnEq.init(0, 0, offsetX*(_scaleSize*32), offsetY*(_scaleSize*32));
    part.stepNow = 0;

    _gunshotParticles.push_back(part);
    */
}


void StdDrawer::_updateCellSprite(ViewEvent event) {
    SpriteDefiner definer(_loader->areaLoader);

    GraphicCell* cell         = _loader->areaLoader->getGraphicCell(event.argA, event.argB);
    AreaCell* areacell        = _loader->areaLoader->getCell(event.argA, event.argB);
    MasterAreaCell masterCell = _loader->areaLoader->getMasterCell(areacell->type);
    MasterAreaCell addMsCell  = _loader->areaLoader->getMasterCell(areacell->additCell);

    if (masterCell.addit == "wall") {
        cell->spriteId = definer.getWallSprite(event.argA, event.argB, _masterCellsView[areacell->type].spriteOffset);
    } else {
        if (masterCell.addit == "door") {
            cell->spriteId = definer.getDoorSprite(event.argA, event.argB, _masterCellsView[areacell->type].spriteOffset);
        } else {
            cell->spriteId = _masterCellsView[areacell->type].spriteOffset;
        }
    }

    if (addMsCell.addit == "wall") {
        cell->additSpriteId = definer.getWallSprite(event.argA, event.argB, _masterCellsView[areacell->additCell].spriteOffset);
    } else {
        if (addMsCell.addit == "door") {
            cell->additSpriteId = definer.getDoorSprite(event.argA, event.argB, _masterCellsView[areacell->additCell].spriteOffset);
        } else {
            cell->additSpriteId = _masterCellsView[areacell->additCell].spriteOffset;
        }
    }

    if (_masterCellsView[areacell->type].isFire) {
        cell->isFire = true;
    } else {
        cell->isFire = false;
    }

    if (_masterCellsView[areacell->type].isTerminal) {
        cell->isTerminal = true;
    } else {
        cell->isTerminal = false;
    }
}


void StdDrawer::_playSound(ViewEvent event) {
    if (event.argS == "opendoor") {
        IrrlichtBase::sound("data\\audio\\sounds\\opendoor.wav");
        return;
    }

    if (event.argS == "reload") {
        IrrlichtBase::sound("data\\audio\\sounds\\reload.wav");
        return;
    }

    if (event.argS == "swapweapon") {
        IrrlichtBase::sound("data\\audio\\sounds\\swapweapon.wav");
        return;
    }

    if (event.argS == "gunshot") {
        IrrlichtBase::sound("data\\audio\\sounds\\gunshot.wav");
        return;
    }

    if (event.argS == "machinegun") {
        IrrlichtBase::sound("data\\audio\\sounds\\machinegun.wav");
        return;
    }

    if (event.argS == "shotgunshot") {
        IrrlichtBase::sound("data\\audio\\sounds\\shotgunshot.wav");
        return;
    }
}


void StdDrawer::_deadBeast(ViewEvent event) {
    Creature* creature = _loader->beastLoader->getBeastById(event.argA);

    creature->startAnimation(CA_DEAD);
}


void StdDrawer::_startHack(ViewEvent event) {
    int width  = IrrlichtBase::getWindowWidth();
    int height = IrrlichtBase::getWindowHeight();

    _interfaceVars.hackBar->drawElem(true);
    _interfaceVars.hackBar->setPosition(width/2 - 50, height - height/4);
    _interfaceVars.hackBar->setColors(BLUE, DARK_BLUE);
    _interfaceVars.hackBar->setText("hacking");
    _interfaceVars.hackBar->setRising(true, 2);
    _interfaceVars.hackBar->setProgressNow(0);
    _interfaceVars.hackBar->setProgressMax(100);

    _isHacking = true;

}


void StdDrawer::_drawCharList(ViewEvent event) {
    Creature* creature = _loader->beastLoader->getBeastById(event.argA);

    if (creature == 0) {
        return;
    }

    _interfaceVars.guiArgList->drawElem((bool)event.argB);
    _interfaceVars.guiArgList->clearList();
    _interfaceVars.guiArgList->setSelectedItem(-1);
    _interfaceVars.guiArgList->setTitle("Character List", YELLOW);


    _interfaceVars.guiArgList->addItem(creature->smallArms.getName(), PINK, creature->smallArms.getLevel());
    _interfaceVars.guiArgList->addItem(creature->throwingWeapons.getName(), PINK, creature->throwingWeapons.getLevel());
    _interfaceVars.guiArgList->addItem(creature->melee.getName(), PINK, creature->melee.getLevel());
    _interfaceVars.guiArgList->addItem(creature->sapper.getName(), PINK, creature->sapper.getLevel());
    _interfaceVars.guiArgList->addItem(creature->intelligence.getName(), PINK, creature->intelligence.getLevel());
    _interfaceVars.guiArgList->addItem(creature->stealth.getName(), PINK, creature->stealth.getLevel());
    _interfaceVars.guiArgList->addItem(creature->computers.getName(), PINK, creature->computers.getLevel());
    _interfaceVars.guiArgList->addItem(creature->technics.getName(), PINK, creature->technics.getLevel());
    _interfaceVars.guiArgList->addItem(creature->science.getName(), PINK, creature->science.getLevel());
    _interfaceVars.guiArgList->addItem(creature->survival.getName(), PINK, creature->survival.getLevel());
    _interfaceVars.guiArgList->addItem(creature->willPower.getName(), PINK, creature->willPower.getLevel());
    _interfaceVars.guiArgList->addItem(creature->medicine.getName(), PINK, creature->medicine.getLevel());
    _interfaceVars.guiArgList->addItem(creature->lucky.getName(), PINK, creature->lucky.getLevel());
}
