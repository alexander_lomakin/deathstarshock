#ifndef GUNSHOTPARTICLE_H
#define GUNSHOTPARTICLE_H


#include <string>

#include "objects/movement.h"

#include "math/coord.h"
#include "math/lineequation.h"


using namespace std;


    class GunshotParticle : public Movement
    {
    public:
        std::string type;
        int startX;
        int startY;
        int endX;
        int endY;
        int stepNow;
        int deadBeastId;
        LineEquation lnEq;
    };


#endif // GUNSHOTPARTICLE_H
