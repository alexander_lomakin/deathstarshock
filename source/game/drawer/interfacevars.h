#ifndef INTERFACEVARS_H
#define INTERFACEVARS_H


#include <string>
#include <vector>

#include "chatmsg.h"
#include "guisystem/guilist.h"
#include "guisystem/guiarglist.h"
#include "guisystem/guistatictext.h"
#include "guisystem/guiprogressbar.h"


using namespace std;

    struct InterfaceVars
    {
        InterfaceVars()
        {
            armorStatusBar = new GuiProgressBar;
            hitPointsBar   = new GuiProgressBar;
            tirednessBar   = new GuiProgressBar;
            experienceBar  = new GuiProgressBar;
            hackBar      = new GuiProgressBar;

            guiStatus     = new GuiStaticText;
            guiWeaponInfo = new GuiStaticText;

            guiList = new GuiList;
            guiArgList = new GuiArgList;

            armorStatusBar->drawElem(false);
            guiList->drawElem(false);
            guiArgList->drawElem(false);
            hackBar->drawElem(false);

        }


        GuiList*       guiList;
        GuiArgList*    guiArgList;
        GuiStaticText* guiStatus;
        GuiStaticText* guiWeaponInfo;

        GuiProgressBar* armorStatusBar;
        GuiProgressBar* hitPointsBar;
        GuiProgressBar* tirednessBar;
        GuiProgressBar* experienceBar;
        GuiProgressBar* hackBar;
    };



#endif // INTERFACEVARS_H
