#ifndef DOLLDRAWER_H
#define DOLLDRAWER_H


#include "irrlichtbase.h"


class DollDrawer {
    public:
        DollDrawer();

        void loadAndInit(int nLeftX, int nLeftY);

        void draw(video::IVideoDriver* _driver);
    private:
        int _leftX, _leftY;

        video::ITexture* _head;
        video::ITexture* _body;
        video::ITexture* _leftLeg;
        video::ITexture* _rightLeg;
        video::ITexture* _leftHand;
        video::ITexture* _rightHand;
};


#endif // DOLLDRAWER_H
