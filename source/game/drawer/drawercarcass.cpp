

#include "drawercarcass.h"


using namespace irr;
using namespace std;
using namespace io;


DrawerCarcass::DrawerCarcass(Loaders* loader)
{
    _loader          = loader;
    _driver          = IrrlichtBase::getDriver();
    _drawCombatZone  = false;

    // LOADING MASTER VIEW
    _loadMasterCellsFromFile(OptionLoader::getStrKey("Drawer", "masterCellsFilePath"));
    _loadMasterItemsFromFile(OptionLoader::getStrKey("Drawer", "masterItemsFilePath"));
}



void DrawerCarcass::step() {
    _loader->semaphore.enter();

    _checkEvents();

    _driver->beginScene(true, true, video::SColor(0, 0, 0, 0));

    _drawGameArea();

    _drawActiveObjects();

    _drawFogOfWar();

    if (_drawCombatZone)
        _drawComZone();

    _drawInterface();

    _driver->endScene();

    _calcActiveObjects();

    _loader->semaphore.leave();
}


void DrawerCarcass::setCombatZoneDrawing(bool draw)
{
    _drawCombatZone = draw;
}


void DrawerCarcass::_loadMasterCellsFromFile(string filePath)
{
    IrrXMLReader* xml = createIrrXMLReader(filePath.c_str());

    while(xml && xml->read())
    {
        if (xml->getNodeType() == EXN_ELEMENT)
        {
            if (!strcmp("mastercell", xml->getNodeName()))
            {
                MasterCellView cellView;

                cellView.spriteOffset = STI(xml->getAttributeValue("spriteOffset"));
                cellView.isWall       = (bool)STI(xml->getAttributeValue("isWall"));
                cellView.isFire       = (bool)STI(xml->getAttributeValue("isFire"));
                cellView.isTerminal   = (bool)STI(xml->getAttributeValue("isTerminal"));
                cellView.subCellId    = STI(xml->getAttributeValue("subCell"));

                _masterCellsView.push_back(cellView);
             }
        }
   }

   delete xml;
}


void DrawerCarcass::_loadMasterItemsFromFile(string filePath)
{
    IrrXMLReader* xml = createIrrXMLReader(filePath.c_str());

    int id = 0;

    while(xml && xml->read())
    {
        if (xml->getNodeType() == EXN_ELEMENT)
        {
            if (!strcmp("itemview", xml->getNodeName()))
            {
                ItemView itemView;

                itemView.spriteOffset = STI(xml->getAttributeValue("spriteOffset"));
                itemView.id = id;
                itemView.color = STC(xml->getAttributeValue("color"));

                id++;

                _itemView.push_back(itemView);
             }
        }
   }

   delete xml;
}



