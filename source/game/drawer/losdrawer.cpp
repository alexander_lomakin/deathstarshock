

#include "losdrawer.h"


LosDrawer::LosDrawer()
{
    for (int j = 0; j < 208; j++)
        for (int i = 0; i < 208; i++)
        {
            _matrixA[i][j].alpha = 0;
            _matrixB[i][j].alpha = 0;

        }

        _additImg = 0;
}


void LosDrawer::init(AreaLoader* _loader, video::IVideoDriver* driver, int scaleSize) {
    _scaleSize = scaleSize;

    _areaLoader = _loader;
    _driver     = driver;
    _darkedZone = 0;

    _additImg = _driver->createImage(irr::video::ECF_A8R8G8B8, core::position2d< u32 >(416*_scaleSize, 416*_scaleSize));
}


void LosDrawer::initMatrix(int stGlobX, int stGlobY, int radiusX, int radiusY) {
    for (int j = 0; j < 208; j++)
        for (int i = 0; i < 208; i++)
        {
            _matrixA[i][j].alpha = 0;
            _matrixB[i][j].alpha = 0;

        }

    int matrixX = 0;

    for (int i = stGlobX - radiusX; i <= stGlobX + radiusX; i++)
    {
        int matrixY = 0;
        for (int j = stGlobY - radiusY; j <= stGlobY + radiusY; j++)
        {
            AreaCell* areaCell        = _areaLoader->getCell(i, j);
            ExtendedCell* cell        = _areaLoader->getExtendedCell(i, j);
            MasterAreaCell masterCell = _areaLoader->getMasterCell(areaCell->type);

            if ((masterCell.lightType > -1 || areaCell->lightType > -1) && cell->isVisible)
            {
                int type;
                if (areaCell->lightType != -1)
                    type = areaCell->lightType;

                if (masterCell.lightType != -1)
                    type = masterCell.lightType;

                Light light = _areaLoader->getMasterLightById(masterCell.lightType);

                _matrixA[matrixX][matrixY].alpha = light.power;
                _matrixA[matrixX][matrixY].red   = light.red;
                _matrixA[matrixX][matrixY].green = light.green;
                _matrixA[matrixX][matrixY].blue  = light.blue;

                matrixY++;
                continue;
            }

            _matrixA[matrixX][matrixY].red   = 0;
            _matrixA[matrixX][matrixY].green = 0;
            _matrixA[matrixX][matrixY].blue  = 0;

            _matrixA[matrixX][matrixY].alpha = cell->darknessLv;

            matrixY++;
        }
        matrixX++;
    }

    scaleMatrix(13, 13, 26, 26);
    scaleMatrix(26, 26, 52, 52);
    scaleMatrix(52, 52, 104, 104);
    scaleMatrix(104, 104, 208, 208);
    _render();
}


void LosDrawer::pushCell(int globX, int globY, int alpha, int red, int green, int blue)
{
    _matrixA[globX][globY].alpha = alpha;
    _matrixA[globX][globY].red   = red;
    _matrixA[globX][globY].green = green;
    _matrixA[globX][globY].blue  = blue;
}




void LosDrawer::scaleMatrix(int aX, int aY, int bX, int bY)
{
    // work
    for (int i = 0; i < aX; i++)
        for (int j = 0; j < aY; j++)
        {
           _matrixB[i*2][j*2].alpha = _matrixA[i][j].alpha;
           _matrixB[i*2][j*2].red   = _matrixA[i][j].red;
           _matrixB[i*2][j*2].green = _matrixA[i][j].green;
           _matrixB[i*2][j*2].blue  = _matrixA[i][j].blue;
        }

    // ��������� ������
    for (int i = 0; i < aX; i++)
        for (int j = 0; j < aY; j++)
        {


            if (i+1 == aX && j+1 == aY)
            {
                _matrixB[i*2+1][j*2+1].alpha = _matrixB[i*2][j*2].alpha;
                _matrixB[i*2+1][j*2+1].red   = _matrixB[i*2][j*2].red;
                _matrixB[i*2+1][j*2+1].green = _matrixB[i*2][j*2].green;
                _matrixB[i*2+1][j*2+1].blue  = _matrixB[i*2][j*2].blue;
                continue;
            }



            if (i+1 == aX)
            {
                _matrixB[i*2+1][j*2+1].alpha = (_matrixB[i*2][j*2].alpha + _matrixB[i*2][j*2+2].alpha) / 2;
                _matrixB[i*2+1][j*2+1].red   = (_matrixB[i*2][j*2].red + _matrixB[i*2][j*2+2].red) / 2;
                _matrixB[i*2+1][j*2+1].green = (_matrixB[i*2][j*2].green + _matrixB[i*2][j*2+2].green) / 2;
                _matrixB[i*2+1][j*2+1].blue  = (_matrixB[i*2][j*2].blue + _matrixB[i*2][j*2+2].blue) / 2;
                continue;
            }

            if (j+1 == aY)
            {
                _matrixB[i*2+1][j*2+1].alpha = (_matrixB[i*2][j*2].alpha + _matrixB[i*2+2][j*2].alpha) / 2;
                _matrixB[i*2+1][j*2+1].red   = (_matrixB[i*2][j*2].red + _matrixB[i*2+2][j*2].red) / 2;
                _matrixB[i*2+1][j*2+1].green = (_matrixB[i*2][j*2].green + _matrixB[i*2+2][j*2].green) / 2;
                _matrixB[i*2+1][j*2+1].blue  = (_matrixB[i*2][j*2].blue + _matrixB[i*2+2][j*2].blue) / 2;
                continue;
            }

            _matrixB[i*2+1][j*2+1].alpha = (_matrixB[i*2][j*2].alpha + _matrixB[i*2+2][j*2].alpha
                                      + _matrixB[i*2+2][j*2+2].alpha + _matrixB[i*2][j*2+2].alpha) / 4;
            _matrixB[i*2+1][j*2+1].red   = (_matrixB[i*2][j*2].red + _matrixB[i*2+2][j*2].red
                                    + _matrixB[i*2+2][j*2+2].red + _matrixB[i*2][j*2+2].red) / 4;
            _matrixB[i*2+1][j*2+1].green = (_matrixB[i*2][j*2].green + _matrixB[i*2+2][j*2].green
                                    + _matrixB[i*2+2][j*2+2].green + _matrixB[i*2][j*2+2].green) / 4;
            _matrixB[i*2+1][j*2+1].blue  = (_matrixB[i*2][j*2].blue + _matrixB[i*2+2][j*2].blue
                                      + _matrixB[i*2+2][j*2+2].blue + _matrixB[i*2][j*2+2].blue) / 4;
        }



    for (int i = 0; i < aX; i++)
        for (int j = 0; j < aY; j++)
        {
             _getSumFromMatrixB(i*2, j*2+1, bX, bY);
             _getSumFromMatrixB(i*2+1, j*2, bX, bY);
        }


    for (int i = 0; i < bX; i++)
        for (int j = 0; j < bY; j++)
        {
            _matrixA[i][j].alpha = _matrixB[i][j].alpha;
            _matrixA[i][j].red   = _matrixB[i][j].red;
            _matrixA[i][j].green = _matrixB[i][j].green;
            _matrixA[i][j].blue  = _matrixB[i][j].blue;
        }

}


void LosDrawer::_getSumFromMatrixB(short x, short y, short bX, short bY)
{
    if (x == 0 && y == 0)
    {
        _matrixB[x][y].alpha = (_matrixB[1][0].alpha + _matrixB[0][1].alpha) / 2;
        _matrixB[x][y].red   = (_matrixB[1][0].red   + _matrixB[0][1].red)   / 2;
        _matrixB[x][y].green = (_matrixB[1][0].green + _matrixB[0][1].green) / 2;
        _matrixB[x][y].blue  = (_matrixB[1][0].blue  + _matrixB[0][1].blue)  / 2;
        return;
    }

    if (x == bX-1 && y == bY-1)
    {
        _matrixB[x][y].alpha = (_matrixB[bX-1][bY-2].alpha + _matrixB[bX-2][bY-1].alpha) / 2;
        _matrixB[x][y].red   = (_matrixB[bX-1][bY-2].red   + _matrixB[bX-2][bY-1].red)   / 2;
        _matrixB[x][y].green = (_matrixB[bX-1][bY-2].green + _matrixB[bX-2][bY-1].green) / 2;
        _matrixB[x][y].blue  = (_matrixB[bX-1][bY-2].blue  + _matrixB[bX-2][bY-1].blue)  / 2;
        return;
    }

    if (x == bX-1 && y == 0)
    {
        _matrixB[x][y].alpha = (_matrixB[bX-2][0].alpha + _matrixB[bX-1][1].alpha) / 2;
        _matrixB[x][y].red   = (_matrixB[bX-2][0].red   + _matrixB[bX-1][1].red)   / 2;
        _matrixB[x][y].green = (_matrixB[bX-2][0].green + _matrixB[bX-1][1].green) / 2;
        _matrixB[x][y].blue  = (_matrixB[bX-2][0].blue  + _matrixB[bX-1][1].blue)  / 2;
        return;
    }

    if (y == bY-1 && x == 0)
    {
        _matrixB[x][y].alpha = (_matrixB[0][bY-2].alpha + _matrixB[1][bY-1].alpha) / 2;
        _matrixB[x][y].red   = (_matrixB[0][bY-2].red   + _matrixB[1][bY-1].red)   / 2;
        _matrixB[x][y].green = (_matrixB[0][bY-2].green + _matrixB[1][bY-1].green) / 2;
        _matrixB[x][y].blue  = (_matrixB[0][bY-2].blue  + _matrixB[1][bY-1].blue)  / 2;
        return;
    }

    if (x == 0)
    {
        _matrixB[x][y].alpha = (_matrixB[0][y-1].alpha + _matrixB[0][y+1].alpha + _matrixB[1][y].alpha) / 3;
        _matrixB[x][y].red   = (_matrixB[0][y-1].red   + _matrixB[0][y+1].red   + _matrixB[1][y].red)   / 3;
        _matrixB[x][y].green = (_matrixB[0][y-1].green + _matrixB[0][y+1].green + _matrixB[1][y].green) / 3;
        _matrixB[x][y].blue  = (_matrixB[0][y-1].blue  + _matrixB[0][y+1].blue  + _matrixB[1][y].blue)  / 3;
        return;
    }

    if (x == bX-1)
    {
        _matrixB[x][y].alpha = (_matrixB[x][y-1].alpha + _matrixB[x][y+1].alpha + _matrixB[x-1][y].alpha) / 3;
        _matrixB[x][y].red   = (_matrixB[x][y-1].red   + _matrixB[x][y+1].red   + _matrixB[x-1][y].red)   / 3;
        _matrixB[x][y].green = (_matrixB[x][y-1].green + _matrixB[x][y+1].green + _matrixB[x-1][y].green) / 3;
        _matrixB[x][y].blue  = (_matrixB[x][y-1].blue  + _matrixB[x][y+1].blue  + _matrixB[x-1][y].blue)  / 3;
        return;
    }


    if (y == 0)
    {
        _matrixB[x][y].alpha = (_matrixB[x-1][0].alpha + _matrixB[x][1].alpha + _matrixB[x+1][0].alpha) / 3;
        _matrixB[x][y].red   = (_matrixB[x-1][0].red   + _matrixB[x][1].red   + _matrixB[x+1][0].red)   / 3;
        _matrixB[x][y].green = (_matrixB[x-1][0].green + _matrixB[x][1].green + _matrixB[x+1][0].green) / 3;
        _matrixB[x][y].blue  = (_matrixB[x-1][0].blue  + _matrixB[x][1].blue  + _matrixB[x+1][0].blue)  / 3;
        return;
    }

    if (y == bY-1)
    {
        _matrixB[x][y].alpha = (_matrixB[x-1][y].alpha + _matrixB[x][y-1].alpha + _matrixB[x+1][y].alpha) / 3;
        _matrixB[x][y].red   = (_matrixB[x-1][y].red   + _matrixB[x][y-1].red   + _matrixB[x+1][y].red)   / 3;
        _matrixB[x][y].green = (_matrixB[x-1][y].green + _matrixB[x][y-1].green + _matrixB[x+1][y].green) / 3;
        _matrixB[x][y].blue  = (_matrixB[x-1][y].blue  + _matrixB[x][y-1].blue  + _matrixB[x+1][y].blue)  / 3;
        return;
    }

    _matrixB[x][y].alpha = (_matrixB[x-1][y].alpha + _matrixB[x+1][y].alpha + _matrixB[x][y-1].alpha + _matrixB[x][y+1].alpha) / 4;
    _matrixB[x][y].red   = (_matrixB[x-1][y].red   + _matrixB[x+1][y].red   + _matrixB[x][y-1].red   + _matrixB[x][y+1].red)   / 4;
    _matrixB[x][y].green = (_matrixB[x-1][y].green + _matrixB[x+1][y].green + _matrixB[x][y-1].green + _matrixB[x][y+1].green) / 4;
    _matrixB[x][y].blue  = (_matrixB[x-1][y].blue  + _matrixB[x+1][y].blue  + _matrixB[x][y-1].blue  + _matrixB[x][y+1].blue)  / 4;
}


void LosDrawer::drawLos(int stPosX, int stPosY) {
    int locWidth   = 432 * _scaleSize;
    int locHalf    = 16  * _scaleSize;
    int locWHalf   = locWidth + locHalf;
    int locWNoHalf = locWidth - locHalf;

    _driver->draw2DImage(_darkedZone, core::position2d< s32 >(stPosX+locHalf, stPosY+locHalf),core::rect < s32 >(0, 0, locWNoHalf, locWNoHalf), 0,
                video::SColor(255,255, 255, 255), true);

     _driver->draw2DRectangle(video::SColor(210, 0, 0, 0), core::rect < s32 >(stPosX, stPosY, stPosX+locHalf, stPosY + locWHalf));
     _driver->draw2DRectangle(video::SColor(210, 0, 0, 0), core::rect < s32 >(stPosX+locWidth, stPosY, stPosX+locWHalf, stPosY + locWHalf));

     _driver->draw2DRectangle(video::SColor(210, 0, 0, 0), core::rect < s32 >(stPosX+locHalf, stPosY, stPosX+locWidth, stPosY+locHalf));
     _driver->draw2DRectangle(video::SColor(210, 0, 0, 0), core::rect < s32 >(stPosX+locHalf, stPosY + locWidth, stPosX+locWidth, stPosY + locWHalf));


}


void LosDrawer::_render()
{
    int pixCoordX, pixCoordY;

    pixCoordX = 0;

    int stepSize = 2*_scaleSize;

    for (int i = 0; i < 208; i++)
    {
        pixCoordY = 0;
        for (int j = 0; j < 208; j++)
        {
            int locX = pixCoordX + stepSize;
            int locY = pixCoordY + stepSize;

            for (int x = pixCoordX; x < locX; x++) {
                for (int y = pixCoordY; y < locY; y++) {
                    _additImg->setPixel(x, y, video::SColor(_matrixA[i][j].alpha, _matrixA[i][j].red, _matrixA[i][j].green, _matrixA[i][j].blue));
                }
            }


            pixCoordY += stepSize;
        }
        pixCoordX += stepSize;
    }

    _darkedZone = _driver->addTexture("darkzone", _additImg);
}


