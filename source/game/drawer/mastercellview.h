#ifndef MASTERCELLVIEW_H
#define MASTERCELLVIEW_H


struct MasterCellView {
    int spriteOffset;
    int subCellId;
    bool isWall;
    bool isFire;
    bool isTerminal;

    MasterCellView() {
        spriteOffset = -1;
        subCellId    = -1;
        isWall       = false;
        isFire       = false;
        isTerminal   = false;
    }
};


#endif // MASTERCELLVIEW_H
