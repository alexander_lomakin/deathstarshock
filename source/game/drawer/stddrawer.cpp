

#include "stddrawer.h"


using namespace std;


StdDrawer::StdDrawer(Loaders* loader) : DrawerCarcass(loader) {
    // SYS INIT
    _oldX           = 0;
    _oldY           = 0;
    _targetObjectId = -1;
    _stdTailX       = 32;
    _stdTailXHalf   = 16;
    _stdTailY       = 32;
    _stdTailYHalf   = 16;
    _scaleSize      = 1;
    _isHacking      = false;


    // CALC BASIC OFFSET AND IN-TILES WIDTH ANF HEIGHT
    int width  = IrrlichtBase::getWindowWidth();
    int height = IrrlichtBase::getWindowHeight();

    int drX, drY;

    if (width > 1024 && height > 768) {
        _stdTailX     = 64;
        _stdTailXHalf = 32;
        _stdTailY     = 64;
        _stdTailYHalf = 32;
        _scaleSize = 2;
    }

    drX = width  / _stdTailX + 2;
    drY = height / _stdTailY + 2;

	_offsetX = -1*((width%_stdTailX)/2 + _stdTailX);
	_offsetY = -1*((height%_stdTailY)/2 + _stdTailY);

    if (drX%2 == 0) {
        drX++;
    }

    if (drY%2 == 0) {
        drY++;
    }

    if (drX > 51) {
        drX = 51;
    }

    if (drY > 51) {
        drY = 51;
    }

    // SET DRAWING RADIUS
	_drawingRadiusX = drX / 2;
    _drawingRadiusY = drY / 2;



    // LOAD SPRITES
	_areaTiles    .loadAndInitSheet(OptionLoader::getStrKey("Drawer",      "MapTilesPath"),  8, 12, 32, 32, true, _scaleSize);
	_itemTiles    .loadAndInitSheet(OptionLoader::getStrKey("Drawer",     "ItemTilesPath"),  8,  8, 32, 32, true, _scaleSize);
	_extTiles     .loadAndInitSheet(OptionLoader::getStrKey("Drawer",      "ExtTilesPath"),  9,  1, 32, 32, true, _scaleSize);
	_bloodTiles   .loadAndInitSheet(OptionLoader::getStrKey("Drawer",    "BloodTilesPath"),  7,  1, 32, 32, true, _scaleSize);
	_gunshotTiles .loadAndInitSheet(OptionLoader::getStrKey("Drawer",  "GunshotTilesPath"),  7,  1, 32, 32, true, _scaleSize);
	_creatureTiles.loadAndInitSheet(OptionLoader::getStrKey("Drawer", "CreatureTilesPath"), 19, 12, 32, 32, true, _scaleSize);

    // INIT LOS DRAWER
    _losDrawer.init(_loader->areaLoader, _driver, _scaleSize);

    // INIT GUI
    _initGuiElems();

    // INIT FIRE ANIMATION
    _fire.setAnimationParameters(30, 5, 81, 6, false);
    _fire.startAnimation();

    // INIT TERMINAL ANIMATION
    _terminal.setAnimationParameters(35, 2, 86, 17, false);
    _terminal.startAnimation();

    // INIT DOLL DRAWER
    _dollDrawer.loadAndInit(220, 170);
}


void StdDrawer::_calcActiveObjects() {
	bool _isAllActionsDone = true;

    // CALC FIRE ANIMATION
    if (_fire.isAnimationEnded()) {
        _fire.startAnimation();
    } else {
        _fire.nextStep();
    }

    // CALC TERMINAL ANIMATION
    if (_terminal.isAnimationEnded()) {
        _terminal.startAnimation();
    } else {
        _terminal.nextStep();
    }

    // CALC IS HAKING BAR
    if (_isHacking == true) {
        if (_interfaceVars.hackBar->getProgressNow() == _interfaceVars.hackBar->getProgressMax()) {
            _isHacking = false;
            _interfaceVars.hackBar->drawElem(false);
            _isAllActionsDone = true;
        } else {
            _isAllActionsDone = false;
        }
    }

    // CALC CREATURES ANIMATION
    for(int i = 0; i < MAX_BEASTS; i++) {
        Creature* cr = _loader->beastLoader->getBeastById(i);

        if (cr == 0) {
            continue;
        }

        if (cr->status == DEAD) {
            continue;
        }

        if (cr->isAnimation()) {
            _isAllActionsDone = false;
			cr->doCurAnimationNextStep();
		}

		if (!cr->isMovementEnded()) {
            _isAllActionsDone = false;
            cr->nextMovementStep();
		}
    }

    /*
    // CALC GUNSHOT PARTICLES ANIMATION
	std::list<GunshotParticle>::iterator i;
    for(i = _gunshotParticles.begin(); i != _gunshotParticles.end(); ++i) {
        if (!((*i).stepNow > (*i).lnEq.getStepSum())) {
			(*i).stepNow += 32*_scaleSize;

            int curX, curY;
            (*i).lnEq.setCoordByStep(curX, curY, (*i).stepNow);
            curX = ((*i).startX + (curX / (32*_scaleSize)));
            curY = ((*i).startY + (curY / (32*_scaleSize)));

            ExtendedCell* eCell = _loader->areaLoader->getExtendedCell(curX, curY);
            if (eCell == 0 && (*i).deadBeastId == -1) {
                i = _gunshotParticles.erase(i);
                continue;
            }

            if (eCell->isVisible == false && (*i).deadBeastId == -1) {
                i = _gunshotParticles.erase(i);
                continue;
            }

			_isAllActionsDone = false;
        } else {
            if ((*i).deadBeastId != -1) {
                EventStack::pushEvent(ViewEvent(BEAST_DEAD, (*i).deadBeastId, 0, 0, 0, ""));
                _isAllActionsDone = false;
            }
            i = _gunshotParticles.erase(i);
        }
    }
    */


	if (_isAllActionsDone) {
        EventStack::pushEvent(ModelEvent(CONTINUE_WORK, 0, 0, 0, 0, ""));
	}
}


void StdDrawer::_setTargetPos(int& globX, int& globY) {

    // CALC TARGET POSITION
    globX = -100;
    globY = -100;

    if (_targetObjectId == -1) {
       globX = _oldX;
       globY = _oldY;
       return;
    }

    Creature* cr = _loader->beastLoader->getBeastById(_targetObjectId);
    if (cr != 0) {
        globX = cr->x;
        globY = cr->y;
    }

    // SAVE PREVIOS POSITION
	if (globX == -100 || globY == -100) {
        globX = _oldX;
        globY = _oldY;
    } else {
        _oldX = globX;
        _oldY = globY;
    }
}


void StdDrawer::_setAnimOffset(int& animX, int& animY) {
    // DEFAULT OFFSET
    animX = 0;
    animY = 0;

    if (_targetObjectId == -1)
        return;

    // GET TARGET OFFSET
    Creature* cr = _loader->beastLoader->getBeastById(_targetObjectId);
    if (cr != 0) {
        cr->getOffsetNow(animX, animY);
        animX *= -1;
        animY *= -1;
    }
}


void StdDrawer::_initGuiElems() {
    int width  = IrrlichtBase::getWindowWidth();
    int height = IrrlichtBase::getWindowHeight();

    _guiManager.addElem(_interfaceVars.guiList);
    _guiManager.addElem(_interfaceVars.guiArgList);
    _guiManager.addElem(_interfaceVars.guiStatus);
    _guiManager.addElem(_interfaceVars.guiWeaponInfo);

    _guiManager.addElem(_interfaceVars.armorStatusBar);
    _guiManager.addElem(_interfaceVars.hitPointsBar);
    _guiManager.addElem(_interfaceVars.tirednessBar);
    _guiManager.addElem(_interfaceVars.experienceBar);
    _guiManager.addElem(_interfaceVars.hackBar);


    _interfaceVars.experienceBar->setProgressMax(33);


    _interfaceVars.hitPointsBar->setPosition(10, 30);
    _interfaceVars.hitPointsBar->setColors(GREEN, DARK_GREEN);
    _interfaceVars.tirednessBar->setPosition(10, 50);
    _interfaceVars.tirednessBar->setColors(GRAY, DARK_GRAY);
    _interfaceVars.armorStatusBar->setPosition(10, 70);
    _interfaceVars.armorStatusBar->setColors(YELLOW, DARK_YELLOW);

    _interfaceVars.experienceBar->setPosition(width/2 - (66), 0);
    _interfaceVars.experienceBar->drawElem(false);

    _interfaceVars.guiStatus->setPosition(10, height - 20);
    _interfaceVars.guiWeaponInfo->setPosition(10, 90);


    _interfaceVars.guiList->setSize(500, 500);
    _interfaceVars.guiList->setPosition(width/2 - 250, height/2 - 250);

    _interfaceVars.guiArgList->setSize(500, 500);
    _interfaceVars.guiArgList->setPosition(width/2 - 250, height/2 - 250);
    _interfaceVars.guiArgList->setValOffset(300);
}


