

#include "demonhand.h"


using namespace std;


DemonHand::DemonHand()
{
    srand(time(0));
}


DemonHand::DemonHand(GameLogic* gmLogic, Loaders* loader, DemonDeck* deck, CombatZone* comZone)
{
    _loader     = loader;
    _demonDeck  = deck;
    _combatZone = comZone;
    _gmLogic    = gmLogic;
    srand(time(0));
}


void DemonHand::init(GameLogic* gmLogic, Loaders* loader, DemonDeck* deck, CombatZone* comZone) {
    _loader     = loader;
    _demonDeck  = deck;
    _combatZone = comZone;
    _gmLogic    = gmLogic;
}


void DemonHand::popAllInactiveBeasts() {
    for(int i = 0; i < MAX_BEASTS; i++) {
        Creature* cr = _loader->beastLoader->getBeastById(i);
        if (cr == 0) {
            continue;
        }

        if (cr->status == DEAD) {
            continue;
        }

        ExtendedCell* cell = _loader->areaLoader->getExtendedCell(cr->x, cr->y);

        if (cell == 0) {
            continue;
        }

        if (cell->combatZoneLevel == -1) {
            DemonDeckCard card;
            card.name = cr->offName;

            _loader->beastLoader->removeBeast(cr->id);

            _demonDeck->pushCard(card);
        }
    }
}


void DemonHand::pushCreature()
{
    if (_demonDeck->isDeckEmpty() || !_loader->beastLoader->canMakeBeast())
        return;

    DemonDeckCard card = _demonDeck->popCard();

    int dist = 8;
    int globX, globY;

    do {
        _combatZone->setRandomCoordOnDist(_loader->beastLoader, globX, globY, dist);
        dist++;
        if (dist > _combatZone->getCombatZoneWidth())
            break;
    }
    while (globX == -100 && globY == -100);

    if (globX == -100 && globY == -100)
    {
        _demonDeck->pushCard(card);
        return;
    }

    cout << "card: " << card.name << endl;

    int id = spawnMonsterAndGetId(card.name, globX, globY, _loader, _gmLogic);

    EventStack::pushEvent(ViewEvent(ACTIVEOBJ_CREATED, id, card.ownerId, globX, globY, ""));
}



