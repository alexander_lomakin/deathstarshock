#ifndef COMBATZONE_H
#define COMBATZONE_H


#include <stack>
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <ctime>


#include "loaders/arealoader.h"
#include "loaders/beastloader.h"


using namespace std;

    class CombatZone
    {
    public:
        CombatZone();
        CombatZone(AreaLoader* nAreaLoader);
        void setAreaLoader(AreaLoader* nAreaLoader);
        void setCombatZoneWidth(int nComZoneWidth);
        void clearCombatZone();
        void calcCombatZone(int globX, int globY);
        void setRandomCoordOnDist(BeastLoader* _beastLoader, int& x, int& y, int distantion);
        int getCombatZoneWidth();
    private:
        int _combatZoneWidth;
        int _combatZoneCenterX;
        int _combatZoneCenterY;
        AreaLoader* _areaLoader;
        bool _isCellCorrect(int globX, int globY, int comZoneWidthNow);
        bool _isCellInLastCombatZoneStep(int globX, int globY, int comZoneWidthNow);
        stack<int> _stackX;
        stack<int> _stackY;
    };


#endif // COMBATZONE_H
