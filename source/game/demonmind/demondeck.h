#ifndef DEMONDECK_H
#define DEMONDECK_H


#include <vector>
#include <string>
#include <cstring>
#include <fstream>
#include <algorithm>
#include <cstdlib>
#include <time.h>
#include <irrXML.h>

#include "demondeckcard.h"

#include "loaders/optionloader.h"

#include "its.h"


using namespace std;


    class DemonDeck
    {
    public:
        DemonDeck();
        void loadDemonDeckFromFile();
        void shuffleDeck();
        void assembleDeck();

        bool isDeckFull();
        bool isDeckEmpty();
        int getCardRating(DemonDeckCard* card);

        void pushCard(DemonDeckCard card);
        DemonDeckCard popCard();
    private:
        string _deckFilePath;
        vector<DemonDeckCard> _deck;
        vector<DemonDeckCard> _fullDeck;
    };



#endif // DEMONDECK_H
