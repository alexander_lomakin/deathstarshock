#ifndef DEMONDECKCARD_H
#define DEMONDECKCARD_H


#include <string>
#include <iostream>

#include "objects/creature.h"


using namespace std;


    class DemonDeckCard
    {
    public:
        DemonDeckCard();
        string name;
        int ownerId;
        int minLv;
        int maxLv;
        int groupSize;

        void traceInConsole();
    };



#endif // DEMONDECKCARD_H
