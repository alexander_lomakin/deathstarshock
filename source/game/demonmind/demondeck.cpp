

#include "demondeck.h"


using namespace std;
using namespace irr;
using namespace io;


DemonDeck::DemonDeck()
{
    _deckFilePath = OptionLoader::getStrKey("DemonMind", "deckFilePath");
    srand(time(0));
}


void DemonDeck::loadDemonDeckFromFile()
{
    IrrXMLReader* xml = createIrrXMLReader(_deckFilePath.c_str());

    while(xml && xml->read())
    {


        if (xml->getNodeType() == EXN_ELEMENT)
        {
            if (!strcmp("card", xml->getNodeName()))
            {
                DemonDeckCard card;

                card.name = xml->getAttributeValue("name");
                card.ownerId = STI(xml->getAttributeValue("ownerId"));
                card.minLv = STI(xml->getAttributeValue("minlevel"));
                card.maxLv = STI(xml->getAttributeValue("maxlevel"));
                card.groupSize = STI(xml->getAttributeValue("groupsize"));


                for (int j = 0; j < card.groupSize; j++)
                    _fullDeck.push_back(card);

             }
        }
   }

   delete xml;
}


void DemonDeck::shuffleDeck()
{
    random_shuffle(_fullDeck.begin(), _fullDeck.end());
}


void DemonDeck::assembleDeck()
{
    for (int j = 0; j < 4; j++)
        for (int i = 0; i < _fullDeck.size(); i++)
        {
            if (isDeckFull())
                return;

            int rating = getCardRating(&_fullDeck[i]);

            int rnd = rand() % 100 + 1;
            if (rnd <= rating)
            {
                _deck.push_back(_fullDeck[i]);
                _fullDeck.erase(_fullDeck.begin()+i);
            }
        }
}


bool DemonDeck::isDeckFull()
{
    return (_deck.size() >= 20);
}


bool DemonDeck::isDeckEmpty()
{
    return (_deck.size() == 0);
}


int DemonDeck::getCardRating(DemonDeckCard* card)
{
    int rating = 30;

    return rating;
}


void DemonDeck::pushCard(DemonDeckCard card)
{
    _deck.push_back(card);
}

DemonDeckCard DemonDeck::popCard()
{
    DemonDeckCard card = _deck.back();
    _deck.pop_back();
    return card;
}
