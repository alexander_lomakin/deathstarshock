#ifndef DEMONMIND_H
#define DEMONMIND_H


#include "demondeck.h"
#include "demonhand.h"
#include "combatzone.h"

#include "mechanics/gamelogic.h"
#include "loaders/loaders.h"


using namespace std;



    class DemonMind
    {
    public:
        DemonMind(Loaders* loader, GameLogic* gmLogic);
        void step();
        void combatZoneStep(int plX, int plY);
    private:
        int _waitTime;
        int _pushingTimer;

        bool _waiting;

        CombatZone _combatZone;
        DemonDeck _demonDeck;
        DemonHand _demonHand;

        Loaders* _loader;
        GameLogic* _gmLogic;

        void _popPhase();
        void _pushPhase();
        void _turnPhase();
        void _endOfMind();
    };


#endif // DEMONMIND_H
