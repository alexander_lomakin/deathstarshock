

#include "combatzone.h"


using namespace std;

CombatZone::CombatZone()
{
    _combatZoneWidth = 0;
    srand(time(0));
}


CombatZone::CombatZone(AreaLoader* nAreaLoader)
{
    _areaLoader = nAreaLoader;
    _combatZoneWidth = 0;
    srand(time(0));
}


void CombatZone::setAreaLoader(AreaLoader* nAreaLoader)
{
    _areaLoader = nAreaLoader;
}


void CombatZone::setCombatZoneWidth(int nComZoneWidth)
{
    _combatZoneWidth = nComZoneWidth;
}


void CombatZone::clearCombatZone()
{
    for (int x = -AREA_WIDTH; x < 2*AREA_WIDTH; x++)
		for (int y = -AREA_HEIGHT; y < 2*AREA_HEIGHT; y++)
		{
			ExtendedCell* cell = _areaLoader->getExtendedCell(x, y);
			cell->combatZoneLevel = -1;
		}
}


void CombatZone::calcCombatZone(int globX, int globY) {
    if (globX == -100 || globY == -100) {
        return;
    }

    ExtendedCell* cell = _areaLoader->getExtendedCell(globX, globY);
    cell->combatZoneLevel = 0;

    _combatZoneCenterX = globX;
    _combatZoneCenterY = globY;

    for (int i = 1; i <= _combatZoneWidth; i++)
    {
        for (int x = -AREA_WIDTH; x < 2*AREA_WIDTH; x++)
            for (int y = -AREA_HEIGHT; y < 2*AREA_HEIGHT; y++)
            {
                if (_isCellCorrect(x, y, i))
                {
                    _stackX.push(x);
                    _stackY.push(y);
                }
            }
        while (!_stackX.empty())
        {
            ExtendedCell* cell = _areaLoader->getExtendedCell(_stackX.top(), _stackY.top());
            _stackX.pop();
            _stackY.pop();
			cell->combatZoneLevel = i;
        }
    }
}


bool CombatZone::_isCellCorrect(int globX, int globY, int comZoneWidthNow) {
    AreaCell* cell            = _areaLoader->getCell(globX, globY);
    MasterAreaCell masterCell = _areaLoader->getMasterCell(cell->type);

    if (masterCell.isPassable == false && masterCell.addit != "door") {
        return false;
    }

    if (_isCellInLastCombatZoneStep(globX, globY, comZoneWidthNow)) {
        return false;
    }

    if (!_isCellInLastCombatZoneStep(globX-1, globY, comZoneWidthNow)
        && !_isCellInLastCombatZoneStep(globX+1, globY, comZoneWidthNow)
        && !_isCellInLastCombatZoneStep(globX, globY-1, comZoneWidthNow)
        && !_isCellInLastCombatZoneStep(globX, globY+1, comZoneWidthNow)) {
        return false;
    } else {
        return true;
    }
}


bool CombatZone::_isCellInLastCombatZoneStep(int globX, int globY, int comZoneWidthNow)
{
    ExtendedCell* cell = _areaLoader->getExtendedCell(globX, globY);
    if (cell == 0)
        return false;
    if (cell->combatZoneLevel < comZoneWidthNow && cell->combatZoneLevel != -1)
        return true;

    return false;
}




void CombatZone::setRandomCoordOnDist(BeastLoader* _beastLoader, int& x, int& y, int distantion)
{
    x = -100;
    y = -100;

    vector<int> stackX;
    vector<int> stackY;
    vector<int> ids;

    int id = -1;

    for (int i = -AREA_WIDTH; i < 2*AREA_WIDTH; i++)
        for (int j = -AREA_HEIGHT; j < 2*AREA_HEIGHT; j++)
        {
            ExtendedCell* cell = _areaLoader->getExtendedCell(i, j);
            if (cell->combatZoneLevel == distantion && _beastLoader->getBeastByPos(i, j) == 0)
            {
                    stackX.push_back(i);
                    stackY.push_back(j);
                    id++;
                    ids.push_back(id);
            }
        }

    if (ids.size() == 0)
        return;

    random_shuffle(ids.begin(), ids.end());

    int randId = rand()%ids.size();

    x = stackX[randId];
    y = stackY[randId];

}


int CombatZone::getCombatZoneWidth()
{
    return _combatZoneWidth;
}
