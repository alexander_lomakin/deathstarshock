

#include "demondeckcard.h"


using namespace std;


DemonDeckCard::DemonDeckCard()
{
    name      = "none";
    ownerId   = -1;
    minLv     = 0;
    maxLv     = 1;
    groupSize = 1;
}


void DemonDeckCard::traceInConsole()
{
    cout << "name: " << name << endl;
    cout << "ownerId: " << ownerId << endl;
    cout << "minLv: " << minLv << endl;
    cout << "maxLv: " << maxLv << endl;
    cout << "groupSize: " << groupSize << endl;
}
