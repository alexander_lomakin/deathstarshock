

#include "demonmind.h"


using namespace std;


DemonMind::DemonMind(Loaders* loader, GameLogic* gmLogic)
{
    _loader  = loader;
    _gmLogic = gmLogic;

    _combatZone.setAreaLoader(_loader->areaLoader);
    _combatZone.setCombatZoneWidth(25);
    _combatZone.clearCombatZone();


    _demonHand.init(_gmLogic, _loader, &_demonDeck, &_combatZone);


    _waitTime = 15;
    _waiting = true;

    _pushingTimer = 0;
}


void DemonMind::step()
{
    if (_waitTime != 0)
    {
        _waitTime--;
        return;
    }

    if (_waiting)
    {
        _waiting = false;
        if (_demonDeck.isDeckEmpty())
        {
            _demonDeck.loadDemonDeckFromFile();
            _demonDeck.shuffleDeck();
            _demonDeck.assembleDeck();
            return;
        }
    }
    else
    {
        if (_demonDeck.isDeckEmpty())
        {
            _endOfMind();
           return;
        }
    }

    _popPhase();
    _pushPhase();
    _turnPhase();
}




void DemonMind::_pushPhase()
{
    if (_pushingTimer == 0)
    {
        for (int i = 0; i < 3; i++)
            _demonHand.pushCreature();

        _pushingTimer = 4;
    }
    else
        _pushingTimer--;
}


void DemonMind::combatZoneStep(int plX, int plY)
{
    _combatZone.clearCombatZone();
    _combatZone.calcCombatZone(plX, plY);
}


void DemonMind::_popPhase()
{
    _demonHand.popAllInactiveBeasts();
}


void DemonMind::_endOfMind()
{
    _waiting = true;
    _waitTime = 50;
}


void DemonMind::_turnPhase()
{

}

