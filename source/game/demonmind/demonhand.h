#ifndef DEMONHAND_H
#define DEMONHAND_H


#include <cstdlib>
#include <time.h>

#include "demondeck.h"
#include "combatzone.h"

#include "mechanics/gamelogic.h"

#include "monsters/spawn.h"

#include "loaders/loaders.h"

#include "eventstack/eventstack.h"


using namespace std;


    class DemonHand
    {
    public:
        DemonHand();
        DemonHand(GameLogic* gmLogic, Loaders* loader, DemonDeck* deck, CombatZone* comZone);
        void init(GameLogic* gmLogic, Loaders* loader, DemonDeck* deck, CombatZone* comZone);
        void popAllInactiveBeasts();
        void pushCreature();
    private:
        GameLogic*           _gmLogic;
        Loaders*             _loader;
        DemonDeck*           _demonDeck;
        CombatZone*          _combatZone;
    };



#endif // DEMONHAND_H
