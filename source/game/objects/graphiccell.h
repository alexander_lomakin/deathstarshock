#ifndef GRAPHICCELL_H
#define GRAPHICCELL_H


struct GraphicCell {
        GraphicCell()
        {
            spriteId      = -1;
            additSpriteId = -1;
            bloodSpriteId = -1;
            itemSpriteId  = -1;
            isFire        = false;
            isTerminal    = false;
        }

        int spriteId;
        int additSpriteId;
        int bloodSpriteId;
        int itemSpriteId;
        bool isFire;
        bool isTerminal;
};


#endif // GRAPHICCELL_H
