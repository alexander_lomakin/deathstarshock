

#include "movement.h"



Movement::Movement()
{
	_stepOffsetSize = 4;
	_nowOffsetX = 0;
	_nowOffsetY = 0;
	_maxX = 0;
	_maxY = 0;

	_lineStep = 0;
    _useLineEquation = false;

	_direction = BOTTOM;
}


void Movement::nextMovementStep()
{
	switch (_direction)
	{
		case LEFT:
			_nowOffsetX -= _stepOffsetSize;
			break;
		case RIGHT:
			_nowOffsetX += _stepOffsetSize;
			break;
		case TOP:
			_nowOffsetY -= _stepOffsetSize;
			break;
		case BOTTOM:
			_nowOffsetY += _stepOffsetSize;
			break;
        case LEFT_BOTTOM: {
			_nowOffsetY += _stepOffsetSize;
			_nowOffsetX -= _stepOffsetSize;
			} break;
        case RIGHT_BOTTOM: {
			_nowOffsetY += _stepOffsetSize;
			_nowOffsetX += _stepOffsetSize;
			} break;
        case RIGHT_TOP: {
			_nowOffsetY -= _stepOffsetSize;
			_nowOffsetX += _stepOffsetSize;
			} break;
        case LEFT_TOP: {
			_nowOffsetY -= _stepOffsetSize;
			_nowOffsetX -= _stepOffsetSize;
			} break;
	}
}


void Movement::nextEquationStep()
{
    _lnEquation.setCoordByStep(_nowOffsetX, _nowOffsetY, _lineStep);
    _lineStep += _stepOffsetSize;
}


void Movement::startMovement(int nX, int nY, int nStepOffsetSize, Direction nDirection)
{
	_stepOffsetSize = nStepOffsetSize;
	_maxX           = nX;
	_maxY           = nY;
	_direction      = nDirection;
}


void Movement::startAMovement(int nX0, int nY0, int nX1, int nY1, int nStepOffsetSize)
{
    _lnEquation.init(nX0, nY0, nX1, nY1);


    _useLineEquation = true;
    _stepOffsetSize = nStepOffsetSize;
    _nowOffsetX = nX0;
	_nowOffsetY = nY0;
	_lineStep   = 0;
}


void Movement::startMovement(int nX, int nY, int stX, int stY, int nStepOffsetSize, Direction nDirection)
{
    _stepOffsetSize = nStepOffsetSize;
	_maxX           = nX;
	_maxY           = nY;
	_direction      = nDirection;
	_nowOffsetX     = stX;
	_nowOffsetY     = stY;
}


void Movement::getOffsetNow(int& x, int & y)
{
	x = _nowOffsetX;
	y = _nowOffsetY;
}


bool Movement::isMovementEnded() {
    if (_useLineEquation) {
        if (_lineStep >= _lnEquation.getStepSum()) {
            return true;
        }
        return false;
    }

	switch (_direction) {
		case LEFT: {
			if (_nowOffsetX <= -_maxX) {
                return true;
            }
        } break;
		case RIGHT: {
			if (_nowOffsetX >= _maxX) {
                return true;
            }
        } break;
		case TOP: {
			if (_nowOffsetY <= -_maxY) {
                return true;
            }
        } break;
        case LEFT_TOP: {
            if (_nowOffsetY <= _maxY && _nowOffsetX <= -_maxX) {
                return true;
            }
        } break;
        case RIGHT_TOP: {
            if (_nowOffsetY <= _maxY && _nowOffsetX >= -_maxX) {
                return true;
            }
        } break;
		case BOTTOM: {
			if (_nowOffsetY >= _maxY) {
                return true;
            }
        } break;
        case LEFT_BOTTOM: {
            if (_nowOffsetY >= _maxY && _nowOffsetX <= -_maxX) {
                return true;
            }
        } break;
        case RIGHT_BOTTOM: {
            if (_nowOffsetY >= _maxY && _nowOffsetX >= -_maxX) {
                return true;
            }
        } break;
	}

    return false;
}


Direction Movement::getDirection()
{
    return _direction;
}


void Movement::useLineEquation(bool use)
{
    _useLineEquation = use;
}
