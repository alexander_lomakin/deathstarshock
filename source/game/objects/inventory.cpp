

#include "inventory.h"


using namespace std;


Inventory::Inventory() {
    init();
}


Inventory::~Inventory() {
    for (int i = 0; i < INVENTORY_SIZE; i++) {
        if (_pool[i] != 0) {
           // delete _pool[i];
        }
    }
}


void Inventory::init() {
    _employedCells = 0;

    for (int i = 0; i < INVENTORY_SIZE; i++) {
        _pool[i] = 0;
    }
}


bool Inventory::canPushItem() {
    return (_employedCells < INVENTORY_SIZE - 1);
}


void Inventory::pushItem(CommonItem* item) {
    int freeCell = -1;

    for (int i = 0; i < INVENTORY_SIZE; i++) {
        if (_pool[i] == 0) {
            freeCell = i;
            break;
        }
    }

    if (freeCell == -1) {
        throw 18;
    }

    _employedCells++;

    _pool[freeCell] = item;
}


CommonItem* Inventory::popItem(int id) {
    for (int i = 0; i < INVENTORY_SIZE; i++) {
        if (_pool[i] != 0) {
            if (_pool[i]->id == id) {
                CommonItem* item = _pool[i];

                _pool[i] = 0;
                _employedCells--;

                return item;
            }
        }
    }

    throw 24;
}


CommonItem* Inventory::popItem(string name) {
    for (int i = 0; i < INVENTORY_SIZE; i++) {
        if (_pool[i] != 0) {
            if (_pool[i]->name == name) {
                CommonItem* item = _pool[i];

                _pool[i] = 0;
                _employedCells--;

                return item;
            }
        }
    }

    throw 24;
}


CommonItem* Inventory::getItemById(int id) {
    for (int i = 0; i < INVENTORY_SIZE; i++) {
        if (_pool[i] != 0) {
            if (_pool[i]->id == id) {
                return _pool[i];
            }
        }
    }

    throw 1;
}


CommonItem* Inventory::getItemByPos(int position) {
    return _pool[position];
}


int Inventory::size() {
    return _employedCells;
}


void Inventory::destroyEmptyAmmo() {

}


bool Inventory::isEmpty() {
    return (_employedCells == 0);
}
