

#include "area.h"


using namespace std;


Area::Area()
{
	_isFirstlyVisited = true;
}


void Area::_fillAreaWithEmptyCells()
{
	AreaCell cell;
	GraphicCell graphCell;
    ExtendedCell extCell;

	for (int x = 0; x < AREA_WIDTH; x++)
		for (int y = 0; y < AREA_HEIGHT; y++)
        {
            table[x][y]        = cell;
            extTable[x][y]     = extCell;
            graphicTable[x][y] = graphCell;
        }

}


void Area::saveAreaToFile(string areaPath)
{
    ofstream areaFile(areaPath.c_str());

    areaFile << _isFirstlyVisited << endl;

	for (int x = 0; x < AREA_WIDTH; x++)
		for (int y = 0; y < AREA_HEIGHT; y++)
        {
			areaFile << table[x][y].type            << endl;
			areaFile << table[x][y].lightType       << endl;
			areaFile << table[x][y].bloodLevel      << endl;
            areaFile << table[x][y].generationGroup << endl;
            areaFile << table[x][y].hitPoints       << endl;
            areaFile << table[x][y].additCell       << endl;
            areaFile << table[x][y].isExplored      << endl;
        }

	areaFile.close();
}


void Area::loadAreaFromFile(string areaPath)
{
	ifstream areaFile(areaPath.c_str());

	if (!areaFile)
	{
		_fillAreaWithEmptyCells();
		areaFile.close();
		return;
	}

	areaFile >> _isFirstlyVisited;

	for (int x = 0; x < AREA_WIDTH; x++)
		for (int y = 0; y < AREA_HEIGHT; y++)
		{
			areaFile >> table[x][y].type;
			areaFile >> table[x][y].lightType;
			areaFile >> table[x][y].bloodLevel;
			areaFile >> table[x][y].generationGroup;
			areaFile >> table[x][y].hitPoints;
			areaFile >> table[x][y].additCell;
			areaFile >> table[x][y].isExplored;
		}

	areaFile.close();
}


void Area::loadAreaFromFile(int nX, int nY, int nLevel, string areaPath, string logDir)
{
	ifstream areaFile(areaPath.c_str());

	if (!areaFile)
	{
		_fillAreaWithEmptyCells();
		areaFile.close();

		// save new area to log
		ofstream logFile(logDir.c_str(), ios::app);
 		logFile << nX << " " << nY << " " << nLevel << " " << areaPath << endl;
 		logFile.close();
		return;
	}

	areaFile >> _isFirstlyVisited;

	for (int x = 0; x < AREA_WIDTH; x++)
		for (int y = 0; y < AREA_HEIGHT; y++)
		{
			areaFile >> table[x][y].type;
			areaFile >> table[x][y].lightType;
			areaFile >> table[x][y].bloodLevel;
			areaFile >> table[x][y].generationGroup;
			areaFile >> table[x][y].hitPoints;
			areaFile >> table[x][y].additCell;
			areaFile >> table[x][y].isExplored;
		}

	areaFile.close();
}


bool Area::isFirstlyVisited()
{
    return _isFirstlyVisited;
}


void Area::makeAreaOldVisited()
{
    _isFirstlyVisited = false;
}
