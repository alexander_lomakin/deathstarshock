#ifndef CRITMODIFIER_H
#define CRITMODIFIER_H


class CritModifier {
    public:
        CritModifier();
        void init(int nDuration, int nStamina, int nConcentration);
        void calc(int &stamina, int &concentration);
        bool isActive();

        int duration;
        int staminaPerTurn;
        int concentrationPerTurn;
};


#endif // CRITMODIFIER_H
