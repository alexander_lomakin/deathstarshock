#ifndef TRAIT_H
#define TRAIT_H


#include <string>
#include <cmath>


using namespace std;


class Trait {
    public:
        Trait(string nName, int nK, int nTimeToLoseMax, int nLoseSpeed);
        Trait();

        void init(string nName, int nK, int nTimeToLoseMax, int nLoseSpeed);

        string getName();
        int getLevel();
        void increase();
        void traitCalc();
    private:
        string _name;
        int    _pointsNow;
        int    _k;

        bool   _isLosing;
        int    _timeToLoseNow;
        int    _timeToLoseMax;
        int    _loseSpeedPerTurn;
};

#endif // TRAIT_H
