

#include "trait.h"


using namespace std;


Trait::Trait(string nName, int nK, int nTimeToLoseMax, int nLoseSpeed) {
    _name             = nName;
    _k                = nK;
    _timeToLoseMax    = nTimeToLoseMax;
    _loseSpeedPerTurn = nLoseSpeed;

    _pointsNow     = 0;
    _timeToLoseNow = 0;
}


Trait::Trait() {
    _name             = "none-trait";
    _k                = 0;
    _timeToLoseMax    = 0;
    _loseSpeedPerTurn = 0;

    _pointsNow     = 0;
    _timeToLoseNow = 0;
}


void Trait::init(string nName, int nK, int nTimeToLoseMax, int nLoseSpeed) {
    _name             = nName;
    _k                = nK;
    _timeToLoseMax    = nTimeToLoseMax;
    _loseSpeedPerTurn = nLoseSpeed;

    _pointsNow     = 0;
    _timeToLoseNow = 0;
}


string Trait::getName() {
    return _name;
}


int Trait::getLevel() {
    if (_pointsNow == 0) {
        return 0;
    } else {
        return _k*log(_pointsNow);
    }
}


void Trait::increase() {
    _pointsNow++;

    _timeToLoseNow = 0;
}


void Trait::traitCalc() {
    _timeToLoseNow++;

    if (_timeToLoseNow >= _timeToLoseMax) {
        _pointsNow -= _loseSpeedPerTurn;
        if (_pointsNow < 0) {
            _pointsNow=0;
        }
    }
}
