#ifndef HEROPROFILE_H_
#define HEROPROFILE_H_


#include <string>
#include <iostream>
#include <fstream>

#include "skill.h"
#include "bodypart.h"
#include "trait.h"
#include "critmodifier.h"


using namespace std;


enum CreatureStatus { NORMAL=0, TIRED, RUN, DEAD };

enum DummyPart { DUMMY_EMPTY=0, DUMMY_HEAD, DUMMY_BODY, DUMMY_LEFTHAND, DUMMY_RIGHTHAND, DUMMY_LEFTLEG, DUMMY_RIGHTLEG };

#define DUMMY_X 16
#define DUMMY_Y 14


class HeroProfile {
    public:
        HeroProfile();

        string name;

        CreatureStatus status;

        // DUMMY
        DummyPart dummy[DUMMY_X][DUMMY_Y];

        BodyPart head;
        BodyPart body;
        BodyPart lHand;
        BodyPart rHand;
        BodyPart lLeg;
        BodyPart rLeg;

        // BASE CHARS
        int stamina;
        int staminaMax;

        int concentration;
        int concentrationMax;

        // MODIFIERS
        CritModifier shock;
        CritModifier contusion;
        CritModifier blindness;
        CritModifier bleeding;

        // TRAITS
        Trait smallArms;
        Trait throwingWeapons;
        Trait melee;
        Trait sapper;
        Trait intelligence;
        Trait stealth;
        Trait computers;
        Trait technics;
        Trait science;
        Trait survival;
        Trait willPower;
        Trait medicine;
        Trait lucky;

        void calc();
        bool checkDead();

        void loadDummy(ifstream& file);
        float getConcentrationInPerCent();
        int getActiveHandSum();
        void recheckBaseChars();
    private:
        void _effectsCalc();
        void _traitCalc();
};


#endif
