

#include "skill.h"


Skill::Skill()
{
	setMax(0);
	setNow(0);
	setMin(0);
}


Skill::Skill(int nMax, int nNow, int nMin)
{
	setMax(nMax);
	setNow(nNow);
	setMin(nMin);
}


void Skill::set(int nMax, int nNow, int nMin) {
    setMax(nMax);
	setNow(nNow);
	setMin(nMin);
}


void Skill::setMax(int nMax)
{
	_max = nMax;
}


void Skill::setNow(int nNow)
{
	_now = nNow;
	if (_now < 0 || _now > _max)
		_now = 0;
}



void Skill::setMin(int nMin)
{
    _min = nMin;
}


void Skill::increase()
{
	if (_now+1 <= _max)
		_now++;
}


void Skill::increase(int val) {
    _now += val;

    if (_now > _max) {
        _now = _max;
    }
}


void Skill::decrease()
{
	if (_now-1 >= _min)
		_now--;
}


void Skill::decrease(int val)
{
    _now -= val;
    if (_now < _min)
        _now = _min;
}


bool Skill::canIncrease()
{
	if (_now+1 <= _max)
		return true;
	return false;
}


bool Skill::canDecrease()
{
	if (_now-1 >= 0)
		return true;
	return false;
}


int Skill::getMax()
{
    return _max;
}


int Skill::getNow()
{
	return _now;
}


int Skill::getMin()
{
    return _min;
}


int Skill::getPointSum()
{
    return _max - _min;
}


bool Skill::isReachedMax()
{
    return (_now == _max);
}


bool Skill::isReachedMin()
{
    return (_now == _min);
}
