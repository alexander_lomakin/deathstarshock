#ifndef SKILL_H_
#define SKILL_H_


    class Skill
    {
    public:
        Skill();
        Skill(int nMax, int nNow, int nMin);

        void set(int nMax, int nNow, int nMin);

        void setMax(int nMax);
        void setNow(int nNow);
        void setMin(int nMin);

        void increase();
        void increase(int val);
        void decrease();

        void decrease(int val);

        bool canIncrease();
        bool canDecrease();

        int getMax();
        int getNow();
        int getMin();

        int getPointSum();

        bool isReachedMax();
        bool isReachedMin();
    private:
        int _max;
        int	_now;
        int _min;
    };


#endif
