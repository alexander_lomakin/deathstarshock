

#include "heroprofile.h"


HeroProfile::HeroProfile() {
	// CORE
	status = NORMAL;
	name   = "<none>";

	// SET BODY PARAMETERS
	head .hitPoints.set(4,  4,  0);
	body .hitPoints.set(16, 16, 0);
	lHand.hitPoints.set(6,  6,  0);
	rHand.hitPoints.set(6,  6,  0);
	rLeg .hitPoints.set(6,  6,  0);
	lLeg .hitPoints.set(8,  8,  0);

    // SET SKILLS
	smallArms      .init("Small Arms",       1, 50, 2);
	throwingWeapons.init("Throwing Weapons", 1, 50, 2);
	melee          .init("Melee",            1, 50, 2);
	sapper         .init("sapper",           1, 50, 2);
	intelligence   .init("Intelligence",     1, 50, 2);
	stealth        .init("Stealth",          1, 50, 2);
	computers      .init("Computers",        1, 50, 2);
	technics       .init("Technics",         1, 50, 2);
	science        .init("Science",          1, 50, 2);
	survival       .init("Survival",         1, 50, 2);
	willPower      .init("Will Power",       1, 50, 2);
	medicine       .init("Medicine",         1, 50, 2);
	lucky          .init("Lucky",            1, 50, 2);

    // BASE CHARS
    recheckBaseChars();
	stamina          = staminaMax;
	concentration    = concentrationMax;

    // INIT CRIT MODIFIERS
    shock    .init(0, 0, 0);
    contusion.init(0, 0, 0);
    blindness.init(0, 0, 0);
    bleeding .init(0, 0, 0);

    // PREPAIR DUMMY
	for (int i = 0; i < DUMMY_X; i++) {
        for (int j = 0; j < DUMMY_Y; j++) {
            dummy[i][j] = DUMMY_EMPTY;
        }
    }
}


void HeroProfile::calc() {
    _traitCalc();
    _effectsCalc();
    checkDead();
}


bool HeroProfile::checkDead() {
    if (stamina <= 0 || status == DEAD || !head.hitPoints.canDecrease() || !body.hitPoints.canDecrease()) {
        status = DEAD;
        return true;
    } else {
        status = NORMAL;
        return false;
    }
}


void HeroProfile::_traitCalc() {
    smallArms      .traitCalc();
	throwingWeapons.traitCalc();
	melee          .traitCalc();
	sapper         .traitCalc();
	intelligence   .traitCalc();
	stealth        .traitCalc();
	computers      .traitCalc();
	technics       .traitCalc();
	science        .traitCalc();
	survival       .traitCalc();
	willPower      .traitCalc();
	medicine       .traitCalc();
}


void HeroProfile::_effectsCalc() {
    shock    .calc(stamina, concentration);
    contusion.calc(stamina, concentration);
    blindness.calc(stamina, concentration);
    bleeding .calc(stamina, concentration);
}


void HeroProfile::loadDummy(ifstream& file) {
    for (int i = 0; i < DUMMY_X; i++) {
        for (int j = 0; j < DUMMY_Y; j++) {
            char currentSymbol;
            file >> currentSymbol;
            switch (currentSymbol) {
                case 'H': {
                    dummy[i][j] = DUMMY_HEAD;
                } break;
                case 'B': {
                    dummy[i][j] = DUMMY_BODY;
                } break;
                case 'h': {
                    dummy[i][j] = DUMMY_LEFTHAND;
                } break;
                case 'j': {
                    dummy[i][j] = DUMMY_RIGHTHAND;
                } break;
                case 'l': {
                    dummy[i][j] = DUMMY_LEFTLEG;
                } break;
                case 'k': {
                    dummy[i][j] = DUMMY_RIGHTLEG;
                } break;
                default: {
                    dummy[i][j] = DUMMY_EMPTY;
                }
            }
        }
    }
}


float HeroProfile::getConcentrationInPerCent() {
    return (float)concentration / ((float)concentrationMax / 200.0f);
}


int HeroProfile::getActiveHandSum() {
    int count = 0;
    if (lHand.hitPoints.canDecrease()) {
        count++;
    }

    if (rHand.hitPoints.canDecrease()) {
        count++;
    }

    return count;
}


void HeroProfile::recheckBaseChars() {
    staminaMax       = 250 + survival.getLevel()*25;
	concentrationMax = 250 + survival.getLevel()*25;
}

