

#include "critmodifier.h"


CritModifier::CritModifier() {
    duration             = 0;
    staminaPerTurn       = 0;
    concentrationPerTurn = 0;
}


void CritModifier::init(int nDuration, int nStamina, int nConcentration) {
    duration             = nDuration;
    staminaPerTurn       = nStamina;
    concentrationPerTurn = nConcentration;
}


void CritModifier::calc(int& stamina, int& concentration) {
    if (isActive()) {
        duration--;
        stamina       -= staminaPerTurn;
        concentration -= concentrationPerTurn;
    }
}

bool CritModifier::isActive() {
    if (duration <= 0) {
        return false;
    } else {
        return true;
    }
}
