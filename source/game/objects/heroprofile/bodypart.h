#ifndef BODYPART_H
#define BODYPART_H


#include "skill.h"


enum BodyPartType { ALL_PART, HEAD, BODY, HAND, LEG };


class BodyPart {
    public:
        BodyPartType type;
        Skill hitPoints;
};

#endif // BODYPART_H
