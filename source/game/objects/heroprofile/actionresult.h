#ifndef ACTIONRESULT_H
#define ACTIONRESULT_H


#include <string>


using namespace std;


enum ActionResultType { ACTION_PERFECT=0, ACTION_NORMAL, ACTION_NOTHING, ACTION_FAIL, ACTION_EPICFAIL };


struct ActionResult {
    ActionResultType type;
    string msg;
    ActionResult(ActionResultType nType, string nMsg) {
        type = nType;
        msg  = nMsg;
    }
};


#endif // ACTIONRESULT_H
