#ifndef INVENTORY_H
#define INVENTORY_H


#include <typeinfo>

#include "items/commonitem.h"
#include "items/ammo.h"


using namespace std;


#define INVENTORY_SIZE 20


class Inventory {
    public:
        Inventory();
        ~Inventory();

        bool canPushItem();
        void pushItem(CommonItem* item);

        void init();

        CommonItem* popItem(int id);
        CommonItem* popItem(string name);

        CommonItem* getItemById(int id);
        CommonItem* getItemByPos(int position);

        int size();

        void destroyEmptyAmmo();
        bool isEmpty();
    private:
        CommonItem* _pool[INVENTORY_SIZE];
        int         _employedCells;

};



#endif // INVENTORY_H
