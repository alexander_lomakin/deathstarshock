#ifndef LIGHT_H
#define LIGHT_H


#include <string>


using namespace std;


struct Light {
        string name;
        int power;
        int red;
        int green;
        int blue;
};


#endif // LIGHT_H
