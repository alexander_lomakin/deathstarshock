#ifndef MOVEMENT_H_
#define MOVEMENT_H_


#include <iostream>

#include "math/lineequation.h"
#include "math/coord.h"


class Movement {
    public:
        Movement();
        void nextMovementStep();
        void nextEquationStep();
        void startAMovement(int nX0, int nY0, int nX1, int nY1, int nStepOffsetSize);
        void startMovement(int nX, int nY, int nStepOffsetSize, Direction nDirection);
        void startMovement(int nX, int nY, int stX, int stY, int nStepOffsetSize, Direction nDirection);
        void getOffsetNow(int& x, int & y);
        bool isMovementEnded();
        Direction getDirection();
        void useLineEquation(bool use);
    private:
        int _nowOffsetX;
        int _nowOffsetY;
        LineEquation _lnEquation;
        int _lineStep;
        int _maxX;
        int _maxY;
        int _stepOffsetSize;
        bool _useLineEquation;
        Direction _direction;
};


#endif
