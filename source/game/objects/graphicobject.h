#ifndef GRAPHICOBJECT_H_
#define GRAPHICOBJECT_H_


#include "animationcarcass.h"
#include "movement.h"
#include <iostream>


class GraphicObject : public Movement {
        Animation _animation[9];
        int _animationSum;
        int _animationIdNow;
    public:
        GraphicObject();

        Animation* getAnimationById(int id);

        /* object position on the area - this is global game coord */
        Direction direction;

        int spriteIdNow;
        int spriteOffset; // from whitch pos object tiles started in SpriteSheet

        void doCurAnimationNextStep();
        void startAnimation(int animationId);
        bool isAnimation();
};


#endif
