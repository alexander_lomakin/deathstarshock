#ifndef COMMONITEM_H
#define COMMONITEM_H


#include <string>
#include <fstream>

#include "math/coord.h"
#include "its.h"


using namespace std;


class CommonItem {
    public:
        CommonItem() {
            id        = -1;
            libraryId = -1;
            name      = "";

            localX = 0;
            localY = 0;
            area   = CENTER;
        }

        int id;
        int libraryId;

        string name;

        short localX;
        short localY;
        Direction area;

        virtual void loadData(ifstream& file) = 0;
        virtual void saveData(ofstream& file) = 0;
        virtual string getSpecialName() = 0;
};


#endif // COMMONITEM_H
