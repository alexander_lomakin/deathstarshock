#ifndef COMPONENT_H
#define COMPONENT_H


#include <fstream>

#include "CommonItem.h"


using namespace std;


class Component : public CommonItem {
    public:
        int cageMax;
        int cageNow;

        void loadData(ifstream& file);
        void saveData(ofstream& file);
        string getSpecialName();
};


#endif // COMPONENT_H
