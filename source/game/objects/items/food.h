#ifndef FOOD_H
#define FOOD_H


#include <fstream>

#include "CommonItem.h"


using namespace std;


class Food : public CommonItem {
    public:
        int restoredStamina;
        int restoredConcentration;

        int mass;

        void loadData(ifstream& file);
        void saveData(ofstream& file);
        string getSpecialName();
};


#endif // FOOD_H
