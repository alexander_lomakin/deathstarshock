

#include "component.h"


using namespace std;


void Component::loadData(ifstream& file) {
    file >> cageNow;
}


void Component::saveData(ofstream& file) {
    file << cageNow << endl;
}


string Component::getSpecialName() {
    return name + " <x" + ITS(cageNow) + ">";
}
