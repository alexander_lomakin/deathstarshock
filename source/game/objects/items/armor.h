#ifndef ARMOR_H
#define ARMOR_H


#include <fstream>

#include "commonitem.h"


using namespace std;


enum ArmorType { HELMET=0, BODY_ARMOR, LEGGINGS, GLOVES, SPACE_SUIT };


class Armor : public CommonItem {
    public:
        int armorClass;
        int armorMass;
        int armorState;
        int armorCamoBonus;

        ArmorType type;

        void loadData(ifstream& file);
        void saveData(ofstream& file);
        string getSpecialName();
};


#endif // ARMOR_H
