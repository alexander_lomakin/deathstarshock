

#include "armor.h"


using namespace std;


void Armor::loadData(ifstream& file) {
    file >> armorState;
}


void Armor::saveData(ofstream& file) {
    file << armorState << endl;
}


string Armor::getSpecialName() {
    return name + " <" + ITS(armorState) + ">";
}
