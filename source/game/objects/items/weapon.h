#ifndef WEAPON_H
#define WEAPON_H


#include <fstream>

#include "commonitem.h"

#include "ammo.h"


using namespace std;


class Weapon : public CommonItem {
    public:
        bool      needAmmo;
        bool      isTwoHanded;
        string    ammoType;
        AmmoClass ammoClassNow;

        int rate;
        int accuracy;
        int mass;
        int magazineCapacity;
        int camoBonus;

        int capacityNow;

        void loadData(ifstream& file);
        void saveData(ofstream& file);
        string getSpecialName();
};


#endif // WEAPON_H
