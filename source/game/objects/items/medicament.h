#ifndef MEDICAMENT_H
#define MEDICAMENT_H


#include <fstream>

#include "CommonItem.h"


using namespace std;


class Medicament : public CommonItem {
    public:
        int restoredHpSum;
        int restoredTiredSum;

        void loadData(ifstream& file);
        void saveData(ofstream& file);
        string getSpecialName();
};


#endif // MEDICAMENT_H
