#ifndef AMMO_H
#define AMMO_H


#include <fstream>

#include "CommonItem.h"


using namespace std;


enum AmmoClass {STANDARD_AMMO, EXTENSIVE_AMMO, PIERCING_AMMO };


class Ammo : public CommonItem {
    public:
        string    ammoType;
        AmmoClass ammoClass;

        int cageNow;
        int cageMaxSize;

        int bulletMass;
        int bulletSpeed;

        int containerMass;

        void loadData(ifstream& file);
        void saveData(ofstream& file);
        string getSpecialName();
};


#endif // AMMO_H
