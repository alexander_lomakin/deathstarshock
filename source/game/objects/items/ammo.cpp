

#include "ammo.h"


using namespace std;


void Ammo::loadData(ifstream& file) {
    file >> cageNow;
}


void Ammo::saveData(ofstream& file) {
    file << cageNow << endl;
}


string Ammo::getSpecialName() {
    return name + " <x" + ITS(cageNow) + ">";
}
