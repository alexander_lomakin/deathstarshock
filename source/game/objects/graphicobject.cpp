

#include "graphicobject.h"


GraphicObject::GraphicObject()
{
	spriteIdNow  = 0;
	spriteOffset = 0;

	direction = BOTTOM;

    _animationSum = 9;
	_animationIdNow = -1;
}


Animation* GraphicObject::getAnimationById(int id)
{
	if (id >= 0 && id < _animationSum)
		return &_animation[id];
	return 0;
}


void GraphicObject::doCurAnimationNextStep()
{
	Animation* anim = getAnimationById(_animationIdNow);
	if (anim == 0)
		return;
	anim->nextStep();
	spriteIdNow = anim->getFrameNow();
}


void GraphicObject::startAnimation(int animationId)
{
	_animationIdNow = animationId;

	Animation* anim = getAnimationById(_animationIdNow);
	anim->startAnimation();
	spriteIdNow = anim->getFrameNow();
}


bool GraphicObject::isAnimation()
{
	if (_animationIdNow == -1)
		return false;
	Animation* anim = getAnimationById(_animationIdNow);

	return !anim->isAnimationEnded();
}
