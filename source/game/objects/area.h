#ifndef AREA_H_
#define AREA_H_


#include <fstream>
#include <iostream>
#include <string>

#include "math/coord.h"
#include "areacell.h"
#include "extendedcell.h"
#include "graphiccell.h"


using namespace std;





class Area {
    public:
        Area();

        AreaCell     table       [AREA_WIDTH][AREA_HEIGHT];
        ExtendedCell extTable    [AREA_WIDTH][AREA_HEIGHT];
        GraphicCell  graphicTable[AREA_WIDTH][AREA_HEIGHT];

        void saveAreaToFile(string areaPath);

        void loadAreaFromFile(string areaPath);
        void loadAreaFromFile(int nX, int nY, int nLevel, string areaPath, string logDir);

        bool isFirstlyVisited();
        void makeAreaOldVisited();
    private:
        bool _isFirstlyVisited;

        void _fillAreaWithEmptyCells();
};


#endif
