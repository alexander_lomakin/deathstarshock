#ifndef MASTERAREACELL_H
#define MASTERAREACELL_H


#include <string>


using namespace std;


    struct MasterAreaCell
    {
        string name;
        string desc;
        string addit;
        bool isTransparent;
        bool isPassable;
        bool isDestroyable;
        int armorClass;
        int hitPoints;
        int additCell;

        int lightType;


    };



#endif // MASTERAREACELL_H
