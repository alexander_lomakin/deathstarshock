

#include "creature.h"


using namespace std;


Creature::Creature()
{
    viewDirection = BOTTOM;



    inventory.init();


	status = NORMAL;

	x = y = 0;

	armor     = 0;
	leftHand  = 0;
	rightHand = 0;
}


Creature::~Creature()
{
    if (leftHand != 0)
        delete leftHand;

    if (armor != 0)
        delete armor;

    if (rightHand != 0)
        delete rightHand;
}


void Creature::equipArmorFromInventory(int itemId)
{
    if (armor != 0)
        inventory.pushItem(armor);

    armor = inventory.popItem(itemId);
}


void Creature::equipArmorFromInventory(string itemName) {
    if (armor != 0)
        inventory.pushItem(armor);

    armor = inventory.popItem(itemName);
}


void Creature::equipWeaponFromInventory(int itemId)
{
    if (leftHand != 0)
        inventory.pushItem(leftHand);

    leftHand = inventory.popItem(itemId);
}


void Creature::equipWeaponFromInventory(string itemName) {
    if (leftHand != 0)
        inventory.pushItem(leftHand);

    leftHand = inventory.popItem(itemName);
}


void Creature::unequipAll()
{
    if (leftHand != 0)
        inventory.pushItem(leftHand);

    if (rightHand != 0)
        inventory.pushItem(rightHand);

    if (armor != 0)
        inventory.pushItem(armor);

    leftHand  = 0;
    rightHand = 0;
    armor     = 0;
}


void Creature::swapWeapon()
{
    CommonItem* a = leftHand;
    leftHand = rightHand;
    rightHand = a;
}


Weapon* Creature::getActiveWeapon() {
    if (leftHand == 0) {
        return 0;
    }

    if (typeid(*leftHand) == typeid(Weapon)) {
        return static_cast<Weapon*> (leftHand);
    } else {
        return 0;
    }

}


bool Creature::canMakeShot() {
    Weapon* weapon = getActiveWeapon();

    if (weapon == 0) {
        return false;
    }

    int freeHands = getActiveHandSum();

    if (freeHands == 0) {
        return false;
    }

    if (weapon->isTwoHanded && freeHands != 2) {
        return false;
    }

    return true;
}
