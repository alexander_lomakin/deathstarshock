#ifndef CREATURE_H_
#define CREATURE_H_


#include <iostream>
#include <string>

#include "heroprofile/heroprofile.h"

#include "graphicobject.h"

#include "inventory.h"

#include "items/weapon.h"

#include "math/coord.h"



using namespace std;


class Creature : public HeroProfile, public GraphicObject {
    public:
        Creature();
        ~Creature();

        int x, y;

        int id;


        string offName;


        int spriteOffset;


        CommonItem* armor;

        CommonItem* leftHand;
        CommonItem* rightHand;




        Inventory inventory;

        Direction viewDirection;

        void equipArmorFromInventory(int itemId);
        void equipArmorFromInventory(string itemName);
        void equipWeaponFromInventory(int itemId);
        void equipWeaponFromInventory(string itemName);

        void unequipAll();

        virtual void swapWeapon();

        Weapon* getActiveWeapon();

        bool canMakeShot();
};


#endif
