

#include "animationcarcass.h"


using namespace std;


void Animation::_recheckFrame()
{
	if (_timeNow % _timePerFrame == 0)
	{
		if (_frameNow+1 >= _spriteOffset + _frameSum)
			_frameNow = _spriteOffset;
		else
			_frameNow++;

	}
}


Animation::Animation()
{
	_animationLength = 0;
	_frameSum        = 0;
	_spriteOffset    = 0;
	_timePerFrame    = 0;
	_frameNow        = 0;
	_timeNow         = 0;
	_seamless        = false;
}


void Animation::setAnimationParameters(int nAnimLength, int nFrameSum, int nSpriteOffset, int nTpf, bool seamless)
{
	_animationLength = nAnimLength;
	_frameSum        = nFrameSum;
	_spriteOffset    = nSpriteOffset;
	_timePerFrame    = nTpf;
	_frameNow        = _spriteOffset;
	_timeNow         = 0;
	_seamless        = seamless;
}


void Animation::startAnimation()
{
    if (!_seamless)
        _frameNow = _spriteOffset;
   //else
    //    _frameNow = _spriteOffset + _frameSum -1;

	_timeNow  = 0;
}


void Animation::nextStep()
{
	_timeNow++;
	if (isAnimationEnded())
		endAnimation();
	else
		_recheckFrame();
}


void Animation::endAnimation()
{
	_timeNow  = _animationLength;
}


bool Animation::isAnimationEnded()
{
	return (_timeNow  == _animationLength);
}


int Animation::getFrameNow()
{
	return _frameNow;
}
