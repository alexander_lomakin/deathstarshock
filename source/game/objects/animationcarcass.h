#ifndef ANIMATIONCARCASS_H_
#define ANIMATIONCARCASS_H_


	enum CreatureAnimation { CA_WALK_BOTTOM = 0, CA_WALK_TOP, CA_WALK_LEFT, CA_WALK_RIGHT,
                             CA_ATTACK_BOTTOM, CA_ATTACK_TOP, CA_ATTACK_LEFT, CA_ATTACK_RIGHT, CA_DEAD };
	enum DoorAnimation { DA_OPEN=0, DA_CLOSE };



    class Animation
    {
        int _animationLength;
        int _frameSum;
        int _spriteOffset;
        int _timePerFrame;
        int _frameNow;
        int _timeNow;
        bool _seamless;

        void _recheckFrame();
    public:
        Animation();
        void setAnimationParameters(int nAnimLength, int nFrameSum, int nSpriteOffset, int nTpf, bool seamless);
        void startAnimation();
        void nextStep();
        void endAnimation();
        bool isAnimationEnded();
        int getFrameNow();
    };




#endif
