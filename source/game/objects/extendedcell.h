#ifndef EXTENDEDCELL_H
#define EXTENDEDCELL_H



struct ExtendedCell {
        ExtendedCell()
        {
            isVisible       = false;
            combatZoneLevel = -1;
            darknessLv      = 0;
            drawBorder      = false;
        }
        bool isVisible;
        bool isLighted;
        int  combatZoneLevel;
        int  darknessLv;
        bool drawBorder;
};



#endif // EXTENDEDCELL_H
