#ifndef AREACELL_H_
#define AREACELL_H_


struct AreaCell {
        AreaCell()
        {
            type            = 0;
            lightType       = -1;
            bloodLevel      = -1;
            generationGroup = -1;
            isExplored      = false;
            hitPoints       = 0;
            additCell       = 0;
        }

        int  type;
        int  lightType;
        int  bloodLevel;
        int  generationGroup;
        int  hitPoints;
        int  additCell;
        bool isExplored;
};


#endif
