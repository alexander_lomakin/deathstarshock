

#include "math/dice.h"
#include "irrlichtbase.h"
#include "modules/startmenu.h"


int main(int argc, char** argv) {
    IrrlichtBase irrlicht(1024, 768, false);

    irrlicht.setCaption("DeathStarShock - Roguelike tactic rpg");

    // INIT RESOURCES
    IrrlichtDevice* dev = IrrlichtBase::getDevice();
    dev->getFileSystem()->addFileArchive("data/graphic/data.zip");

    Dice dice;

    StartMenu startMenu;

    irrlicht.drop();

    return 0;
}
