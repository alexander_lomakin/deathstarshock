

#include "loscalc.h"




//source: http://roguebasin.roguelikedevelopment.org/index.php?title=PythonShadowcastingImplementation


LosCalc::LosCalc(AreaLoader* areaLoader)
{
    _areaLoader = areaLoader;
    baseLightLv = OptionLoader::getNumKey("LosCalc", "BaseLightLv");
    _darknessLv = OptionLoader::getNumKey("LosCalc", "DarknessLv");

    _oldX = 0;
    _oldY = 0;
}


void LosCalc::darkLevel()
{
	for (int x = -AREA_WIDTH; x < 2*AREA_WIDTH; x++)
		for (int y = -AREA_HEIGHT; y < 2*AREA_HEIGHT; y++)
		{
            ExtendedCell* cell = _areaLoader->getExtendedCell(x, y);
			cell->isVisible  = false;
			cell->isLighted = false;
			cell->darknessLv = _darknessLv;
		}
}


void LosCalc::setCellDarknessLevel(int globX, int globY, int darknessLevel)
{

	AreaCell* cell            = _areaLoader->getCell(globX, globY);
	MasterAreaCell masterCell = _areaLoader->getMasterCell(cell->type);

	cell->isExplored = true;
	ExtendedCell* cell2 = _areaLoader->getExtendedCell(globX, globY);
	cell2->isVisible = true;
	cell2->darknessLv = _darknessLv;

	if (masterCell.lightType != -1)
        cell2->isLighted = true;
}


void LosCalc::calcLos(int globX, int globY, int radius)
{
    if (globX == -100)
    {
        globX = _oldX;
        globY = _oldY;
    }
    else
    {
        _oldX = globX;
        _oldY = globY;
    }

    setCellDarknessLevel(globX, globY, baseLightLv);

	int mult[4][8] = {
			{1,  0,  0, -1, -1,  0,  0,  1},
			{0,  1, -1,  0,  0, -1,  1,  0},
			{0,  1,  1,  0,  0, -1, -1,  0},
			{1,  0,  0,  1, -1,  0,  0, -1}
		};

	for (int oct = 0; oct < 8; oct++)
		cast_light(globX, globY, 1, 1.0, 0.0, radius,
					mult[0][oct], mult[1][oct],
					mult[2][oct], mult[3][oct], 0);

    for (int i = -AREA_WIDTH; i < 2*AREA_WIDTH; i++)
		for (int j = -AREA_HEIGHT; j < 2*AREA_HEIGHT; j++)
		{
			ExtendedCell* cell = _areaLoader->getExtendedCell(i, j);

            if (cell->darknessLv == _darknessLv)
				continue;

			cell->darknessLv += calcCellDarknessLevel(i, j, globX, globY);
		}
}


void LosCalc::cast_light(int cx, int cy, int row, double start, double end, int radius, int xx, int xy, int yx, int yy, int id)
{
	if (start < end)
		return;

	double radius_squared = radius*radius;

	for (int j = row; j <= radius+1; j++)
	{
		int dx = -j-1;
		int dy = -j;
		bool blocked = false;
		double new_start = 0;

		while (dx <= 0)
		{
			dx++;

			int X = cx + dx * xx + dy * xy;
			int Y = cy + dx * yx + dy * yy;

			double l_slope = (dx-0.5)/(dy+0.5);
			double r_slope = (dx+0.5)/(dy-0.5);

			if (start < r_slope)
				continue;
			else if (end > l_slope)
					break;
				else
				{
					if (dx * dx + dy * dy < radius_squared)
						set_lit(X, Y);

					if (blocked)
					{
						if (isblocked(X, Y))
						{
							new_start = r_slope;
							continue;
						}
						else
						{
							blocked = false;
							start = new_start;
						}
					}
					else
					{
						if (isblocked(X, Y) && j < radius)
						{
							blocked = true;
							cast_light(cx, cy, j + 1, start, l_slope,
									radius, xx, xy, yx, yy, id + 1);
							new_start = r_slope;
						}
					}
				}
		}

		if (blocked)
			break;
	}
}

void LosCalc::set_lit(int globX, int globY)
{
	setCellDarknessLevel(globX, globY, baseLightLv);
}


bool LosCalc::isblocked(int globX, int globY)
{
	AreaCell* cell            = _areaLoader->getCell(globX, globY);
	MasterAreaCell masterCell = _areaLoader->getMasterCell(cell->type);

	return !masterCell.isTransparent;
}



int LosCalc::calcCellDarknessLevel(int px, int py, int cx, int cy)  {
    int dx;
    int dy;

    dx = cx - px;
    dy = cy - py;
    if (dx < 0)
        dx *= -1;
    if (dy < 0)
        dy *= -1;

    dx++;
    dy++;

    return (dy*dy+dx*dx) *2;
}
