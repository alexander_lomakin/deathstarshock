

#include "cellgenerator.h"

using namespace std;
using namespace irr;
using namespace io;


CellGenerator::CellGenerator()
{
    _masterCells = 0;

    srand(time(0));
}


void CellGenerator::init(vector<MasterAreaCell>* masterCells, string cellGenFilePath)
{
    _masterCells = masterCells;

    _loadCellGroups(cellGenFilePath);
}


void CellGenerator::generateCells(Area* area)
{
    for (int x = 0; x < AREA_WIDTH; x++)
		for (int y = 0; y < AREA_HEIGHT; y++)
        {
            if (area->table[x][y].generationGroup >= 0 && rand()%100 > 20)
            {
                area->table[x][y].additCell = area->table[x][y].type;
                area->table[x][y].type = _getRandomCellByGroupId(area->table[x][y].generationGroup);
            }

        }
}


void CellGenerator::_loadCellGroups(string cellGenFilePath)
{
    IrrXMLReader* xml = createIrrXMLReader(cellGenFilePath.c_str());

    CellGroup cellGroup;

    while(xml && xml->read())
    {
        if (xml->getNodeType() == EXN_ELEMENT)
        {
            if (!strcmp("cellgroup", xml->getNodeName()))
            {
                if (cellGroup.cells.size() != 0)
                    _cellGroups.push_back(cellGroup);

                cellGroup.cells.clear();
                cellGroup.name = xml->getAttributeValue("name");
            }

            if (!strcmp("cell", xml->getNodeName()))
            {
                cellGroup.cells.push_back(xml->getAttributeValue("name"));
            }
        }
    }

    if (cellGroup.cells.size() != 0)
        _cellGroups.push_back(cellGroup);
}


int CellGenerator::_getRandomCellByGroupId(int groupId)
{
    string randCellName = _cellGroups[groupId].cells[rand()%_cellGroups[groupId].cells.size()];

    for (int i = 0; i < _masterCells->size(); i++)
    {
        MasterAreaCell* m = &(*_masterCells)[i];

        if (m->name == randCellName)
            return i;
    }
    return 0;
}


vector<string> CellGenerator::getCellGroupsNames()
{
    vector<string> lst;

    for (int i = 0; i < _cellGroups.size(); i++)
        lst.push_back(_cellGroups[i].name);

    return lst;
}
