#ifndef DROPTOKEN_H
#define DROPTOKEN_H


#include <string>


using namespace std;

    struct DropToken
    {
        DropToken(int nId, string nName, int nTotal)
        {
            ownerId = nId;
            name    = nName;
            total   = nTotal;
        }
        int ownerId;
        string name;
        int total;
    };



#endif // DROPTOKEN_H
