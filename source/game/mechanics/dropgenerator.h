#ifndef DROPGENERATOR_H
#define DROPGENERATOR_H


#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
#include <cstring>
#include <time.h>
#include <irrXML.h>

#include "objects/creature.h"

#include "loaders/itemloader.h"

#include "math/coord.h"

#include "droptoken.h"
#include "dropargument.h"


using namespace std;
using namespace irr;
using namespace irr::io;



class DropGenerator {
    public:
        DropGenerator();
        void loadCells(string xmlFilePath);

        void pushCellItemsOnFloor(int globX, int globY, string cellName);

        void init(ItemLoader* itemLoader);
    private:
        vector<DropToken>    _cellItems;
        vector<DropArgument> _cellGroups;

        int _cellGroupLastId;

        ItemLoader* _itemLoader;

        void _pushCellItemsOnFloor(int globX, int globY, int dropGroupId, int max);

        void _loadCellDrop(IrrXMLReader* xml, string ownerName, int groupSum);
        void _loadCellGroup(IrrXMLReader* xml, string ownerName, int dropSum, int maxDrop, int dropChance);
    };


#endif // DROPGENERATOR_H
