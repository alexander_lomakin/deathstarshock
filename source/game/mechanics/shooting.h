#ifndef SHOOTING_H
#define SHOOTING_H


#include <typeinfo>
#include <cstdlib>
#include <ctime>
#include <Cmath>

#include "loaders/loaders.h"

#include "objects/heroprofile/actionresult.h"

#include "colors/colors.h"
#include "math/coord.h"

#include "eventstack/eventstack.h"
#include "eventstack/waitingcarcass.h"

#include "math/dice.h"
#include "math/lineequation.h"


using namespace std;


class Shooting : public WaitingCarcass {
    public:
        Shooting();

        void init(Loaders* loader);

        ActionResult shoot(Creature* shooter, int targetX, int targetY, int impactX, int impactY);
    private:
        Loaders* _loader;

        Creature* _shooter;
        Creature* _target;
        Weapon*   _shooterWeapon;

        int _targetX, _targetY;
        int _stImpactX, _stImpactY;

        int _getDistance();

        bool _isBarrierOnWay(int curX, int curY);
        bool _barrierTest(int impactY, int curX, int curY);
        void _handleBarrier(int curX, int curY);

        bool _isEnemyOnWay(int curX, int curY);
        void _handleEnemy();
};


#endif // SHOOTING_H
