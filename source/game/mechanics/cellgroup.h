#ifndef CELLGROUP_H
#define CELLGROUP_H


#include <vector>
#include <string>


using namespace std;


    class CellGroup
    {
    public:
        string name;
        vector<string> cells;
    };


#endif // CELLGROUP_H
