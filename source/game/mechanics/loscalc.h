#ifndef LOSCALC_H_
#define LOSCALC_H_


#include "loaders/arealoader.h"
#include "loaders/optionloader.h"



    class LosCalc
    {
    public:
        LosCalc(AreaLoader* areaLoader);

        int baseLightLv;
        int _darknessLv;

        void calcLos(int globX, int globY, int radius);
        void darkLevel();
    private:
        AreaLoader* _areaLoader;
        void setCellDarknessLevel(int globX, int globY, int darknessLevel);
        void cast_light(int cx, int cy, int row, double start, double end, int radius, int xx, int xy, int yx, int yy, int id);
        void set_lit(int globX, int globY);
        bool isblocked(int globX, int globY);
        int calcCellDarknessLevel(int px, int py, int cx, int cy);

        int _oldX, _oldY;
    };



#endif
