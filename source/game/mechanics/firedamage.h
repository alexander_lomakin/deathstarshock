#ifndef FIREDAMAGE_H
#define FIREDAMAGE_H


#include <typeinfo>
#include <cstdlib>
#include <ctime>

#include "loaders/loaders.h"

#include "colors/colors.h"
#include "math/coord.h"

#include "eventstack/eventstack.h"

#include "math/dice.h"


using namespace std;


class FireDamage {
    public:
        FireDamage();

        void init(Loaders* loader);

        void fire(int targetId);
    private:
        Loaders* _loader;

        Creature* _target;

        int _resist;
        int _fireDmg;
};


#endif // FIREDAMAGE_H
