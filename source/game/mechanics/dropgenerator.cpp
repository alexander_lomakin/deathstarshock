

#include "dropgenerator.h"

using namespace std;
using namespace irr;
using namespace io;


DropGenerator::DropGenerator()
{
    _cellGroupLastId = -1;

    _itemLoader = 0;

    srand(time(0));
}



void DropGenerator::loadCells(string xmlFilePath)
{
    IrrXMLReader* xml = createIrrXMLReader(xmlFilePath.c_str());

    while(xml && xml->read())
    {
        if (xml->getNodeType() == EXN_ELEMENT)
        {
            if (!strcmp("cell", xml->getNodeName()))
            {
                string cellName = xml->getAttributeValue("name");
                int dropNum     = STI(xml->getAttributeValue("total"));

                _loadCellDrop(xml, cellName, dropNum);
            }
        }
    }

    delete xml;
}


void DropGenerator::pushCellItemsOnFloor(int globX, int globY, string cellName)
{
    int chance = 100;

    for (int i = 0; i < _cellGroups.size(); i++)
    {
        if (_cellGroups[i].ownerName == cellName)
        {
            if (_cellGroups[i].chanceToDrop >= rand()%100 + 1)
            {
                _pushCellItemsOnFloor(globX, globY, _cellGroups[i].dropId, _cellGroups[i].maxDrop);
                break;
            }
        }
    }
}


void DropGenerator::init(ItemLoader* itemLoader)
{
    _itemLoader = itemLoader;
}


void DropGenerator::_pushCellItemsOnFloor(int globX, int globY, int dropGroupId, int max)
{
    vector <int> items;

    for (int i = 0; i < _cellItems.size(); i++)
        if (_cellItems[i].ownerId == dropGroupId)
            items.push_back(i);

    random_shuffle(items.begin(), items.end());

    if (items.size() < max)
        max = items.size();

    max = rand()%max+1;

    Direction direction = getDirectionByGlobCoord(globX, globY);
    int localX, localY;

    setLocalCoordByDir(globX, globY, direction, localX, localY);

    for (int i = 0; i < max; i++)
        for (int j = 0; j < _cellItems[items[i]].total; j++)
        {
            _itemLoader->makeActiveItemFromLibrary(_cellItems[items[i]].name, localX, localY, direction);
        }
}


void DropGenerator::_loadCellDrop(IrrXMLReader* xml, string ownerName, int groupSum)
{
    int dropNow = 0;

    while(xml && xml->read() && dropNow < groupSum)
    {
        if (xml->getNodeType() == EXN_ELEMENT)
            if (!strcmp("dropgroup", xml->getNodeName()))
                _loadCellGroup(xml, ownerName, STI(xml->getAttributeValue("total")),
                               STI(xml->getAttributeValue("maxDrop")), STI(xml->getAttributeValue("chanceToDrop")));
        dropNow++;
    }
}


void DropGenerator::_loadCellGroup(IrrXMLReader* xml, string ownerName, int dropSum, int maxDrop, int dropChance)
{
    DropArgument dropArg;

    _cellGroupLastId++;

    dropArg.ownerName      = ownerName;
    dropArg.maxDrop      = maxDrop;
    dropArg.chanceToDrop = dropChance;
    dropArg.dropId       = _cellGroupLastId;

    _cellGroups.push_back(dropArg);

    int dropNow = 0;

    while(xml && xml->read() && dropNow < dropSum)
    {
        if (xml->getNodeType() == EXN_ELEMENT)
            if (!strcmp("item", xml->getNodeName()))
                _cellItems.push_back(DropToken(dropArg.dropId, xml->getAttributeValue("name"), STI(xml->getAttributeValue("total"))));

        dropNow++;
    }
}
