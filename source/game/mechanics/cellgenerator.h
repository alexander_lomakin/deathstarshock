#ifndef CELLGENERATOR_H
#define CELLGENERATOR_H


#include <vector>
#include <iostream>
#include <algorithm>
#include <string>
#include <cstring>
#include <time.h>
#include <irrXML.h>

#include "cellgroup.h"
#include "objects/masterareacell.h"
#include "objects/area.h"


using namespace std;
using namespace irr;
using namespace io;


    class CellGenerator
    {
    public:
        CellGenerator();
        void init(vector<MasterAreaCell>* masterCells, string cellGenFilePath);

        void generateCells(Area* area);

        vector<string> getCellGroupsNames();
    private:
        vector<CellGroup> _cellGroups;
        vector<MasterAreaCell>* _masterCells;

        void _loadCellGroups(string cellGenFilePath);

        int _getRandomCellByGroupId(int groupId);
    };



#endif // CELLGENERATOR_H
