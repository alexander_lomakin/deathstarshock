#ifndef GAMELOGIC_H_
#define GAMELOGIC_H_


#include "loaders/loaders.h"

#include "eventstack/waitingcarcass.h"

#include "closecombat.h"
#include "shooting.h"
#include "firedamage.h"
#include "math/coord.h"


class GameLogic : public WaitingCarcass {
    public:
        GameLogic(Loaders* loaders);

        bool movePlayer(Creature* creature);
        void moveForward(Creature* creature);


        void assaultForward(Creature* creature);

        void spawnItem(Creature* creature, string itemName);



        CloseCombat closeCombat;
        Shooting shooting;
        FireDamage fire;


        void reloadWeapon(int id);

        void recalcCreaturePos(Direction direction);

        void changeBeastDirection(Creature* creature, const Direction newDirection);

        void moveBeastWithoutChecks(Creature* creature);
        void moveBeastWithChecks(Creature* creature);

        void dropItemFromInventoryById(Creature* creature, int itemId);
        void dropItemFromInventoryByPos(Creature* creature, int itemPositon);
        void equipItemFromInventoryById(Creature* creature, int itemId);
        void equipItemFromInventoryByPos(Creature* creature, int itemPosition);

        void useItemFromInventoryByPos(Creature* creature, int itemPosition);

        void updateGetItemInfoAndGetItemSum(Creature* creature, const int curPos, int& itemSum, int& itemId);
        void putToInventoryActiveItemById(Creature* creature, const int itemId);


        void updateLos(Creature* creature);
    private:
        Loaders* _loaders;

        bool _isCellFreeToMove(int globX, int globY);
};


#endif
