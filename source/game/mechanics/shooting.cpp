

#include "shooting.h"



using namespace std;


Shooting::Shooting() {
    _loader = 0;
    _target = 0;
}


void Shooting::init(Loaders* loader) {
    _loader = loader;
}


ActionResult Shooting::shoot(Creature* shooter, int targetX, int targetY, int impactX, int impactY) {

    cout << ">>>SHOOTING" << endl;

    // SYS INIT
    _shooter       = shooter;
    _target        = 0;
    _shooterWeapon = _shooter->getActiveWeapon();
    _targetX       = targetX;
    _targetY       = targetY;
    _stImpactX     = impactX;
    _stImpactY     = impactY;

    if (_shooterWeapon == 0) {
        return ActionResult(ACTION_NOTHING, "No active weapon");
    }

    if (_shooter->canMakeShot() == false) {
        return ActionResult(ACTION_NOTHING, "No free hands for weapon");
    }

    // ����������� ���������� ����
    int targetStealth = 0;
    _target = _loader->beastLoader->getBeastByPos(_targetX, _targetY);
    if (_target != 0) {
        targetStealth = _target->stealth.getLevel();
    }
    /* ������ ������ */
    // ���������
    int distance        = _getDistance();
    // ����������
    int stealthSkill    = targetStealth;
    // ����� ��������
    int shootSkill      = _shooter->smallArms.getLevel();
    // ��������
    int intelligence    = _shooter->intelligence.getLevel();
    // �������� ������
    int weaponAccuracy  = _shooterWeapon->accuracy;
    // ������� ������������ � ���������
    float concentration = _shooter->getConcentrationInPerCent();

    /* Delta=(���������+����������)-(����� ��������+��������+�������� ������)*������������(%) */
    float delta = (distance+stealthSkill) - (shootSkill + intelligence + weaponAccuracy)*(concentration/100.0f);

    /* ������ ����� ��������� � ���� ���������� */
    for (int j = 0 ; j < 10; j++)
        cout <<  j << " " << Dice::RandomRange(delta) << endl;
    int impactPointX = _stImpactX + Dice::RandomRange(delta);
    int impactPointY = _stImpactY + Dice::RandomRange(delta);

    cout << "Delta:          " << delta << endl;
    cout << "Distance:       " << distance << endl;
    cout << "stealthSkill:   " << stealthSkill << endl;
    cout << "shootSkill:     " << shootSkill << endl;
    cout << "intelligence:   " << intelligence << endl;
    cout << "weaponAccuracy: " << weaponAccuracy << endl;
    cout << "concentration:  " << concentration << endl;

    // ���� ����������
    float deflectionAngle = 0;

    if (impactPointX < 0) {
        deflectionAngle = atan(delta/(16 * distance));
        impactPointX = DUMMY_X - abs(impactPointX);
        if (impactPointX < 0) {
            impactPointX = Dice::getRandomValue(DUMMY_X - 5, DUMMY_X - 1);
        }
    }

    if (impactPointX >= DUMMY_X) {
        deflectionAngle = -1 * atan(delta/(16 * distance));
        impactPointX = abs(impactPointX);
        if (impactPointX >= DUMMY_X) {
            impactPointX = Dice::getRandomValue(0, 5);
        }
    }

    if (impactPointY >= DUMMY_Y) {
        impactPointY = DUMMY_Y - 1;
    }

    if (impactPointY < 0) {
        impactPointY = 0;
    }


    cout << "deflectionAngle: " << deflectionAngle << endl;

    cout << "impactPointX: " << impactPointX << endl;
    cout << "impactPointY: " << impactPointY << endl;


    /* ���������� ������ ���� */
    LineEquation lnEq(_shooter->x, _shooter->y, _targetX, _targetY);
    int stepNow         = 1;
    int curX            = 0;
    int curY            = 0;

    // ������ ���� � ����������� �� ���������� ����
    lnEq.setAngleDeg(lnEq.getAngleInDegrees() + deflectionAngle);

    while (true) {
        lnEq.setCoordByStep(curX, curY, stepNow);
        stepNow++;

        cout << "stepNow: " <<  stepNow << " ||| " <<  curX << " " << curY << endl;

        if (_isBarrierOnWay(curX, curY) == true) {
            // ���� �� ��������� �� �����������
            if (_barrierTest(impactPointY, curX, curY) == true) {
                _handleBarrier(curX, curY);
                break;
            }
        }

        if (_isEnemyOnWay(curX, curY) == true) {
            _target = _loader->beastLoader->getBeastByPos(curX, curY);
            // ���� ������ ���� � �����-������ ����� ���� - �� ���������� ���������
            if (_target->dummy[impactPointX][impactPointY] != DUMMY_EMPTY) {
                _handleEnemy();
                break;
            }
        }
    }

    return ActionResult(ACTION_NORMAL, "");
}


int Shooting::_getDistance() {
    int x1 = _shooter->x;
    int y1 = _shooter->y;
    int x2 = _targetX;
    int y2 = _targetY;

    float dist = sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));

    return (int)dist;
}


bool Shooting::_barrierTest(int impactY, int curX, int curY) {
    AreaCell* cell            = _loader->areaLoader->getCell(curX, curY);
    MasterAreaCell masterCell = _loader->areaLoader->getMasterCell(cell->type);

    cout << "hete" << endl;

    if (cell == 0) {
        return false;
    }

    cout << "hete2" << endl;

    if (masterCell.isTransparent == false) {
        return true;
    }

    cout << "DUMMY_Y - DUMMY_Y/3: " << DUMMY_Y - DUMMY_Y/3 << "impactnow: " << impactY << endl;

    return (impactY >= DUMMY_Y - DUMMY_Y/3);
}


void Shooting::_handleEnemy() {

    /* ������ ���� */
    /*for (int bulletNow = 0; bulletNow < weapon->bfb; bulletNow++) {
        // �������� ����
        int bulletSpeed = 90-2;
        // ����� ����
        int bulletMass  = 32;
        // ���������� ����������
        int dmK = 5;

        /* bulletDmg = bulletSpeed^2 *(bulletMass*1/(2*dmK)) */
       // int bulletDmg = (int)(bulletSpeed^2 *(bulletMass*1/(2*dmK)));
   // }
    //*/
}


bool Shooting::_isBarrierOnWay(int curX, int curY) {
    AreaCell* cell            = _loader->areaLoader->getCell(curX, curY);
    MasterAreaCell masterCell = _loader->areaLoader->getMasterCell(cell->type);

    if (masterCell.isTransparent == false) {
        return true;
    }

    if (masterCell.isPassable == false) {
        return true;
    }

    return false;
}

void Shooting::_handleBarrier(int curX, int curY) {
    AreaCell* cell            = _loader->areaLoader->getCell(curX, curY);
    MasterAreaCell masterCell = _loader->areaLoader->getMasterCell(cell->type);

    if (cell == 0) {
        return;
    }

    cout << "Target is a " << masterCell.name << endl;
}

bool Shooting::_isEnemyOnWay(int curX, int curY) {
    return (_loader->beastLoader->getBeastByPos(curX, curY) != 0);
}
