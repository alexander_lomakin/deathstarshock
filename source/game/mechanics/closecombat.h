#ifndef CLOSECOMBAT_H
#define CLOSECOMBAT_H


#include <typeinfo>
#include <cstdlib>
#include <ctime>

#include "loaders/loaders.h"

#include "colors/colors.h"
#include "math/coord.h"

#include "eventstack/eventstack.h"
#include "eventstack/waitingcarcass.h"

#include "math/dice.h"


using namespace std;


class CloseCombat : public WaitingCarcass {
    public:
        CloseCombat();
        void init(Loaders* loader);

        void punch(int attackerId);
        void hardPunch(int attackerId);
    private:
        Creature* _attacker;
        Creature* _defender;
        int _targetX, _targetY;

        Loaders* _loader;
};


#endif // CLOSECOMBAT_H
