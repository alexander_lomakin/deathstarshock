

#include "gamelogic.h"


GameLogic::GameLogic(Loaders* loaders)
{
	_loaders = loaders;

	closeCombat.init(_loaders);
	shooting.init(_loaders);
	fire.init(_loaders);
}


void GameLogic::changeBeastDirection(Creature* creature, const Direction newDirection) {
    if (newDirection == CENTER) {
        return;
    }

    creature->viewDirection = newDirection;

    EventStack::pushEvent(ViewEvent(CHANGE_BEAST_DIR, creature->id, (int)newDirection, 0, 0, ""));
}


void GameLogic::moveBeastWithoutChecks(Creature* creature) {
    ExtendedCell* cell = _loaders->areaLoader->getExtendedCell(creature->x, creature->y);

	int globX = creature->x;
	int globY = creature->y;

	updateCoordByDir(globX, globY, creature->viewDirection);

    creature->x = globX;
    creature->y = globY;

    if (creature->status == DEAD) {
        return;
    }

    ExtendedCell* cell2 = _loaders->areaLoader->getExtendedCell(creature->x, creature->y);

    if (cell->isVisible || cell2->isVisible) {
        EventStack::pushEvent(ViewEvent(MOVE_BEAST, creature->id, (int)creature->viewDirection, creature->x, creature->y, ""));
        _waitingForDrawer(_loaders);
    }
}


bool GameLogic::movePlayer(Creature* creature) {
	int globX = creature->x;
	int globY = creature->y;
	int lastX = globX;
	int lastY = globY;

	updateCoordByDir(globX, globY, creature->viewDirection);

    if (_isCellFreeToMove(globX, globY)) {
        creature->x = globX;
        creature->y = globY;

        if (creature->status == DEAD) {
            return true;
        }

        if (getDirectionByGlobCoord(globX, globY) != getDirectionByGlobCoord(lastX, lastY)) {
            _loaders->areaLoader->shiftCurrentAreas(getDirectionByGlobCoord(creature->x, creature->y));
            recalcCreaturePos(getDirectionByGlobCoord(creature->x, creature->y));
        }

        EventStack::pushEvent(ViewEvent(MOVE_BEAST, creature->id, (int)creature->viewDirection, creature->x, creature->y, ""));

        EventStack::pushEvent(ViewEvent(RECALC_LOS, creature->x, creature->y, 0, 0, ""));

        _waitingForDrawer(_loaders);


    }

    return true;
}


void GameLogic::moveForward(Creature* creature) {
    int globX = creature->x;
	int globY = creature->y;

	updateCoordByDir(globX, globY, creature->viewDirection);

    if (_isCellFreeToMove(globX, globY)) {
        creature->x = globX;
        creature->y = globY;

        if (creature->status == DEAD) {
            return;
        }

        ExtendedCell* cell = _loaders->areaLoader->getExtendedCell(globX, globY);

        if (cell->isVisible) {
            EventStack::pushEvent(ViewEvent(MOVE_BEAST, creature->id, (int)creature->viewDirection, creature->x, creature->y, ""));
            _waitingForDrawer(_loaders);
        }

    }
}


void GameLogic::spawnItem(Creature* creature, string itemName) {
    creature->inventory.pushItem(_loaders->itemLoader->createAndCutActiveItemByName(itemName));
}


void GameLogic::recalcCreaturePos(Direction direction) {
	for(int i = 0; i < MAX_BEASTS; i++) {
        Creature* cr = _loaders->beastLoader->getBeastById(i);

        if (cr == 0) {
            continue;
        }

        switch (direction)
		{
			case LEFT:
				cr->x += AREA_WIDTH;
				break;
			case RIGHT:
				cr->x -= AREA_WIDTH;
				break;
			case TOP:
				cr->y += AREA_HEIGHT;
				break;
			case BOTTOM:
				cr->y -= AREA_HEIGHT;
				break;
			case LEFT_TOP:
				cr += AREA_WIDTH;
				cr->y += AREA_HEIGHT;
				break;
			case RIGHT_TOP:
				cr->x -= AREA_WIDTH;
				cr->y += AREA_HEIGHT;
				break;
			case LEFT_BOTTOM:
				cr->x += AREA_WIDTH;
				cr->y -= AREA_HEIGHT;
				break;
			case RIGHT_BOTTOM:
				cr->x -= AREA_WIDTH;
				cr->y -= AREA_HEIGHT;
				break;
		}
		EventStack::pushEvent(ViewEvent(CHANGE_BEAST_POS, cr->id, 0, cr->x, cr->y, ""));
    }
}


void GameLogic::reloadWeapon(int id)
{

    EventStack::pushEvent(ViewEvent(PLAY_SOUND, 0, 0, 0, 0, "reload"));

}


bool GameLogic::_isCellFreeToMove(int globX, int globY) {
    AreaCell* cell            = _loaders->areaLoader->getCell(globX, globY);
    MasterAreaCell masterCell = _loaders->areaLoader->getMasterCell(cell->type);

    if (cell == 0) {
        return false;
    }

    bool isFree = masterCell.isPassable;

    if (_loaders->beastLoader->getBeastByPos(globX, globY) != 0) {
        isFree = false;
    }

    return isFree;
}


void GameLogic::dropItemFromInventoryById(Creature* creature, int itemId) {
    int localX, localY;
    Direction direction = getDirectionByGlobCoord(creature->x, creature->y);
    setLocalCoordByDir(creature->x, creature->y, direction, localX, localY);

    _loaders->itemLoader->pushActiveItem(creature->inventory.popItem(itemId), localX, localY, direction);
}


void GameLogic::dropItemFromInventoryByPos(Creature* creature, int itemPointer) {
    int localX, localY;
    Direction direction = getDirectionByGlobCoord(creature->x, creature->y);
    setLocalCoordByDir(creature->x, creature->y, direction, localX, localY);
    CommonItem* item = creature->inventory.getItemByPos(itemPointer);

    if (item == 0) {
        return;
    }

    _loaders->itemLoader->pushActiveItem(creature->inventory.popItem(item->id), localX, localY, direction);
}


void GameLogic::equipItemFromInventoryById(Creature* creature, int itemId) {
    CommonItem* item = creature->inventory.getItemById(itemId);

    if (typeid(*item) == typeid(Weapon)) {
        creature->equipWeaponFromInventory(itemId);
    } else if (typeid(*item) == typeid(Armor)) {
        creature->equipArmorFromInventory(itemId);
    }
}


void GameLogic::equipItemFromInventoryByPos(Creature* creature, int itemPointer) {
    CommonItem* item = creature->inventory.getItemByPos(itemPointer);

    if (item == 0) {
        return;
    }

    if (typeid(*item) == typeid(Weapon)) {
        creature->equipWeaponFromInventory(item->id);
    } else if (typeid(*item) == typeid(Armor)) {
        creature->equipArmorFromInventory(item->id);
    }
}


void GameLogic::useItemFromInventoryByPos(Creature* creature, int itemPosition) {
    CommonItem* item = creature->inventory.getItemByPos(itemPosition);

    if (item == 0) {
        return;
    }

    if (typeid(*item) == typeid(Medicament)) {
        Medicament* med = static_cast<Medicament*> (item);

        creature->inventory.popItem(item->id);

        delete item;
    }

}


void GameLogic::assaultForward(Creature* creature) {
    if (creature->id == PLAYER_ID) {
        movePlayer(creature);
    } else {
        moveForward(creature);
    }

    int globX = creature->x;
    int globY = creature->y;
    updateCoordByDir(globX, globY, creature->viewDirection);

    shooting.shoot(creature, globX, globY, 8, 7);
}


void GameLogic::updateGetItemInfoAndGetItemSum(Creature* creature, const int curPos, int& itemSum, int& itemId) {
    // SYS INIT
    int localX, localY;
    Direction direction = getDirectionByGlobCoord(creature->x, creature->y);
    setLocalCoordByDir(creature->x, creature->y, direction, localX, localY);

    _loaders->itemLoader->getActiveItemsIdSumOnPos(itemSum, localX, localY, direction);

    if (itemSum == 0) {
        cout << "here " << endl;
    }

    // DRAW LIST
    EventStack::pushEvent(ViewEvent(DRAW_ITEM_LIST, 1, 0, 0, 0, "-=-=-Get item from the floor-=-=-"));

    for (int i = 0; i < itemSum; i++) {
        CommonItem* item = _loaders->itemLoader->getActiveItemIdOnPos(i, localX, localY, direction);
        EventStack::pushEvent(ViewEvent(ADD_ITEM_ELEM, item->libraryId, 0, 0, 0, item->getSpecialName()));

        if (i == curPos) {
            itemId = item->id;
        }
    }

    // SET CURREBT POS IN LIST
    EventStack::pushEvent(ViewEvent(SELECT_NEW_ELEM, curPos, 0, 0, 0, ""));
}


void GameLogic::putToInventoryActiveItemById(Creature* creature, const int itemId) {
    CommonItem* item = _loaders->itemLoader->cutActiveItemByid(itemId);
    creature->inventory.pushItem(item);
    EventStack::pushEvent(ViewEvent(CHAT_MSG, (int)CYAN, 0, 0, 0, "You picked up " + item->name));
}


void GameLogic::updateLos(Creature* creature) {
    EventStack::pushEvent(ViewEvent(RECALC_LOS, creature->x, creature->y, 0, 0, ""));
    _waitingForDrawer(_loaders);
}
